import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoundationNewChatPage } from './foundation-new-chat';

@NgModule({
  declarations: [
    FoundationNewChatPage,
  ],
  imports: [
    IonicPageModule.forChild(FoundationNewChatPage),
  ],
})
export class FoundationNewChatPageModule {}
