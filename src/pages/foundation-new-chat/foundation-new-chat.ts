import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { FoundationChatPageDetailsPage } from '../foundation-chat-page-details/foundation-chat-page-details';
import * as firebase from 'firebase';
import { AngularFireDatabase } from '@angular/fire/database';
import { Network } from '@ionic-native/network';
import { DatabaseHelperProvider } from '../../providers/database-helper/database-helper';
import { FoundationChatMainPage } from '../foundation-chat-main/foundation-chat-main';
import { PhotoViewer } from '@ionic-native/photo-viewer';

/**
 * Generated class for the FoundationNewChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foundation-new-chat',
  templateUrl: 'foundation-new-chat.html',
})
export class FoundationNewChatPage {
  AllChats = [];
  chatID: string;
  isnew: boolean;
  UserData: any;

  items = [];
  forwaredText = ""
  tblChatDetails = "INSERT INTO tblChatDetails (chatid, isreply, message , messagedate , reciverid , replymessageid , status , userid , username , isSync) VALUES";

  GroupUsers = [];
  constructor(public navCtrl: NavController, public ObjDatabase: DatabaseHelperProvider, private database: AngularFireDatabase, private network: Network, public navParams: NavParams, private ObjHelper: HelperProvider, private photoViewer: PhotoViewer) {
    this.forwaredText = navParams.get('isForwardText');
    let Users = navParams.get("Users");
    debugger;
    if (Users != undefined) {
      this.GroupUsers = Users;
      this.items = Users;

      this.items = this.ObjHelper.AlphabaticallyOrderGroup(this.items, true);

      for (let index = 0; index < this.items.length; index++) {
        debugger;
        if (this.items[index].isadmin == "1") {
          let element = this.items[index];
          this.items.splice(index, 1);
          this.items.splice(0, 0, element);
        }
      }
    }
    else {
      this.GetAllChats();
      this.initializeItems();
    }
  }

  GetAllChats() {

    this.database.object('/TblChat/').valueChanges().subscribe(data => {

      let arr = Object.keys(data);
      for (var i = 0; i < arr.length; i++) {
        const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
        this.AllChats.push(object2);
      }

    })

  }

  initializeItems() {
    this.items = [];
    debugger;
    this.ObjHelper.UserInfo.forEach(element => {
      if (element.userid != this.ObjHelper.LoggedInUserID) {
        this.items.push({
          // username: element.name,
          name: element.name,
          userid: element.userid,
          image: element.pic,
          city: element.city,
          village: element.village

        });
      }
    });

    this.items = this.ObjHelper.AlphabaticallyOrder(this.items, false);

  }

  OpenPhotoViewer(photo) {
    var DP = "";
    if (photo.includes("firebasestorage")) {
      DP = photo
    }
    else {
      DP = "https://rajputfoundation.org/" + photo;
    }
    this.photoViewer.show(DP);
  }

  StartChat(param) {

    if (param != this.ObjHelper.LoggedInUserID) {
      if (this.forwaredText == undefined) {
        //param == UserID
        // this.navCtrl.push(FoundationChatPageDetailsPage, { UserData: param }) // param = UserID 
        this.navCtrl.push(FoundationChatPageDetailsPage, { UserData: param, username: this.ObjHelper.UserInfo.find(items => items.userid == param).name, image: "https://rajputfoundation.org/" + this.ObjHelper.UserInfo.find(items => items.userid == param).image });

      } else {

        this.UserData = this.AllChats.find(x => x.recieverid == param && x.userid == this.ObjHelper.LoggedInUserID);
        if (this.UserData == undefined) {
          this.isnew = true;
          this.ForwardMsg(param, "");
        } else {
          this.isnew = false
          this.ForwardMsg(param, this.UserData.chatid)
        }
      }
    }
  }

  getItems(ev: any) {
    this.items = [];
    var val = ev.target.value;
    if (val && val.trim() != '') {
      if (this.GroupUsers.length == 0) {

        // this.items = this.ObjHelper.UserInfo.filter(data =>  {
        //   if(data.name.toLowerCase().indexOf(val.toLowerCase()) > -1){
        //     return data;
        //   }
        // })


        this.ObjHelper.UserInfo.forEach(element => {
          if (element.name.toLowerCase().indexOf(val.toLowerCase()) > -1) {
            if (element.userid != this.ObjHelper.LoggedInUserID) {
              this.items.push({
                // username: element.name,
                name: element.name,
                userid: element.userid,
                image: element.pic,
                city: element.city,
                village: element.village
              });

            }
          }
        });



        this.items = this.ObjHelper.AlphabaticallyOrder(this.items, false);
        // this.items = this.ObjHelper.UserInfo.filter((item) => {
        // if (item.name != null)
        //   if(item.name.toLowerCase().indexOf(val.toLowerCase()) > -1){

        //   }else{

        //   }
        //   return ();
        // })
      }
      else {
        this.items = this.GroupUsers.filter((item) => {
          if (item.name != null)
            return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }
    else {
      if (this.GroupUsers.length == 0)
        this.initializeItems();
      else
        this.items = this.GroupUsers;
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoundationNewChatPage');
  }

  createID() {
    let date = new Date().getTime().toString();
    date = date.substring(date.length - 6);
    return date
  }

  ForwardMsg(userID, paramChatID) {

    if (this.isnew) {
      this.chatID = this.createID();
      paramChatID = this.chatID;
      this.database.list("/TblChat/").push({
        // lastmessage: "click to see your message",
        lastmessage: this.forwaredText,
        datechat: new Date().toISOString().split('T')[0],
        userid: this.ObjHelper.LoggedInUserID,
        username: this.ObjHelper.UserInfo.find(items => items.userid == userID).username,
        dp: "https://rajputfoundation.org/" + this.ObjHelper.UserInfo.find(items => items.userid == userID).image,
        recieverid: userID,
        chatid: this.chatID,
        image: "",
        isadminview: 0
      })
      this.database.list("/TblChat/").push({
        // lastmessage: "click to see your message",
        lastmessage: this.forwaredText,
        datechat: new Date().toISOString().split('T')[0],
        userid: userID,
        username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
        dp: "https://rajputfoundation.org/" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).image,
        recieverid: this.ObjHelper.LoggedInUserID,
        chatid: this.chatID,
        image: "",
        isadminview: 0
      })
    }

    let network = this.network.type;
    if (network == "none") {
      for (let i = 0; i < this.forwaredText.length; i++) {

        this.tblChatDetails = this.tblChatDetails + "('" + paramChatID + "','" + 0 + "','" + this.forwaredText[i] + "','" + new Date().toISOString().split('T')[0] + "','" + userID + "','" + "" + "','Pending','" + this.ObjHelper.LoggedInUserID + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username + "',0),"
        var query = this.tblChatDetails.slice(0, -1);

        this.ObjDatabase.InsertQuerySqlite(query, []).then(data => {
          // this.navCtrl.push(FoundationChatMainPage);
          //Added by Haroon on 6/6/19 (Second day of Eid)
          if (i == (this.forwaredText.length - 1))
            this.navCtrl.push(FoundationChatPageDetailsPage, { UserData: userID, ChatID: paramChatID, username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username, image: "https://rajputfoundation.org/" + this.ObjHelper.UserInfo.find(items => items.userid == userID).image });
        })
      }
    }
    else {

      for (let i = 0; i < this.forwaredText.length; i++) {

        this.database.list("/TblChatDetails/").push({
          userid: this.ObjHelper.LoggedInUserID,
          username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).name,
          reciverid: userID,
          message: this.forwaredText[i],
          messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
          status: "Send",
          isreply: 0,
          replymessageid: "",
          chatid: paramChatID
        }).then(x => {
          // this.navCtrl.push(FoundationChatMainPage); 
          //Added by Haroon on 6/6/19 (Second day of Eid)
          if (i == (this.forwaredText.length - 1))
            this.navCtrl.push(FoundationChatPageDetailsPage, { UserData: userID, ChatID: paramChatID, username: this.ObjHelper.UserInfo.find(items => items.userid == userID).username, image: "https://rajputfoundation.org/" + this.ObjHelper.UserInfo.find(items => items.userid == userID).image });
        })
      }
    }


  }



}
