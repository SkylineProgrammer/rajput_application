import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoundationChatLogingPage } from './foundation-chat-loging';

@NgModule({
  declarations: [
    FoundationChatLogingPage,
  ],
  imports: [
    IonicPageModule.forChild(FoundationChatLogingPage),
  ],
})
export class FoundationChatLogingPageModule {}
