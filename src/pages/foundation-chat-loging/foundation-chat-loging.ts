import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { HelperProvider } from '../../providers/helper/helper';
import { ToastController } from 'ionic-angular';

import firebase, { database } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { FoundationChatMainPage } from '../foundation-chat-main/foundation-chat-main';
import { HomePage } from '../home/home';
import { DatabaseHelperProvider } from '../../providers/database-helper/database-helper';
import { Network } from '@ionic-native/network';
import { RegistrationPage } from '../Registration/registration';
import { dateDataSortValue } from 'ionic-angular/umd/util/datetime-util';
import { ViewChild } from '@angular/core/src/metadata/di';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { Platform } from 'ionic-angular';
import { Badge } from '@ionic-native/badge';
import { HTTP, HTTPResponse } from '@ionic-native/http';

/**
 * Generated class for the FoundationChatLogingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foundation-chat-loging',
  templateUrl: 'foundation-chat-loging.html',
})
export class FoundationChatLogingPage {





  UserName = "";
  Password = "";

  PasswordType = "password";
  PasswordCheck = false;


  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    private network: Network,
    public navParams: NavParams,
    private ObjHelper: HelperProvider,
    private database: AngularFireDatabase,
    public toastCtrl: ToastController,
    private ObjHttp: Http,
    public ObjDatabase: DatabaseHelperProvider,
    private alertCtrl: AlertController,
    private camera: Camera,
    public platform: Platform,
    private badge: Badge,
    public httpPlugin: HTTP
  ) {

  }

  ShowPassword() {

    if (this.PasswordCheck) {
      this.PasswordType = "password";
      this.PasswordCheck = false;
    } else {
      this.PasswordType = "text";
      this.PasswordCheck = true;
    }
  }


  SetBadge() {
    this.badge.requestPermission().then(data => {
      this.badge.set(2).then(data => {
        alert("Done");
      }).catch(err => {
        alert(err);
      })
    })
  }

  presentToast(param) {
    const toast = this.toastCtrl.create({
      message: param,
      duration: 3000
    });
    toast.present();
  }
  GetJSONString(Json: string) {
    return Json.substring(Json.indexOf("["), Json.indexOf("</string>"));
  }

  SaveAllUsers(data) {

    this.ObjDatabase.SelectQuerySqlite("delete from TblAllUsers").then(res => {
      let AllUsers = "INSERT INTO TblAllUsers (userid, username, village , country , area , city , pic , email , isadmin , name) VALUES";
      data.forEach(element => {
        AllUsers = AllUsers + "('" + element.userid + "','" + element.username + "','" + element.village + "','" + element.country + "','" + element.area + "','" + element.city + "','" + element.pic + "','" + element.email + "','" + element.isadmin + "','" + element.name + "'),"
      });
      AllUsers = AllUsers.slice(0, -1);
      this.ObjDatabase.InsertQuerySqlite(AllUsers, []).then(data => {
        // alert("User Inserted");
      })
    })

  }

  // testing() {
  //   this.crop.crop('https://images.unsplash.com/photo-1521093470119-a3acdc43374a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80', {quality: 75})
  //   .then(
  //     newImage => {
  //        
  //     },
  //     error => {
  //        
  //     }
  //   );
  // }

  GotoSignUpPage() {
    this.navCtrl.push(RegistrationPage, { isEdit: "0" });
  }

  LogIn() {

    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      // duration: 3000
    });
    loader.present();

    if (this.network.type != 'none') {
      if (this.UserName != "" && this.Password != "") {
        const Params = {
          UserName: this.UserName,
          Password: this.Password,
          _PlayerID: "35e95185-2926-496d-805d-1b9f03ea8513"
        };

        const ParamsAllUser = {};
        // alert(this.ObjHelper.ServerHost);
        // this.ObjHttp.post(this.ObjHelper.ServerHost + "UserLogin", Params).map((res: Response) => res.json()).subscribe((data) => {
        // 35e95185-2926-496d-805d-1b9f03ea8513
        // this.httpPlugin.setHeader("content-type", "application/json");

        this.ObjHttp.get(this.ObjHelper.ServerHost + "UserLogin?UserName=" + this.UserName + "&Password=" + this.Password + "&_PlayerID=" + this.ObjHelper.UserPlayerID).map((res: Response) => res.text()).subscribe((data) => {
          // this.httpPlugin.get(this.ObjHelper.ServerHost + "UserLogin?UserName=" + this.UserName + "&Password=" + this.Password + "&_PlayerID=" + this.ObjHelper.UserPlayerID, {}, {}).then((data : any) => {
          // let res = JSON.parse(data.d);

          // let res = this.GetJSONString(data);
          let res = data.includes("true");
          if (res) {
            this.ObjHttp.post(this.ObjHelper.ServerHost + "GetAllUser", ParamsAllUser).map((res: Response) => res.json()).subscribe((data) => {

              let UserInfo = [];
              UserInfo = JSON.parse(data.d);
              if (UserInfo.length > 0)
                this.ObjHelper.UserInfo = UserInfo;
              this.SaveAllUsers(UserInfo);
              let Value = UserInfo.find(items => items.username.toLowerCase() == this.UserName.toLowerCase());
              if (Value == undefined) {
                Value = UserInfo.find(items => items.phone == this.UserName.toLowerCase());
              }
              if (Value != undefined) {

                console.log("Found")
                this.ObjHelper.LoggedInUserID = Number(Value.userid);
                this.ObjHelper.LoggedUserName = Value.username;
                this.ObjHelper.LoggedInUserImage = Value.image;
                this.ObjHelper.isAdmin = Value.isadmin;
                this.ObjHelper.isBlocked = Value.isblock;
                this.ObjHelper.UpdateUserLastSeen(true);
                let Query = "INSERT INTO TblCurrentLoggedInUser (userid , islogin , isAdmin , isblock ) VALUES (" + this.ObjHelper.LoggedInUserID + " , 'true' , " + Value.isadmin + " , " + Value.isblock + ")";
                this.ObjDatabase.ExecuteIntoSQlite(Query, []).then(data => {
                  console.log("user Inserted");
                })
                // this.navCtrl.setRoot(FoundationChatMainPage);



                setTimeout(() => {
                  loader.dismiss();

                  this.navCtrl.push(HomePage).then(() => {
                    let index = 0;
                    this.navCtrl.remove(index);
                  });
                }, 3000);

                // this.navCtrl.setRoot(HomePage);
              }
            }, (error) => {
              loader.dismiss();
              alert("Error");
              alert(JSON.stringify(error));
              // alert("Error while getting all users from cloud");
              // alert(error);
            })


            // this.ObjHelper.GetAllUsersFromMYSQL().then((data) => {

            //   if (data) {
            //     let Value = this.ObjHelper.UserInfo.find(items => items.username.toLowerCase() == this.UserName.toLowerCase());
            //     if (Value != undefined) {
            //       // console.log("Found")
            //       this.ObjHelper.LoggedInUserID = Number(Value.userid);
            //       this.ObjHelper.LoggedUserName = Value.username;
            //       this.ObjHelper.LoggedInUserImage = Value.image;
            //     }
            //     else {
            //       // console.log("Couldn't find");
            //       this.ObjHelper.LoggedInUserID = Number(prompt("Enter Userid"));
            //     }
            //     this.ObjDatabase.InsertQuerySqlite("delete from TblCurrentLoggedInUser", []).then(data => {
            //       console.log("TblCurrentLoggedInUser deleted");
            //       let IsLogin = "INSERT INTO TblCurrentLoggedInUser (userid, islogin , isAdmin , isblock) VALUES";
            //       IsLogin = IsLogin + "('" + this.ObjHelper.LoggedInUserID + "','" + "true" + "','" + this.ObjHelper.isAdmin + "','" + this.ObjHelper.isBlocked + "')"

            //       this.ObjDatabase.InsertQuerySqlite(IsLogin, []).then(data => {
            //         console.log("IsLogin + " + true);
            //       }, (error) => {
            //         console.log("Error while adding data TblCurrentLoggedInUser");
            //         console.log("IsLogin " + false);
            //         console.log(error);
            //       })
            //     })

            //     this.navCtrl.push(HomePage);
            //   }
            //   else
            //     console.log("Internet failed to connect");
            //   // alert("Internet failed to connect");

            // }
            //   , (error) => {
            //     //alert("Internet failed to connect");
            //     console.log("Something went wrong");
            //   }
            // );
          } else {
            loader.dismiss();
            alert("Invalid Username or Password");
          }
          // console.log(this.BindArray)
        }, (error) => {

          loader.dismiss();

          alert("While logging")
          alert(error);
          alert(JSON.stringify(error));

        }
        )

      } else {
        loader.dismiss();
        alert("Some Fields are Empty");
      }
    } else {
      loader.dismiss();
      alert("Your internet is not connected");
    }





  }

  Alert() {
    alert('Invalid Email Address');
  }

  ForgotPasswordPrompt() {

    let alert = this.alertCtrl.create({
      title: 'Forgot Password',
      inputs: [
        // {
        //   name: 'oldemail',
        //   placeholder: 'Current Email'
        // },
        {
          name: 'newemail',
          placeholder: 'Email to send Password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send Email',
          handler: (data) => {

            if (data.newemail != "" && data.newemail.includes("@") && data.newemail.includes(".com")) {

              console.log('ok clicked');
              this.ForgotPassword(data.newemail, data.newemail)

            } else {
              // invalid login
              this.Alert();
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  ForgotPassword(Email, NewEmail) {

    // this.ObjHelper.ServerHost = "http://localhost:18857/services/Testing.asmx/";
    this.ObjHttp.get(this.ObjHelper.ServerHost + "ForgotPassword?Email=" + Email + "&NewEmail=" + NewEmail).map((res: Response) => res.text()).
      subscribe((data) => {
        if (data.includes("true"))
          alert("Password sent to your email");
        else
          alert("No User found with provided email");
      }
        , (error) => {

        }
      )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoundationChatLogingPage');
  }

}
