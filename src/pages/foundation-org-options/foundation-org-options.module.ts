import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoundationOrgOptionsPage } from './foundation-org-options';

@NgModule({
  declarations: [
    FoundationOrgOptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(FoundationOrgOptionsPage),
  ],
})
export class FoundationOrgOptionsPageModule {}
