import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ViewController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { FoundationNewChatPage } from '../foundation-new-chat/foundation-new-chat';

/**
 * Generated class for the FoundationOrgOptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foundation-org-options',
  templateUrl: 'foundation-org-options.html',
})
export class FoundationOrgOptionsPage {

  OrgGroupChat = [];
  AllOrgGroupMembers  = [];
  constructor(public viewCtrl: ViewController , public navCtrl: NavController, private database: AngularFireDatabase, public alertCtrl: AlertController , public loadingCtrl: LoadingController  ,public navParams: NavParams, private ObjHelper: HelperProvider) {
    this.OrgGroupChat = this.navParams.get("OrgGroupDetails");
    this.AllOrgGroupMembers = this.navParams.get("OrgGroupMembers");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoundationOrgOptionsPage');
  }


  BtnLogoutClick() {
    const confirm = this.alertCtrl.create({
      // title: 'Use this lightsaber?',
      message: 'Do you want to Clear Group ?? ',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            this.ClearAllChat()
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('No clicked');
          }
        }
      ]
    });
    confirm.present();

  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Clearing ..... ",
      duration: 2000
    });
    loader.present();
  }

  ClearAllChat(){
    var aa = this.OrgGroupChat;
     this.presentLoading();
    this.OrgGroupChat.forEach(Outerelement => {
      Outerelement.Msgs.forEach(Innerelement => {
        this.database.list("/TblGroupOrgDetails/"+Innerelement.Key).push({
          ClearUserID : this.ObjHelper.LoggedInUserID
        }).then(res => {
          this.viewCtrl.dismiss();
        // alert('running');          
        });
      });
    });

  }

  ShowGroupUsers(){
    let aa = this.AllOrgGroupMembers;
     
    this.navCtrl.push(FoundationNewChatPage, { Users: this.AllOrgGroupMembers });

  }

}
