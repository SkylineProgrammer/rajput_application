import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoundationOrgGroupsPage } from './foundation-org-groups';

@NgModule({
  declarations: [
    FoundationOrgGroupsPage,
  ],
  imports: [
    IonicPageModule.forChild(FoundationOrgGroupsPage),
  ],
})
export class FoundationOrgGroupsPageModule {}
