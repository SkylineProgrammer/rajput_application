import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, PopoverController } from 'ionic-angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { HelperProvider } from '../../providers/helper/helper';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import firebase from 'firebase';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Network } from '@ionic-native/network';
import { DatabaseHelperProvider } from '../../providers/database-helper/database-helper';
import { FoundationNewChatPage } from '../foundation-new-chat/foundation-new-chat';
import { Clipboard } from '@ionic-native/clipboard';
import { FoundationOrgOptionsPage } from '../foundation-org-options/foundation-org-options';
import { CaptureAudioOptions, MediaFile, MediaCapture, CaptureError } from '@ionic-native/media-capture';
import { Media } from '@ionic-native/media';
import { Base64 } from '@ionic-native/base64';
import { Http, Response } from '@angular/http';
// import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Crop } from '@ionic-native/crop';
import { RatioCrop, RatioCropOptions } from 'ionic-cordova-plugin-ratio-crop';


/**
 * Generated class for the FoundationOrgGroupsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foundation-org-groups',
  templateUrl: 'foundation-org-groups.html',
})
export class FoundationOrgGroupsPage {
  AudioText: any;
  isDisable: boolean;
  ImagePreview = 0;
  ImagePreviewVar = "https://yt3.ggpht.com/a/AGF-l79ZSQE5TBgbUJnPdvmDK_TYYi8itV6JXqGl8A=s900-mo-c-c0xffffffff-rj-k-no"
  IsActionPresented: boolean;
  toggled: boolean;
  GroupName: any;
  ChatMeg = [];
  MessageText = "";
  InputMessage = "";
  tblOrgGroupDetails = "INSERT INTO tblOrgTableDetails (isreply, message , messagedate ,  status , userid , username , OrgGroupName, isSync) VALUES";


  private cropOptions: RatioCropOptions = {
    quality: 75,
    targetWidth: 1080,
    targetHeight: 1080,
    widthRatio: -1,
    heightRatio: -10
  };

  takePicture() {
    return this.camera.getPicture({
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: false,
      correctOrientation: true
    })
      .then((fileUri) => {
        this.Ccrop.ratioCrop(fileUri, this.cropOptions).then(ImageData  => {
          const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 5000
          });
          loader.present();
          let imgFinal = this.generateFromImage(ImageData);
        })
      })
  }

  selectPicture() {
    
    return this.camera.getPicture({
      allowEdit: false,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      destinationType: this.camera.DestinationType.FILE_URI
    })
      .then((fileUri) => {
        return this.Ccrop.ratioCrop(fileUri, this.cropOptions).then(ImageData => {
          const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 5000
          });
          loader.present();
          let imgFinal = this.generateFromImage(ImageData);
        })
      })
      
  }



  // userid: this.ObjHelper.LoggedInUserID,
  // username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
  // groupid: this.CurrectGroupID,
  // message: MessageText,
  // messagedate: new Date().toISOString().split('T')[0],
  // grouptype: this.CurrentGroupType,
  // status: "Recieved",
  // isreply: 0,
  // replymessageid: "",
  // village: this.CurrentGroupStatus
  TodayDate = new Date().toISOString().split('T')[0];
  YesterDayDate = new Date(Date.now() - 864e5).toISOString().split('T')[0];
  constructor(public navCtrl: NavController,
    private ObjHttp: Http,
    private camera: Camera,
    private filePlugin: File,
    private base64: Base64,
    private media: Media,
    private mediaCapture: MediaCapture,
    public popoverCtrl: PopoverController,
    private network: Network,
    public ObjDatabase: DatabaseHelperProvider,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private photoViewer: PhotoViewer,
    private database: AngularFireDatabase,
    private ObjHelper: HelperProvider,
    private clipboard: Clipboard,
    public toastCtrl: ToastController,
    private crop: Crop,
    private Ccrop: RatioCrop
  ) {
    this.GroupName = navParams.get('GroupName');
    this.isDisable = this.ObjHelper.isBlocked == "0" ? false : true;
    this.GetText();
    this.CurrentGroupUser();
    let Mynetwork = this.network.type;
    if (Mynetwork == "none") {
      this.GetAllChatsFromSQLITE();
    } else {
      this.isSyncTrue();
      this.GetAllChatsFromFirebase();
    }
  }

  CurrentGroupUsers = [];

  CurrentGroupUser() {
    this.CurrentGroupUsers = [];
    this.ObjDatabase.SelectQuerySqlite("select * from TblOrgUser").then((InnerDATA: any) => {
       
      var Count = 0;
      InnerDATA.forEach(element => {
         
        if (element.GroupName == this.GroupName && Count == 0) {
          var User = this.ObjHelper.UserInfo.find(data => data.userid == element.UserID).name;
          this.CurrentGroupUsers.push(User);
          Count++;
          // alert(User);
        }
      });
    })
  }

  // 16/7/2019
  OpenPopOver(param) {

    var CurrentGroupMembers = []
    this.ObjDatabase.SelectQuerySqlite("select * from TblOrgUser where GroupName = '" + this.GroupName + "'").then((data: any) => {
      data.forEach(element => {

        CurrentGroupMembers.push(this.ObjHelper.UserInfo.find(data => data.userid == element.UserID));
      });
      let popover = this.popoverCtrl.create(FoundationOrgOptionsPage, { OrgGroupDetails: this.ChatMeg, OrgGroupMembers: CurrentGroupMembers });
      popover.present({
        ev: param
      });

    })


  }

  checkUserClear(param) {

    var Keys = Object.keys(param);
    var res = 0;

    for (var loop = 0; loop < Keys.length; loop++) {

      if (Keys[loop] == "OrgGroupName" || Keys[loop] == "isreply" || Keys[loop] == "message" || Keys[loop] == "messagedate" || Keys[loop] == "replymessageid" ||
        Keys[loop] == "status" || Keys[loop] == "userid" || Keys[loop] == "username"
      ) {

      } else {
        if (param[Keys[loop]].ClearUserID == this.ObjHelper.LoggedInUserID) {
          res = 1;
        }
      }
    }

    return res;
  }

  GetAllChatsFromFirebase() {
    this.database.object('/TblGroupOrgDetails/').valueChanges().subscribe(data => {
      this.ChatMeg = [];
      let AllMsgs = [];
      let arr = Object.keys(data);
      for (var i = 0; i < arr.length; i++) {
        const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
        if (object2.OrgGroupName == this.GroupName) {
          if (this.checkUserClear(data[arr[i]]) == 1) {

          }
          else {
            AllMsgs.push(object2);
            if (i == (arr.length - 1) && AllMsgs.length > 1) {
              let IsFirst = false;
              const group = AllMsgs.reduce((Date, MsgObject, i) => {

                if (!IsFirst) {
                  Date = [];
                  Date[AllMsgs[0].messagedate.split(' ')[0]] = [];
                  Date[AllMsgs[0].messagedate.split(' ')[0]].push(AllMsgs[0]);
                }
                IsFirst = true;
                let MsgDate = MsgObject.messagedate.split(' ')[0];
                if (!Date[MsgDate])
                  Date[MsgDate] = [];

                Date[MsgDate].push(MsgObject);
                return Date;

              })

              // this.ChatMeg = group;
              // console.clear();

              if (IsFirst) {
                this.ChatMeg = Object.keys(group).map((res) => {

                  console.log(group + res);
                  return {
                    MsgDate: res,
                    Msgs: group[res]
                  }
                });
              }
            }
            else if (AllMsgs.length > 0) {


              this.ChatMeg[0] = {
                MsgDate: AllMsgs[0].messagedate.split(' ')[0],
                Msgs: AllMsgs
              };
              // this.ChatMeg = {
              //   MsgDate: new Date().toISOString().split('T')[0],
              //   Msgs: AllMsgs[0]
              // }
            }
          }

          // this.ChatMeg.push(object2);
        }
      }
    })
  }

  handleSelection(event) {
    // this.emojitext = this.emojitext + " " + event.char;
    this.InputMessage = this.InputMessage + " " + event.char;
    this.toggled = false;
  }

  createID() {
    let date = new Date().getTime().toString();
    date = date.substring(date.length - 6);
    return date
  }
  image = ""

  GetAllChatsFromSQLITE() {
    this.ObjDatabase.SelectQuerySqlite("select * from tblOrgTableDetails where OrgGroupName = '" + this.GroupName + "'").then((data: any) => {
      this.ChatMeg = data;
    })
  }


   async generateFromImage(img, MAX_WIDTH: number = 700, MAX_HEIGHT: number = 700, quality: number = 1,) {
    var canvas: any = document.createElement("canvas");
    var image = new Image();

    image.onload = () => {
      var width = image.width;
      var height = image.height;

      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");

      ctx.drawImage(image, 0, 0, width, height);

      // IMPORTANT: 'jpeg' NOT 'jpg'
      var dataUrl = canvas.toDataURL('image/jpeg', quality);
      
      const pictures: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + this.createID());

      pictures.putString(dataUrl, 'data_url').then(() => {
        // const UP: firebase.storage.UploadTask = sotre;
        pictures.getDownloadURL().then((downloadURL) => {

          this.image = downloadURL;
          this.ImagePreviewVar = downloadURL;
           
          // document.getElementById('DvScroll').style.opacity = "0.7";
          // document.getElementById('DvScroll').style.backgroundImage = "";
          // document.getElementById('DvScroll').style.backgroundColor = "black";
          // this.ImagePreview = 1;

          var obj = {
            userid: this.ObjHelper.LoggedInUserID,
            username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
            message: this.image,
            messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
            status: "Recieved",
            isreply: 0,
            replymessageid: "",
            OrgGroupName: this.GroupName
          }
          this.SendFirebase(obj);
          this.tblOrgGroupDetails = this.tblOrgGroupDetails + "(0,'" + this.image + "','" + new Date().toISOString().split('T')[0] + " " + new Date().toISOString().split('T')[1] + "', 'Recieved' ,'" + this.ObjHelper.LoggedInUserID + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username + "','" + this.GroupName + "' , 1)"
          this.SendSQLITE2(this.tblOrgGroupDetails);

        }, (error) => {
          alert("Error " + error);

          alert(JSON.stringify(error));
        });
      });
      // callback(dataUrl)
    }
    image.src = img;
  }

  async OpenCamera() {
    // today
    let Mynetwork = this.network.type;
    if (Mynetwork != "none") {

      try {
        // const options: CameraOptions = {
        //   quality: 100,
        //   targetHeight: 600,
        //   targetWidth: 600,
        //   destinationType: this.camera.DestinationType.DATA_URL,
        //   encodingType: this.camera.EncodingType.JPEG,
        //   mediaType: this.camera.MediaType.PICTURE
        // }

        const options: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE
        }

       this.camera.getPicture(options).then((Image) => {
          this.crop.crop(Image, { quality: 75 })
            .then(
            newImage => {
              let imgFinal = this.generateFromImage(newImage);
            },
            error => alert('Error cropping image' + error)
            );
        })


      } catch (error) {
        alert("Catch " + error);
        alert(JSON.stringify(error));
      }

    } else {
      // alert("Imag");
    }

  }


  // async OpenCamera() {
  //   // today
  //   let Mynetwork = this.network.type;
  //   if (Mynetwork != "none") {

  //     try {
  //       const options: CameraOptions = {
  //         quality: 100,
  //         targetHeight: 600,
  //         targetWidth: 600,
  //         destinationType: this.camera.DestinationType.DATA_URL,
  //         encodingType: this.camera.EncodingType.JPEG,
  //         mediaType: this.camera.MediaType.PICTURE
  //       }

  //       const res = await this.camera.getPicture(options);
  //        


  //       this.crop.crop(res, { quality: 75 })
  //         .then(
  //         newImage => alert('new image path is: ' + newImage),
  //         error => alert('Error cropping image ' + error)
  //         );

  //       const image = `data:image/jpeg;base64,${res}`;
  //       const pictures: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + this.createID());

  //       pictures.putString(image, 'data_url').then(() => {
  //         // const UP: firebase.storage.UploadTask = sotre;
  //         pictures.getDownloadURL().then((downloadURL) => {

  //           this.image = downloadURL;
  //           this.ImagePreviewVar = downloadURL;
  //           // document.getElementById('DvScroll').style.opacity = "0.7";
  //           // document.getElementById('DvScroll').style.backgroundImage = "";
  //           // document.getElementById('DvScroll').style.backgroundColor = "black";
  //           // this.ImagePreview = 1;

  //           var obj = {
  //             userid: this.ObjHelper.LoggedInUserID,
  //             username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
  //             message: this.image,
  //             messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
  //             status: "Recieved",
  //             isreply: 0,
  //             replymessageid: "",
  //             OrgGroupName: this.GroupName
  //           }
  //           this.SendFirebase(obj);
  //           this.tblOrgGroupDetails = this.tblOrgGroupDetails + "(0,'" + this.image + "','" + new Date().toISOString().split('T')[0] + " " + new Date().toISOString().split('T')[1] + "', 'Recieved' ,'" + this.ObjHelper.LoggedInUserID + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username + "','" + this.GroupName + "' , 1)"
  //           this.SendSQLITE2(this.tblOrgGroupDetails);

  //         }, (error) => {
  //           alert("Error " + error);

  //           alert(JSON.stringify(error));
  //         });
  //       });

  //     } catch (error) {
  //       alert("Catch " + error);
  //       alert(JSON.stringify(error));
  //     }

  //   } else {
  //     // alert("Imag");
  //   }

  // }


  GetText() {
    this.database.object('/Validation/').valueChanges().subscribe(data => {

      let arr = Object.keys(data);

      for (var i = 0; i < arr.length; i++) {
        const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
        this.AudioText = object2.Text;
      }


    })
  }

  Audio = "";
  OpenMic() {

    if (this.AudioText == "") {


       
      let options: CaptureAudioOptions = { limit: 1 };
      this.mediaCapture.captureAudio(options)
        .then((data: MediaFile[]) => {
          debugger
          this.Audio = data[0].fullPath;
          let Name = data[0].name;
          // Name = Name.split('.')[0] + ".mp3";
          let storageRef = firebase.storage().ref();
          let metadata = {
            contentType: 'audio/mp3',
          };

          let filePath = data[0].fullPath;
          this.base64.encodeFile(filePath).then(
            (base64: any) => {
               
              console.log('file base64 encoding: ' + base64);
            }
            , (error) => {
               
            }
          );
          // filePath = "file:///storage/emulated/0/VoiceRecorder/my_sounds/";
          filePath = data[0].fullPath.split(data[0].fullPath.split('/')[data[0].fullPath.split('/').length - 1])[0];
          this.filePlugin.readAsDataURL(filePath, Name).then((file) => {
             
            let voiceRef = storageRef.child('/photos/audios/' + this.createID()).putString(file, firebase.storage.StringFormat.DATA_URL);
            voiceRef.on(firebase.storage.TaskEvent.STATE_CHANGED, (snapshot) => {
              console.log("uploading");
            }, (e) => {
               
              console.log(JSON.stringify(e, null, 2));
            }, () => {
              // let DURL = "https://firebasestorage.googleapis.com/v0/b/" + voiceRef.snapshot.metadata.bucket + "/o/";
              var rep = voiceRef.snapshot.metadata.fullPath.replace('/', '%2F');
              let DURL = "https://firebasestorage.googleapis.com/v0/b/" + voiceRef.snapshot.metadata.bucket + "/o/" + rep.replace('/', '%2F');

              let DURL2 = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F250342";

              this.ObjHttp.get(DURL)
                .map((res: Response) => res.json())
                .subscribe((data) => {
                  DURL = DURL + "?alt=media&token=" + data.downloadTokens;
                  this.image = DURL;
                   
                  this.SendMsgImage();
                  // this.SendMsg();
                  // let jsonString: string = this.GetJsonString(data);
                  // let Data = JSON.parse(jsonString);
                  //  
                },
                (error) => {
                   
                  console.log("Error in Subscribe." + error);
                });

              // let DURL = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F020863?alt=media&token=90723f01-1a20-4049-9ddc-09c3916df539"
              // var downloadURL = voiceRef.snapshot.downloadURL;
              // this.Audio = DURL;
              // this.Audio = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F499007?alt=media"

              var str = "<audio controls><source src=' " + this.Audio + " ' type='audio/amr'></audio>";
              // document.getElementById('audio').innerHTML = str;
            });
          }
            , (error) => {
               
              console.log(error)
              console.log(JSON.stringify(error));
            }
          );


        }, (err: CaptureError) => {
           
        });

    } else {
      alert(this.AudioText);
    }

  }

  onFileChanged(param) {
    // today
    let Mynetwork = this.network.type;
    if (Mynetwork != "none") {

      var aa;
      const file: any = param.target.files[0];
       

      this.crop.crop(file, { quality: 75 })
        .then(
        newImage => alert('new image path is: ' + newImage),
        error => alert('Error cropping image ' + error)
        );


      const metaData = { 'contentType': file.type };
      let filename = this.createID();
      //  Video wala kaam
      if (file.type == "video/mp4") {
        var StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/vidoes/' + this.createID());
      } else {
        var StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + this.createID());
      }
      //  Video wala kaam
      // const StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + filename + "1");
      // const Store = StorageRef.put(file, metaData);
      const loader = this.loadingCtrl.create({
        content: "Please wait...",
        // duration: 3000
      });
      loader.present();
      StorageRef.put(file, metaData).then((data) => {
        // alert("Put");
        StorageRef.getDownloadURL().then((downloadURL) => {
          // alert(downloadURL + " is download url");
          this.image = downloadURL;
          this.ImagePreviewVar = downloadURL;
          document.getElementById('DvScroll').style.backgroundImage = "";
          document.getElementById('DvScroll').style.backgroundColor = "black";
          this.ImagePreview = 1;

          loader.dismiss();

        }, (error) => {
          // alert("Error while creating download url");
          // alert(error);
          // alert(error.body);
          // alert(error.message);
          // alert(JSON.stringify(error));
        });
      }, (error) => {
        // alert("Error while putting");
      });

    }

  }

  SendMsgImage() {

    var obj = {
      userid: this.ObjHelper.LoggedInUserID,
      username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
      message: this.image,
      messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
      status: "Recieved",
      isreply: 0,
      replymessageid: "",
      OrgGroupName: this.GroupName
    }
    this.SendFirebase(obj);
    this.BackMsg();
    this.tblOrgGroupDetails = this.tblOrgGroupDetails + "(0,'" + this.image + "','" + new Date().toISOString().split('T')[0] + " " + new Date().toISOString().split('T')[1] + "', 'Recieved' ,'" + this.ObjHelper.LoggedInUserID + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username + "','" + this.GroupName + "' , 1)"
    this.SendSQLITE2(this.tblOrgGroupDetails);
  }

  BackMsg() {
    document.getElementById('DvScroll').style.opacity = "1";
    document.getElementById('DvScroll').style.backgroundImage = "url('https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png')";
    document.getElementById('DvScroll').style.backgroundColor = "";
    this.ImagePreview = 0;
  }

  SendSQLITE(Query, Obj) {
    this.ObjDatabase.InsertQuerySqlite(Query, []).then(data => {
      this.InputMessage = "";
      this.tblOrgGroupDetails = "INSERT INTO tblOrgTableDetails (isreply, message , messagedate ,  status , userid , username , OrgGroupName, isSync) VALUES";
      this.ChatMeg.push(Obj);
    })
  }

  SendSQLITE2(Query) {

    this.ObjDatabase.InsertQuerySqlite(Query, []).then(data => {
      this.tblOrgGroupDetails = "INSERT INTO tblOrgTableDetails (isreply, message , messagedate ,  status , userid , username , OrgGroupName, isSync) VALUES";
      // this.ChatMeg.push(Obj);
    })
  }

  isSyncTrue() {
    this.ObjDatabase.SelectQuerySqlite("select * from tblOrgTableDetails where OrgGroupName = '" + this.GroupName + "' and isSync = 0").then((data: any) => {

      this.ObjDatabase.SelectQuerySqlite("update tblOrgTableDetails set isSync = 1 , status = 'Recieved'  where OrgGroupName = '" + this.GroupName + "'").then(x => {
        data.forEach(element => {
          var obj = {
            userid: element.userid,
            username: element.username,
            message: element.message,
            messagedate: element.messagedate,
            status: "Recieved",
            isreply: 0,
            replymessageid: "",
            OrgGroupName: this.GroupName
          }
          this.SendFirebase(obj);
        });
      })


    })
  }

  SendClick() {

    if (this.ObjHelper.isReply == 1 && this.InputMessage != "") {
      var obj2 = {
        userid: this.ObjHelper.LoggedInUserID,
        username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
        message: this.InputMessage,
        messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
        status: "Recieved",
        isreply: 1,
        replymessageid: this.ObjHelper.isReplyData[0].message,
        OrgGroupName: this.GroupName
      }
      this.ObjHelper.isReply = 0;
      this.ObjHelper.isReplyData = [];
      this.ObjHelper.isReplyText = "";
      this.IsActionPresented = false;
      this.SendFirebase(obj2);
      this.tblOrgGroupDetails = this.tblOrgGroupDetails + "(0,'" + this.InputMessage + "','" + new Date().toISOString().split('T')[0] + " " + new Date().toISOString().split('T')[1] + "', 'Recieved' ,'" + this.ObjHelper.LoggedInUserID + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username + "','" + this.GroupName + "' , 1)"
      this.SendSQLITE2(this.tblOrgGroupDetails);

    }

    else {
      if (this.InputMessage != "") {
        let network = this.network.type;
        if (network == "none") {
          // tblOrgGroupDetails = "INSERT INTO tblOrgTableDetails (isreply, message , messagedate ,  status , userid , username , OrgGroupName, isSync) VALUES";
          var obj = {
            userid: this.ObjHelper.LoggedInUserID,
            username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
            message: this.InputMessage,
            messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toISOString().split('T')[1],
            status: "Pending",
            isreply: 0,
            replymessageid: "",
            OrgGroupName: this.GroupName
          }
          this.tblOrgGroupDetails = this.tblOrgGroupDetails + "(0,'" + this.InputMessage + "','" + new Date().toISOString().split('T')[0] + " " + new Date().toISOString().split('T')[1] + "', 'Pending' ,'" + this.ObjHelper.LoggedInUserID + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username + "','" + this.GroupName + "' , 0)"
          this.SendSQLITE(this.tblOrgGroupDetails, obj);
        }
        else {
          var obj = {
            userid: this.ObjHelper.LoggedInUserID,
            username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
            message: this.InputMessage,
            messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
            status: "Recieved",
            isreply: 0,
            replymessageid: "",
            OrgGroupName: this.GroupName
          }
          this.SendFirebase(obj);
          this.tblOrgGroupDetails = this.tblOrgGroupDetails + "(0,'" + this.InputMessage + "','" + new Date().toISOString().split('T')[0] + " " + new Date().toISOString().split('T')[1] + "', 'Recieved' ,'" + this.ObjHelper.LoggedInUserID + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username + "','" + this.GroupName + "' , 1)"
          this.SendSQLITE2(this.tblOrgGroupDetails);
        }

      }
    }


  }

  SendFirebase(Obj) {
    this.database.list("/TblGroupOrgDetails/").push(Obj).then(x => {
      this.InputMessage = "";
    })
  }

  ConversationDivClick() {
    if (!this.RealClick) {

      this.toggled = false;
      this.IsActionPresented = false
      // if (this.SelectedID != "") {
      //   document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
      //   this.SelectedID = ""
      // }
      if (this.SelectedID.length > 0) {
        for (let i = 0; i < this.SelectedID.length; i++) {
          document.getElementById(this.SelectedID[i]).style.backgroundColor = this.SelectedID[i][0] == 'S' ? "#e1ffc7" : "white";
        }
        this.SelectedID = [];
        this.SelectedChat = [];
        this.ObjHelper.isReply = 0;
        this.ObjHelper.isReplyData = [];
        this.ObjHelper.isReplyText = "";
      }
    }
    else {
      this.RealClick = false;
    }
  }

  DeleteClick() {

    for (let i = 0; i < this.SelectedChat.length; i++) {
      if (this.SelectedChat[i].userid == this.ObjHelper.LoggedInUserID) {
        let param = this.SelectedChat[i];
        this.database.list('/TblGroupOrgDetails/').remove(this.SelectedChat[i].Key).then((data) => {

          this.IsActionPresented = false;
          this.SelectedID = [];
          this.SelectedChat.splice(this.SelectedChat.indexOf(this.SelectedChat.find(items => items == param)), 1);
        }, (error) => {

        }
        );
      }
      else
        alert("Cannot delete other user messages");

    }

    // if (this.SelectedChat.userid == this.ObjHelper.LoggedInUserID) {
    //   this.database.list('/TblGroupDetails/').remove(this.SelectedChat.Key).then((data) => {
    //      
    //     this.IsActionPresented = false;
    //     this.SelectedID = "";
    //   }, (error) => {
    //      
    //   }
    //   );
    // }
    // else
    //   alert("Cannot delete other user messages");

  }
  SelectedChat = [];
  SelectedID = [];

  presentActionSheet(param, selectedid, SorR) {
    selectedid = SorR + selectedid;
    // this.SelectedChat.push(param);

    // if (this.SelectedID != "" && this.SelectedID != selectedid) {
    //   document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
    //   this.SelectedID = "";
    // }

    // ============
    // if (this.SelectedID == selectedid) {
    //   document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
    //   this.IsActionPresented = false;
    //   this.SelectedID = "";
    // }
    // else {
    //   if (this.SelectedID != "") {
    //     document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
    //     this.SelectedID = "";
    //   }
    //   else {
    //     this.IsActionPresented = true;
    //     this.SelectedID = selectedid;
    //     document.getElementById(this.SelectedID).style.backgroundColor = "cornflowerblue";
    //   }
    if (this.SelectedID.find(items => items == selectedid) != undefined) {
      document.getElementById(this.SelectedID.find(items => items == selectedid)).style.backgroundColor = this.SelectedID.find(items => items == selectedid)[0] == 'S' ? "#e1ffc7" : "white";
      this.IsActionPresented = false;
      this.SelectedID.splice(this.SelectedID.indexOf(this.SelectedID.find(items => items == selectedid)), 1);
      this.SelectedChat.splice(this.SelectedChat.indexOf(this.SelectedChat.find(items => items == param)), 1);
      // this.SelectedChat.push(param);

      // this.SelectedID = "";
    }
    else {
      // if (this.SelectedID.length > 0) {
      //   document.getElementById(selectedid).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
      //   // this.SelectedID = "";
      //   this.SelectedID.splice(this.SelectedID.indexOf(this.SelectedID.find(items => items == selectedid)), 1);

      // }
      // else {
      this.IsActionPresented = true;
      this.SelectedID.push(selectedid);
      this.SelectedChat.push(param);
      document.getElementById(selectedid).style.backgroundColor = "cornflowerblue";
      // }
    }


    // const actionSheet = this.actionSheetCtrl.create({
    //   title: 'Select Your Activity',
    //   buttons: [
    //     {
    //       text: 'Copy',
    //       role: 'destructive',
    //       handler: () => {
    //         this.clipboard.copy(param.message);
    //         console.log('Destructive clicked');
    //       }
    //     }, {
    //       text: 'Forward',
    //       handler: () => {
    //         if (this.network.type != "this.network.type != ") {
    //           this.clipboard.copy(param.message)
    //           // .then(() => {
    //           //     this.navCtrl.push(FoundationNewChatPage, { isForwardText: param.message });
    //           // });

    //           this.clipboard.paste().then(
    //             (resolve: string) => {
    //               var text = resolve;
    //                
    //               this.navCtrl.push(FoundationNewChatPage, { isForwardText: text });
    //               // this.clipboard.clear();
    //             },
    //             (reject: string) => {
    //               //  alert('Error: ' + reject);
    //             }
    //           );
    //           // this.navCtrl.push(FoundationNewChatPage, { isForwardText: "ForwardText" });

    //         }
    //         else {
    //           alert("Network Error");
    //         }
    //         console.log('Archive clicked');
    //       }
    //     }, {
    //       text: 'Delete',
    //       handler: () => {
    //         // this.database.list('/TblChat/').update("param.Key", {
    //         //   Name: 'arqam',
    //         //   pass: 234
    //         // })

    //         // bus Messge ki ID dedenge to delete hgyega 
    //          
    //         if (param.userid == this.ObjHelper.LoggedInUserID) {
    //           this.database.list('/TblChatDetails/').remove(param.key).then((data) => {
    //              
    //           }, (error) => {
    //              
    //           }
    //           );
    //         }
    //         else
    //           alert("Cannot delete other user messages");

    //         console.log('Archive clicked');
    //       }
    //     }, {
    //       text: 'Cancel',
    //       role: 'cancel',
    //       handler: () => {
    //         console.log('Cancel clicked');
    //       }
    //     }
    //   ]
    // });
    // actionSheet.onDidDismiss(() => {
    //    
    //   this.IsActionPresented = false;
    // })
    // actionSheet.present();
  }

  ForwardClick() {
    let messages = [];

    for (let i = 0; i < this.SelectedChat.length; i++) {
      messages.push(this.SelectedChat[i].message);
    }
    if (this.network.type != "none") {
      // this.clipboard.copy(this.SelectedChat.message)
      // .then(() => {
      //     this.navCtrl.push(FoundationNewChatPage, { isForwardText: param.message });
      // });

      // this.clipboard.paste().then(
      // (resolve: string) => {
      // var text = resolve;

      this.navCtrl.push(FoundationNewChatPage, { isForwardText: messages }).then(() => {
        this.IsActionPresented = false;

        if (this.SelectedChat.length == this.SelectedID.length) {
          for (let i = 0; i < this.SelectedChat.length; i++) {
            document.getElementById(this.SelectedID[i]).style.backgroundColor = this.SelectedID[i][0] == 'S' ? "#e1ffc7" : "white";
          }
          this.SelectedChat = [];
          this.SelectedID = []
        }
      });


    }
    else {
      alert("Network Error");
    }

  }

  CopyClick() {
    this.clipboard.copy(this.SelectedChat.length > 0 ? this.SelectedChat[0].message : "").then(() => {
      this.IsActionPresented = false;
    });
  }

  ReplyClick() {
    this.ObjHelper.isReply = 1;
    this.ObjHelper.isReplyData = this.SelectedChat;
    if (this.ObjHelper.isReplyData[0].message.split(':')[0] == "https") {
      // this.presentToast("Reply Against Your Selected Iamge");
      this.ObjHelper.isReplyText = 'Reply Against Your Selected Iamge';

    } else {
      // this.presentToast("Reply Against ( " + this.ObjHelper.isReplyData[0].message + " )");
      this.ObjHelper.isReplyText = this.ObjHelper.isReplyData[0].message.substring(0, 30) + "..";
    }
  }

  presentToast(param) {
    const toast = this.toastCtrl.create({
      message: param,
      duration: 9000,
      position: 'top'
    });
    toast.present();

  }
  ImageClick(param) {
    this.photoViewer.show(param);
  }

  GoBack() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoundationOrgGroupsPage');
  }
  RealClick = false;

  SelectMsgClick(param, index, SorR) {
    this.RealClick = true;
    if (this.SelectedID.length > 0)
      this.presentActionSheet(param, index, SorR);
    else {
      this.RealClick = false;
      this.SelectedChat.length = 0;
    }
  }
}
