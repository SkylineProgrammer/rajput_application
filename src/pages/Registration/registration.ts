import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController, NavParams, AlertController } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { HelperProvider } from '../../providers/helper/helper';
import { FoundationChatLogingPage } from '../foundation-chat-loging/foundation-chat-loging';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import firebase from 'firebase';
import { DatabaseHelperProvider } from '../../providers/database-helper/database-helper';
import { RatioCrop, RatioCropOptions } from 'ionic-cordova-plugin-ratio-crop';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'registration',
  templateUrl: 'registration.html'
})
export class RegistrationPage {
  signupform: FormGroup;
  //image = "https://www.rajputfoundation.org/image/logo2.png";
  image = "https://www.rajputfoundation.org/user_image/default_profile_picture.jpeg";


  ShowUserInfo: any;
  ObjReg = {
    Email: "",
    Password: "",
    ConfirmPassword: "",
    Name: "",
    FatherName: "",
    Gender: "",
    ///DOB: new Date().toISOString(),
    DOB: "",
    Country: "",
    Area: "",
    Village: "",
    Phone: "",
    Profession: "",
    City: "",
    State: ""
  }
  OtherVillage = "";
  OtherCity = "";
  StatesOfPakista = [
    "Sindh",
    "K.P.K",
    "Punjab",
    "Balochistan"
  ]

  ngOnInit() {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.signupform = new FormGroup({
      Email: new FormControl('', [Validators.required]),
      Password: new FormControl('', [Validators.required]),
      ConfirmPassword: new FormControl('', [Validators.required]),
      Name: new FormControl('', [Validators.required]),
      FatherName: new FormControl('', [Validators.required]),
      Gender: new FormControl('', [Validators.required]),
      DOB: new FormControl('', [Validators.required]),
      Country: new FormControl('', [Validators.required]),
      Area: new FormControl('', [Validators.required]),
      Village: new FormControl('', [Validators.required]),
      Phone: new FormControl('', [Validators.required]),
      Profession: new FormControl('', [Validators.required]),
      City: new FormControl('', [Validators.required]),
      State: new FormControl('', [Validators.required]),
    });
  }

  SaveAllUsers(data) {

    this.ObjDatabase.SelectQuerySqlite("delete from TblAllUsers").then(res => {
      let AllUsers = "INSERT INTO TblAllUsers (userid, username, village , country , area , city , pic , email , isadmin , name) VALUES";
      data.forEach(element => {
        AllUsers = AllUsers + "('" + element.userid + "','" + element.username + "','" + element.village + "','" + element.country + "','" + element.area + "','" + element.city + "','" + element.pic + "','" + element.email + "','" + element.isadmin + "','" + element.name + "'),"
      });
      AllUsers = AllUsers.slice(0, -1);
      this.ObjDatabase.InsertQuerySqlite(AllUsers, []).then(data => {
        // alert("User sk Inserted");
      })
    })

  }

  GetAllUserFromServer() {
    const ParamsAllUser = {};
    this.ObjHttp.post(this.ObjHelper.ServerHost + "GetAllUser", ParamsAllUser).map((res: Response) => res.json()).subscribe((data) => {

      let UserInfo = [];
      UserInfo = JSON.parse(data.d);
      if (UserInfo.length > 0)
        this.ObjHelper.UserInfo = UserInfo;
      this.SaveAllUsers(UserInfo);
      let Value = UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID);
      if (Value != undefined) {

        console.log("Found")
        this.ObjHelper.LoggedInUserID = Number(Value.userid);
        this.ObjHelper.LoggedUserName = Value.username;
        this.ObjHelper.LoggedInUserImage = Value.image;
        this.ObjHelper.isAdmin = Value.isadmin;
        this.ObjHelper.isBlocked = Value.isblock;
        var aa = this.ObjHelper.GetYourVillageUserCount();
        var bb = this.ObjHelper.GetYourAreaUserCount();
        var cc = this.ObjHelper.GetYourCityCount();
        let Query = "INSERT INTO TblCurrentLoggedInUser (userid , islogin , isAdmin , isblock ) VALUES (" + this.ObjHelper.LoggedInUserID + " , 'true' , " + Value.isadmin + " , " + Value.isblock + ")";

        this.ObjDatabase.SelectQuerySqlite("delete from TblCurrentLoggedInUser").then(res => {
          this.ObjDatabase.ExecuteIntoSQlite(Query, []).then(data => {
            // alert("user Inserted");
          })
        })


        // this.navCtrl.setRoot(HomePage);
      }
    }, (error) => {
      // alert("Error while getting all users from cloud");
      // alert(error);
    })
  }

  showConfirmAlert() {
    const confirm = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Do you Update Your Information ?? ',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            // console.log('Agree clicked');
            this.RegisterUser();
          }
        }
      ]
    });
    confirm.present();
  }

  Register() {

    // if (this.ObjReg.Password.length >= 6) {
    //   if (this.ObjReg.Password == this.ObjReg.ConfirmPassword) {
    if (this.ObjReg.Password.length >= 6 && this.ObjReg.Password == this.ObjReg.ConfirmPassword && this.ObjReg.Email != "" && this.ObjReg.Name != "" && this.ObjReg.FatherName != ""
      && this.ObjReg.Country != "" && this.ObjReg.Gender != "" &&
      this.ObjReg.Area != "" && this.ObjReg.Village != ""
      && this.ObjReg.Phone != "" && this.ObjReg.Profession != "" && this.ObjReg.Country != ""
      && this.image != "https://www.rajputfoundation.org/user_image/default_profile_picture.jpeg"
      && this.ObjReg.Village != "" && this.ObjReg.City != ""
    ) {

      if (this.ObjReg.Village != "Other") {
        if (this.ObjReg.Country != "Pakistan") {
          if (this.isEdit) {
            this.showConfirmAlert();
          } else {
            this.RegisterUser();
          }
        }
        else {
          if (this.ObjReg.City == "Other") {
            if (this.OtherCity == "") {
              document.getElementById("City2").style.border = "1px solid red"
              //this.presentToast("City is required");
            }
            else {
              if (this.isEdit) {
                this.showConfirmAlert();
              } else {
                this.RegisterUser();
              }
            }
          }
          else if (this.ObjReg.City == "") {
            document.getElementById("City").style.border = "1px solid red";
            //this.presentToast("City is required");
          }
          else {
            if (this.isEdit) {
              this.showConfirmAlert();
            } else {
              this.RegisterUser();
            }
          }

        }
      }
      else {
        if (this.OtherVillage == "") {
          document.getElementById("Village2").style.border = "1px solid red"
          //this.presentToast("Village is required");
        }
        else
          this.ObjReg.Village = this.OtherVillage;
        // else {
        //   if (this.isEdit) {
        //     this.showConfirmAlert();
        //   } else {
        //     this.RegisterUser();
        //   }
        // }
      }

    } else {
      debugger;
      if (this.ObjReg.Email == "") {
        // document.getElementById("Email").style.border = "1px solid red";
        document.getElementById("Email2").style.display = "block";

        document.getElementById("Password").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";

      }

      else if (this.ObjReg.Password.length < 6) {
        document.getElementById("Password").style.display = "block";

        document.getElementById("Email2").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";

      }

      else if (this.ObjReg.Password != this.ObjReg.ConfirmPassword) {
        document.getElementById("ConfirmPassword").style.display = "block";

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";

        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";

      }

      // else {
      //   document.getElementById("Email").style.border = "none";
      //   document.getElementById("Email2").style.display = "none";

      // }

      else if (this.ObjReg.Name == "") {
        // document.getElementById("Name").style.border = "1px solid red"
        document.getElementById("Name2").style.display = "block"

        document.getElementById("ConfirmPassword").style.display = "none";
        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";

      }
      // else {
      //   document.getElementById("Name").style.border = "none";
      //   document.getElementById("Name2").style.display = "none"

      // }

      else if (this.ObjReg.FatherName == "") {
        // document.getElementById("Father").style.border = "1px solid red"
        document.getElementById("Father2").style.display = "block"

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";


      }
      // else {
      //   document.getElementById("Father").style.border = "none";
      //   document.getElementById("Father2").style.border = "none"
      // }

      else if (this.ObjReg.Gender == "") {
        // document.getElementById("Gender").style.border = "1px solid red"
        document.getElementById("Gender2").style.display = "block"

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";


      }
      else if (this.ObjReg.DOB == "") {

        // document.getElementById("Date").style.border = "1px solid red"

        document.getElementById("DOB2").style.display = "block"

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";

      }

      else if (this.ObjReg.Country == "") {
        // document.getElementById("Country").style.border = "1px solid red";
        document.getElementById("Country2").style.display = "block";

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";

      }

      else if (this.ObjReg.Country == "Pakistan" && this.ObjReg.State == "") {
        // document.getElementById("State").style.border = "1px solid red";
        document.getElementById("State2").style.display = "block";

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";

      }
      else if (this.ObjReg.State != "" && this.ObjReg.City == "") {
        // document.getElementById("City").style.border = "1px solid red";
        document.getElementById("City3").style.display = "block";

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";

      }
      // else {
      //   document.getElementById("Gender").style.border = "none";
      //   document.getElementById("Gender2").style.border = "none"
      // }

      else if (this.ObjReg.Area == "") {
        // document.getElementById("Area").style.border = "1px solid red"
        document.getElementById("Area2").style.border = "block"

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";

      }
      // else {
      //   document.getElementById("Area").style.border = "none";
      //   document.getElementById("Area2").style.border = "block"

      // }

      else if (this.ObjReg.Village == "") {
        // document.getElementById("Village").style.border = "1px solid red"
        document.getElementById("Village3").style.display = "block"

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";

      }
      // else {
      //   document.getElementById("Village").style.border = "none";
      //   document.getElementById("Village3").style.display = "none"

      // }

      else if (this.ObjReg.Phone == "") {
        // document.getElementById("Phone").style.border = "1px solid red"
        document.getElementById("Phone2").style.display = "block"

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";


      }
      // else {
      //   document.getElementById("Phone").style.border = "none";
      //   document.getElementById("Phone2").style.border = "none"

      // }

      else if (this.ObjReg.Profession == "") {
        // document.getElementById("Profession").style.border = "1px solid red"
        document.getElementById("Profession2").style.display = "block"

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";

      }
      // else {
      //   document.getElementById("Profession").style.border = "none";
      //   document.getElementById("Profession2").style.display = "none"
      // }


      // else {
      //   document.getElementById("Country").style.border = "none";
      //   document.getElementById("Country2").style.display = "none";
      // }

      else if (this.image == "https://www.rajputfoundation.org/user_image/default_profile_picture.jpeg") {
        this.presentToast("Image is required");
        document.getElementById("Image").style.display = "block";

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("ConfirmPassword").style.display = "none";

      }


      else if (this.ObjReg.Country == "Pakistan" && this.ObjReg.State == "") {
        // document.getElementById("State").style.border = "1px solid red";
        document.getElementById("State2").style.display = "block";

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("City3").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("City3").style.border = "none";
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";


        //this.presentToast("State is required");
      }
      else if (this.ObjReg.Country == "Pakistan" && this.ObjReg.State != "" && this.ObjReg.City == "") {
        // document.getElementById("City").style.border = "1px solid red";
        document.getElementById("City3").style.border = "block";

        document.getElementById("Email2").style.display = "none";
        document.getElementById("Password").style.display = "none";
        document.getElementById("Name2").style.display = "none"
        document.getElementById("Father2").style.display = "none"
        document.getElementById("Gender2").style.display = "none"
        document.getElementById("DOB2").style.display = "none"
        document.getElementById("Country2").style.display = "none";
        document.getElementById("State2").style.display = "none";
        document.getElementById("Area2").style.border = "none"
        document.getElementById("Village3").style.display = "none"
        document.getElementById("Phone2").style.display = "none"
        document.getElementById("Profession2").style.display = "none"
        document.getElementById("Image").style.display = "none";
        document.getElementById("ConfirmPassword").style.display = "none";


        //this.presentToast("City is required");
      }

      // this.presentToast("Fill all Feilds First");
    }
  }
  // else {
  //   this.presentToast("Password Does not Matched");
  // }
  //}
  // else
  //   document.getElementById("Password").style.display = "block";

  //alert("Password Must Be Greater Than 6 Characters");
  //}

  GetJSONString(Json: string) {
    return Json.substring(Json.indexOf("["), Json.indexOf("</string>"));
  }

  createID() {
    let date = new Date().getTime().toString();
    date = date.substring(date.length - 6);
    return date
  }

  onFileChanged(param) {


    var aa;
    const file: File = param.target.files[0];
    const metaData = { 'contentType': file.type };
    let filename = this.createID();
    //  Video wala kaam
    // if (file.type == "video/mp4") {
    //   var StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/vidoes/' + this.createID());
    // } else {
    var StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/DP/' + this.createID());
    // }
    //  Video wala kaam
    // const StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + filename + "1");
    // const Store = StorageRef.put(file, metaData);
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      // duration: 3000
    });
    loader.present();
    StorageRef.put(file, metaData).then((data) => {
      // alert("Put");
      StorageRef.getDownloadURL().then((downloadURL) => {
        // alert(downloadURL + " is download url");
        this.image = downloadURL;

        // alert(this.image);
        // var obj = {
        //   userid: this.ObjHelper.LoggedInUserID,
        //   username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
        //   message: this.image,
        //   messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
        //   status: "Recieved",
        //   isreply: 0,
        //   replymessageid: "",
        //   OrgGroupName: this.GroupName
        // }
        // this.SendFirebase(obj);
        // this.tblOrgGroupDetails = this.tblOrgGroupDetails + "(0,'" + this.image + "','" + new Date().toISOString().split('T')[0] + " " + new Date().toISOString().split('T')[1] + "', 'Recieved' ,'" + this.ObjHelper.LoggedInUserID + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username + "','" + this.GroupName + "' , 1)"
        // this.SendSQLITE2(this.tblOrgGroupDetails);
        loader.dismiss();

      }, (error) => {
        // alert("Error while creating download url");
        // alert(error);
        // alert(error.body);
        // alert(error.message);
        // alert(JSON.stringify(error));
      });
    }, (error) => {
      // alert("Error while putting");
    });

  }

  private cropOptions: RatioCropOptions = {
    quality: 75,
    targetWidth: 1080,
    targetHeight: 1080,
    widthRatio: -1,
    heightRatio: -10
  };

  selectPicture() {

    return this.camera.getPicture({
      allowEdit: false,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      destinationType: this.camera.DestinationType.FILE_URI
    })
      .then((fileUri) => {
        return this.Ccrop.ratioCrop(fileUri, this.cropOptions).then(ImageData => {
          const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 5000
          });
          loader.present();
          let imgFinal = this.generateFromImage(ImageData);
        })
      })

  }

  PasswordType = "password";
  PasswordCheck = false;

  PasswordTypeConfirm = "password";
  PasswordCheckConfirm = false;

  ShowPasswordConfirm() {

    if (this.PasswordCheckConfirm) {
      this.PasswordTypeConfirm = "password";
      this.PasswordCheckConfirm = false;
    } else {
      this.PasswordTypeConfirm = "text";
      this.PasswordCheckConfirm = true;
    }
  }

  ShowPassword() {

    if (this.PasswordCheck) {
      this.PasswordType = "password";
      this.PasswordCheck = false;
    } else {
      this.PasswordType = "text";
      this.PasswordCheck = true;
    }
  }

  async generateFromImage(img, MAX_WIDTH: number = 700, MAX_HEIGHT: number = 700, quality: number = 1) {
    var canvas: any = document.createElement("canvas");
    var image = new Image();

    image.onload = () => {
      var width = image.width;
      var height = image.height;

      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");

      ctx.drawImage(image, 0, 0, width, height);

      // IMPORTANT: 'jpeg' NOT 'jpg'
      var dataUrl = canvas.toDataURL('image/jpeg', quality);

      //Save To Fireabse
      const pictures: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + this.createID());

      pictures.putString(dataUrl, 'data_url').then(() => {
        // const UP: firebase.storage.UploadTask = sotre;
        pictures.getDownloadURL().then((downloadURL) => {

          this.image = downloadURL;
          // this.ImagePreviewVar = downloadURL
          // // document.getElementById('DvScroll').style.opacity = "0.7";
          // document.getElementById('DvScroll').style.backgroundImage = "";
          // document.getElementById('DvScroll').style.backgroundColor = "black";
          // this.ImagePreview = 1;

          // alert(this.image);
        }, (error) => {
          alert("Error " + error);

          alert(JSON.stringify(error));
        });
      });



    }
    image.src = img;
  }

  isEdit = false;
  btnName = "Registration"
  constructor(public navCtrl: NavController, private Ccrop: RatioCrop, public ObjDatabase: DatabaseHelperProvider, private ObjHttp: Http, public navParams: NavParams, private ObjHelper: HelperProvider,
    private transfer: FileTransfer,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController
  ) {

    this.ObjHelper.GetAllCities();
    this.ObjHelper.GetAllVillages();
    this.ObjHelper.GetAllCountries();

  }
  ionViewWillEnter() {

    let Data = this.navParams.get('isEdit');
    if (Data != "0") {
      this.btnName = "Update"
      this.isEdit = true;
      let UserId = this.ObjHelper.LoggedInUserID
      this.ShowUserInfo = this.ObjHelper.UserInfo.find(items => items.userid == UserId);
      const Params = {
        // ID: this.ShowUserInfo.userid
        //Changed by Haroon 30-6-19
        ID: this.ObjHelper.LoggedInUserID
      }
      // this.ObjHelper.ServerHost + "GetUserInfo"
      this.ObjHttp.post(this.ObjHelper.ServerHost + "GetUserInfo", Params).map((res: Response) => res.json()).subscribe((data) => {
        // let jsonString = this.GetJSONString(data);
        let UserInfo = JSON.parse(data.d);

        this.ObjReg.Email = UserInfo[0].username;
        this.ObjReg.Password = UserInfo[0].password
        this.ObjReg.ConfirmPassword = UserInfo[0].password
        this.ObjReg.Name = UserInfo[0].name;
        this.ObjReg.FatherName = UserInfo[0].father_name
        this.ObjReg.Gender = UserInfo[0].sex
        this.ObjReg.DOB = new Date(UserInfo[0].dob).toString();
        this.ObjReg.Country = UserInfo[0].country
        this.ObjReg.Area = UserInfo[0].area
        this.ObjReg.Village = UserInfo[0].indian_village
        this.ObjReg.Phone = UserInfo[0].phone
        this.ObjReg.Profession = UserInfo[0].profession
        this.ObjReg.City = UserInfo[0].city;
        this.ObjReg.State = UserInfo[0].state;
        this.image = UserInfo[0].picname;

      }, (error) => {

      }
      )
    }
  }

  RegisterUser() {

    var Params = {

    }
    if (this.isEdit) {
      Params = {
        ID: this.ShowUserInfo.userid,
        username: this.ObjReg.Email,
        email: this.ObjReg.Email,
        password: this.ObjReg.Password,
        name: this.ObjReg.Name,
        fathername: this.ObjReg.FatherName,
        gender: this.ObjReg.Gender,
        dob: this.ObjReg.DOB,
        location: this.ObjReg.Country,
        area: this.ObjReg.Area,
        village: this.OtherVillage == "" ? this.ObjReg.Village : this.OtherVillage,
        phone: this.ObjReg.Phone,
        profession: this.ObjReg.Profession,
        State: this.ObjReg.State,
        city: this.OtherCity == "" ? this.ObjReg.City : this.OtherCity,
        image: this.image

      }
    } else {

      let FinalDate = "";
      try {
        let a = this.ObjReg.DOB.toString();
        if (a != null && a != undefined) {
          let b = a.split('T')[0].split('-');
          if (b != null && b != undefined)
            FinalDate = b[1] + "-" + b[2] + "-" + b[0];
        }
      }
      catch (e) {
        alert("Invalid Date");
      }
      Params = {
        ID: "0",
        username: this.ObjReg.Email,
        email: this.ObjReg.Email,
        password: this.ObjReg.Password,
        name: this.ObjReg.Name,
        fathername: this.ObjReg.FatherName,
        gender: this.ObjReg.Gender,
        dob: FinalDate,
        location: this.ObjReg.Country,
        area: this.ObjReg.Area,
        village: this.OtherVillage == "" ? this.ObjReg.Village : this.OtherVillage,
        phone: this.ObjReg.Phone,
        profession: this.ObjReg.Profession,
        State: this.ObjReg.State,
        city: this.OtherCity == "" ? this.ObjReg.City : this.OtherCity,
        image: this.image

      }
    }


    this.ObjHttp.post(this.ObjHelper.ServerHost + "RegisterUser", Params).map((res: Response) => res.json()).subscribe((data) => {

      if (data.d == "US") {
        this.navCtrl.setRoot(FoundationChatLogingPage);
      }
      else if (data.d == "UAE") {
        alert("This username or Phone No is already taken. Enter another username.")
      } else if (data.d == "Edited") {
        alert("Profile updated");
        //Added by Haroon
        let Value = this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID);
        if (Value != undefined) {
          Value.city = this.ObjReg.City;
          Value.village = this.ObjReg.Village;
          Value.country = this.ObjReg.Country;

        }
        this.GetAllUserFromServer();
        this.navCtrl.parent.select(0);

      }
      else {
        alert("Something went wrong. Please check all fields");
      }
    }, (error) => {
      alert("Something went wrong. Please check all fields");

    }
    )
  }

  imageURI: any;
  imageFileName: any;
  getImage() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      alert(this.imageURI)
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }

  // RegisterUser() {
  //    
  //   // var aa;
  //   // const file: File = param.target.files[0];

  //   let loader = this.loadingCtrl.create({
  //     content: "Uploading..."
  //   });
  //   loader.present();
  //   const fileTransfer: FileTransferObject = this.transfer.create();

  //   let options: FileUploadOptions = {
  //     fileKey: 'ionicfile',
  //     fileName: 'ionicfile',
  //     chunkedMode: false,
  //     mimeType: "image/jpeg",
  //     params: {
  //       username: this.ObjReg.Email,
  //       email: this.ObjReg.Name,
  //       password: this.ObjReg.Password,
  //       name: this.ObjReg.Name,
  //       fathername: this.ObjReg.FatherName,
  //       gender: this.ObjReg.Gender,
  //       dob: this.ObjReg.DOB,
  //       location: "",
  //       area: this.ObjReg.Area,
  //       village: this.ObjReg.Village,
  //       phone: this.ObjReg.Phone,
  //       profession: this.ObjReg.Profession
  //     },
  //     headers: {}
  //   }

  //   fileTransfer.upload(this.imageURI, this.ObjHelper.ServerHost + "RegisterUser", options)
  //     .then((data) => {
  //       alert(data + " Uploaded Successfully");
  //       this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
  //       loader.dismiss();
  //       this.presentToast("Image uploaded successfully");
  //     }, (err) => {
  //       alert(JSON.stringify(err));
  //       loader.dismiss();
  //       this.presentToast(JSON.stringify(err));
  //     });
  // }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }



}
