import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InoutHomePage } from '../inout-home/inout-home';
import { LoginPage } from '../login/login';
import { FoundationChatMainPage } from '../foundation-chat-main/foundation-chat-main';
import { FoundationProfilePage } from '../foundation-profile/foundation-profile';
import { RegistrationPage } from '../Registration/registration';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  home = FoundationChatMainPage;
  Addtocart = RegistrationPage;
  // WishList = InoutHomePage;
  tabToShow = 0;

  constructor(public navCtrl: NavController) {

  }

}
