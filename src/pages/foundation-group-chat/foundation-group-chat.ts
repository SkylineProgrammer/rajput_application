import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, PopoverController } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { FoundationChatMainPage } from '../foundation-chat-main/foundation-chat-main';
import { HomePage } from '../home/home';
import { Network } from '@ionic-native/network';
import { Content } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { HelperProvider } from '../../providers/helper/helper';
import firebase from 'firebase';
import { AngularFireDatabase } from '@angular/fire/database';
// import { console.logController } from 'ionic-angular';
import { FoundationChatPageDetailsPage } from '../foundation-chat-page-details/foundation-chat-page-details';
import { FoundationProfilePage } from '../foundation-profile/foundation-profile';
import { DatabaseHelperProvider } from '../../providers/database-helper/database-helper';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { FoundationNewChatPage } from '../foundation-new-chat/foundation-new-chat';
import { Clipboard } from '@ionic-native/clipboard';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { InoutHomePage } from '../inout-home/inout-home';
import { CaptureAudioOptions, MediaFile, MediaCapture, CaptureError } from '@ionic-native/media-capture';
import { Media } from '@ionic-native/media';
import { Base64 } from '@ionic-native/base64';
import { File } from '@ionic-native/file';
import { Crop } from '@ionic-native/crop';
import { RatioCrop, RatioCropOptions } from 'ionic-cordova-plugin-ratio-crop';


/**
 * Generated class for the FoundationChatPageDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foundation-group-chat',
  templateUrl: 'foundation-group-chat.html',
})
export class FoundationGroupChatPage {
  AudioText: any;
  Village: any;


  @ViewChild(Content) chatlist: Content

  CurrectGroupID = 1;
  MessageText = "";
  ChatInfo: any;
  ChatMeg: any = [
    // {
    //   Self: true,
    //   Other: false,
    //   Msg: 'MashaAllah u look Handsome and change Msg 2',
    //   status: "Read", // Read ? Send ? Received, Pending
    //   Time_Date: "12:20 am",
    //   MsgType: '0', // 0 = "Text" , 1 = "Image" , 2 = "Video"
    //   Image: '',
    //   Video: '',
    //   UserName: 'Haroon Sattar'
    // },
  ];
  isDisable = false;
  tblGroupDetails = "INSERT INTO tblGroupDetails (groupid, grouptype, isreply , message , messagedate , replymessageid , status , userid , username , village, isSync) VALUES";
  GroupName = "";
  ImagePreview = 0;
  ImagePreviewVar = "https://yt3.ggpht.com/a/AGF-l79ZSQE5TBgbUJnPdvmDK_TYYi8itV6JXqGl8A=s900-mo-c-c0xffffffff-rj-k-no"


  private cropOptions: RatioCropOptions = {
    quality: 75,
    targetWidth: 1080,
    targetHeight: 1080,
    widthRatio: -1,
    heightRatio: -10
  };

  takePicture() {
    return this.camera.getPicture({
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: false,
      correctOrientation: true
    })
      .then((fileUri) => {
        this.Ccrop.ratioCrop(fileUri, this.cropOptions).then(ImageData => {
          const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 5000
          });
          loader.present();
          let imgFinal = this.generateFromImage(ImageData);
        })
      })
  }

  selectPicture() {

    return this.camera.getPicture({
      allowEdit: false,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      destinationType: this.camera.DestinationType.FILE_URI
    })
      .then((fileUri) => {
        return this.Ccrop.ratioCrop(fileUri, this.cropOptions).then(ImageData => {
          const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 5000
          });
          loader.present();
          let imgFinal = this.generateFromImage(ImageData);
        })
      })

  }

  CurrentGroupStatus = ""; // Village // Area // City
  TodayDate = new Date().toISOString().split('T')[0];
  YesterDayDate = new Date(Date.now() - 864e5).toISOString().split('T')[0];
  CurrentGroupUsers = [];
  constructor(
    private filePlugin: File,
    private base64: Base64,
    private media: Media,
    private mediaCapture: MediaCapture,
    public navCtrl: NavController, public navParams: NavParams, private photoViewer: PhotoViewer, private network: Network, private ObjHttp: Http, private ObjHelper: HelperProvider, private database: AngularFireDatabase, public ObjAlertlogCtrl: AlertController, public loadingCtrl: LoadingController, public ObjDatabase: DatabaseHelperProvider, public toastCtrl: ToastController, private clipboard: Clipboard,
    private camera: Camera,
    private crop: Crop,
    public popoverCtrl: PopoverController,
    private Ccrop: RatioCrop
  ) {
    this.GetText();
    var abc = this.ObjHelper.isAdmin

    //Ibtesam's Code
    this.ChatInfo = navParams.get('data');

    this.CurrectGroupID = navParams.get('GroupID')
    this.GroupName = navParams.get("GroupName");
    this.isDisable = this.ObjHelper.isBlocked == "0" ? false : true;

    if (this.CurrectGroupID == 5 && this.ObjHelper.LoggedInUserID == 113)
      this.isDisable = false;
    else if (this.CurrectGroupID == 5 && this.ObjHelper.LoggedInUserID != 113)
      this.isDisable = true;



    this.Village = this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID);
    if (this.Village != undefined) {
      this.CurrentGroupStatus = this.CurrectGroupID == 1 ? this.Village.village : this.CurrectGroupID == 2 ? this.Village.area : this.CurrectGroupID == 3 ? this.Village.city : "";
    }



    if (this.CurrectGroupID == 1)
      this.CurrentGroupUsers = this.ObjHelper.UserInfo.filter(items => {
        if (items.village != null)
          return items.village.toLowerCase() == this.CurrentGroupStatus.toLowerCase()
      });

    else if (this.CurrectGroupID == 2)
      this.CurrentGroupUsers = this.ObjHelper.UserInfo.filter(items => {
        if (items.area != null)
          return items.area.toLowerCase() == this.CurrentGroupStatus.toLowerCase()
      });

    else if (this.CurrectGroupID == 3)
      this.CurrentGroupUsers = this.ObjHelper.UserInfo.filter(items => {
        if (items.city != null)
          return items.city.toLowerCase() == this.CurrentGroupStatus.toLowerCase()
      });

    else if (this.CurrectGroupID == 4)
      this.CurrentGroupUsers = this.ObjHelper.UserInfo.filter(items => {
        return items.isadmin == "1";
      });


    let TestNet = this.network.type;
    if (TestNet == "none") {
      // let Village = this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID);
      // if (Village != undefined) {
      //   Village = this.CurrectGroupID == 1 ? Village.village : this.CurrectGroupID == 2 ? Village.area : this.CurrectGroupID == 3 ? Village.city : "";
      // }
      // this.ObjDatabase.SelectQuerySqlite("select * from tblGroupDetails").then(data => {
      var query = "select * from tblGroupDetails where groupid = " + Number(this.CurrectGroupID) + " and village = '" + this.CurrentGroupStatus + "'";
      this.ObjDatabase.SelectQuerySqlite(query).then(data => {
        // 
        this.ChatMeg = data;
      })
    } else {
      this.isSyncTrue();
    }
    // this.GetGroupDetailChat();


  }


  GetText() {
    this.database.object('/Validation/').valueChanges().subscribe(data => {

      let arr = Object.keys(data);

      for (var i = 0; i < arr.length; i++) {
        const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
        this.AudioText = object2.Text;
      }


    })
  }

  Audio = "";
  OpenMic() {

    if (this.AudioText == "") {

       
      let options: CaptureAudioOptions = { limit: 1 };
      this.mediaCapture.captureAudio(options)
        .then((data: MediaFile[]) => {
          debugger
          this.Audio = data[0].fullPath;
          let Name = data[0].name;
          // Name = Name.split('.')[0] + ".mp3";
          let storageRef = firebase.storage().ref();
          let metadata = {
            contentType: 'audio/mp3',
          };

          let filePath = data[0].fullPath;
          this.base64.encodeFile(filePath).then(
            (base64: any) => {
               
              console.log('file base64 encoding: ' + base64);
            }
            , (error) => {
               
            }
          );
          // filePath = "file:///storage/emulated/0/VoiceRecorder/my_sounds/";
          filePath = data[0].fullPath.split(data[0].fullPath.split('/')[data[0].fullPath.split('/').length - 1])[0];
          this.filePlugin.readAsDataURL(filePath, Name).then((file) => {
             
            let voiceRef = storageRef.child('/photos/audios/' + this.createID()).putString(file, firebase.storage.StringFormat.DATA_URL);
            voiceRef.on(firebase.storage.TaskEvent.STATE_CHANGED, (snapshot) => {
              console.log("uploading");
            }, (e) => {
               
              console.log(JSON.stringify(e, null, 2));
            }, () => {
              // let DURL = "https://firebasestorage.googleapis.com/v0/b/" + voiceRef.snapshot.metadata.bucket + "/o/";
              var rep = voiceRef.snapshot.metadata.fullPath.replace('/', '%2F');
              let DURL = "https://firebasestorage.googleapis.com/v0/b/" + voiceRef.snapshot.metadata.bucket + "/o/" + rep.replace('/', '%2F');

              let DURL2 = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F250342";

              this.ObjHttp.get(DURL)
                .map((res: Response) => res.json())
                .subscribe((data) => {
                  DURL = DURL + "?alt=media&token=" + data.downloadTokens;
                  this.image = DURL;
                   
                  this.SendMsgToFirebase(this.image);
                  // this.SendMsg();
                  // let jsonString: string = this.GetJsonString(data);
                  // let Data = JSON.parse(jsonString);
                  //  
                },
                  (error) => {
                     
                    console.log("Error in Subscribe." + error);
                  });

              // let DURL = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F020863?alt=media&token=90723f01-1a20-4049-9ddc-09c3916df539"
              // var downloadURL = voiceRef.snapshot.downloadURL;
              // this.Audio = DURL;
              // this.Audio = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F499007?alt=media"

              var str = "<audio controls><source src=' " + this.Audio + " ' type='audio/amr'></audio>";
              // document.getElementById('audio').innerHTML = str;
            });
          }
            , (error) => {
               
              console.log(error)
              console.log(JSON.stringify(error));
            }
          );


        }, (err: CaptureError) => {
           
        });

    } else {
      alert(this.AudioText);
    }
  }

  OpenPopOver(myEvent) {
    //  this.navCtrl.push(InoutHomePage, { Users: this.CurrentGroupUsers });
    // this.CurrectGroupID = navParams.get('GroupID')
    // this.GroupName = navParams.get("GroupName");

    let popover = this.popoverCtrl.create(InoutHomePage, { Users: this.CurrentGroupUsers, GroupID: this.CurrectGroupID, GroupName: this.Village, GroupDteails: this.ChatMeg });
    // let ev = {
    //   target : {
    //     getBoundingClientRect : () => {
    //       return {
    //         top: 100
    //       };
    //     }
    //   }
    // };
    popover.present({
      ev: myEvent
    });
  }
  isSyncTrue() {
    console.log("Inside IsSync Method");
    this.ObjDatabase.SelectQuerySqlite("select * from tblGroupDetails where isSync = 0").then((data: any) => {
      console.log("Got Data from tblGroupDetails where isSync = 0");
      data.forEach(element => {
        this.database.list("/TblGroupDetails/").push({
          userid: element.userid,
          username: element.username,
          groupid: element.groupid,
          message: element.message,
          messagedate: element.messagedate,
          grouptype: element.grouptype,
          status: "Send",
          isreply: element.isreply,
          replymessageid: element.replymessageid,
          village: element.village
        })
        // .then((data) => {
        // console.log("Selecting IsSync From Ibtesam's Code");
        // this.ObjDatabase.SelectQuerySqlite("update tblGroupDetails set isSync = 1").then((data: any) => {
        //   // //this.presentToast("Syncing Complete");
        //   console.log("syncing completed");
        //   this.GetGroupDetailFromFirebase();
        // }, (error) => {
        //   console.log("Error while updating Sqlite table");
        //   console.log(error);
        // })
        // }, (error) => {
        // console.log("Error while pushing data to firebase");
        // console.log(error);
        // });
      });
    }, (error) => {
      console.log("Error while selecting tblGroupDetails where isSync = 0 because");
      console.log(error);
    })


    console.log("Updating Sqlite any way");
    this.ObjDatabase.SelectQuerySqlite("update tblGroupDetails set isSync = 1").then((data: any) => {
      // //this.presentToast("Syncing Complete");
      console.log("syncing completed");
      this.GetGroupDetailFromFirebase();
    }, (error) => {
      console.log("Error while updating Sqlite table");
      console.log(error);
    })

    this.GetGroupDetailFromFirebase();

  }

  presentToast(param) {
    const toast = this.toastCtrl.create({
      message: param,
      duration: 9000,
      position: 'top'
    });
    toast.present();

  }

  ImageClick(param) {
    this.photoViewer.show(param);
  }

  GoBack() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoundationChatPageDetailsPage');
  }

  PressPersonChat(param, data) {
    this.showConfirm(data);
  }

  showConfirm(data) {
    const confirm = this.ObjAlertlogCtrl.create({
      title: '',
      message: 'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
      buttons: [
        {
          text: 'View kProfile',
          handler: () => {
            this.navCtrl.push(FoundationProfilePage, { userid: "175" })
          }
        },
        {
          text: 'Perosnal Chat',
          handler: () => {
            this.navCtrl.push(FoundationChatPageDetailsPage, { UserData: data })
          }
        }
      ]
    });
    confirm.present();
  }

  toggled: boolean = false;
  // emojitext: string;

  handleSelection(event) {
    // this.emojitext = this.emojitext + " " + event.char;
    this.MessageText = this.MessageText + " " + event.char;
    this.toggled = false;
  }

  BtnSendMsgClick() {

    if (this.ObjHelper.isReply == 1) {
      if (this.MessageText != "") {
        let text = this.MessageText;
        this.database.list("/TblGroupDetails/").push({
          userid: this.ObjHelper.LoggedInUserID,
          username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
          groupid: this.CurrectGroupID,
          message: this.MessageText,
          messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
          grouptype: this.CurrentGroupType,
          status: "Recieved",
          isreply: 1,
          replymessageid: this.ObjHelper.isReplyData[0].message,
          village: this.CurrentGroupStatus
        }).then(data => {
          this.MessageText = "";
          this.ObjHelper.isReply = 0;
          this.ConversationDivClick()
        })
      }
    } else {
      if (this.MessageText != "") {
        let network = this.network.type;
        if (network == "none") {
          this.tblGroupDetails = this.tblGroupDetails + "(" + this.CurrectGroupID + ",'" + this.CurrentGroupType + "','" + 0 + "','" + this.MessageText + "','" + new Date().toISOString().split('T')[0] + "','" + "" + "','" + "Pending" + "','" + this.ObjHelper.LoggedInUserID + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).village + "' , 0),"
          var query = this.tblGroupDetails.slice(0, -1);
          this.ObjDatabase.InsertQuerySqlite(query, []).then(data => {
            this.ChatMeg.push({
              userid: this.ObjHelper.LoggedInUserID,
              // username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
              username: this.ObjHelper.LoggedUserName,
              groupid: this.CurrectGroupID,
              message: this.MessageText,
              messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
              grouptype: this.CurrentGroupType,
              status: "Pending",
              isreply: 0,
              replymessageid: "",
              village: this.CurrentGroupStatus
            })
            // this.SelectedChat.length = 0;
            // this.SelectedID.length = 0;
            this.ConversationDivClick()

          })
        }
        else {
          if (this.MessageText != "") {
            this.ChatMeg.push({
              username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
              userid: this.ObjHelper.LoggedInUserID,
              image: this.ObjHelper.LoggedInUserImage,
              message: this.MessageText,
              status: 'Pending'
            });

            // this.SendMsg(this.MessageText);
            this.SendMsgToFirebase(this.MessageText)
            this.MessageText = "";

          }
          else {
            // Show Error Message
          }
        }
      }
    }


  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
  }

  createID() {
    let date = new Date().getTime().toString();
    date = date.substring(date.length - 6);
    return date
  }
  image = "";

  AddToUserUnreadMsgs(ReceiverUserIDs) {

    for (let i = 0; i < ReceiverUserIDs.length; i++) {
      this.database.list("/TblGroupCounter/").push({
        senderid: this.ObjHelper.LoggedInUserID,
        recieverid: Number(ReceiverUserIDs[i]),
        IsRead: false,
        Count: 1,
        groupid: this.CurrectGroupID,
        grouptype: this.CurrentGroupType,
        village: this.CurrentGroupStatus
      }).then((data) => {
        this.ChatMeg[this.ChatMeg.length - 1].status = 'Recieved';
      }, (error) => { alert("Error while adding message into firebase") });

    }

  }


  async generateFromImage(img, MAX_WIDTH: number = 700, MAX_HEIGHT: number = 700, quality: number = 1, ) {
    var canvas: any = document.createElement("canvas");
    var image = new Image();

    image.onload = () => {
      var width = image.width;
      var height = image.height;

      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");

      ctx.drawImage(image, 0, 0, width, height);

      // IMPORTANT: 'jpeg' NOT 'jpg'
      var dataUrl = canvas.toDataURL('image/jpeg', quality);

      //Save To Fireabse
      const pictures: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + this.createID());

      pictures.putString(dataUrl, 'data_url').then(() => {
        // const UP: firebase.storage.UploadTask = sotre;
        pictures.getDownloadURL().then((downloadURL) => {

          this.image = downloadURL;
          // this.ImagePreviewVar = downloadURL;
          // document.getElementById('DvScroll').style.backgroundImage = "";
          // document.getElementById('DvScroll').style.backgroundColor = "black";
          // this.ImagePreview = 1;

          // this.SendMsg()
          this.SendMsgToFirebase(this.image);
        }, (error) => {
          alert("Error " + error);

          alert(JSON.stringify(error));
        });
      });



    }
    image.src = img;
  }


  async OpenCamera() {

    try {





      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }


      this.camera.getPicture(options).then((Image) => {
         
        this.crop.crop(Image, { quality: 75 })
          .then(
            newImage => {
              let imgFinal = this.generateFromImage(newImage);
            },
            error => alert('Error cropping image' + error)
          );
      })










      // setTimeout(() => {
      //   const UP: firebase.storage.UploadTask = sotre;
      //   UP.snapshot.ref.getDownloadURL().then((downloadURL) => {
      //      
      //     this.image = downloadURL;

      //     this.SendMsg()
      //   }, (error) => {
      //     alert("Error " + error);

      //     alert(JSON.stringify(error));
      //   });
      // }, 20000);

    } catch (error) {
      alert("Catch " + error);
      alert(JSON.stringify(error));
    }
  }

  SendImageconfirm() {
    this.SendMsgToFirebase(this.image);
    this.BackMsg();
  }

  onFileChanged(param) {
    var aa;
    const file: any = param.target.files[0];
    const metaData = { 'contentType': file.type };
    let filename = this.createID();

    //  Video wala kaam
    if (file.type == "video/mp4") {
      var StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/vidoes/' + this.createID());
    } else {
      var StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + this.createID());
    }
    //  Video wala kaam

    // const StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + filename + "1");
    // const Store = StorageRef.put(file, metaData);
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      // duration: 3000
    });
    loader.present();
    StorageRef.put(file, metaData).then((data) => {
      // alert("Put");
      StorageRef.getDownloadURL().then((downloadURL) => {
        // alert(downloadURL + " is download url");
        this.image = downloadURL;
        this.ImagePreviewVar = downloadURL;
        document.getElementById('DvScroll').style.backgroundImage = "";
        document.getElementById('DvScroll').style.backgroundColor = "black";
        this.ImagePreview = 1;

        // this.SendMsgToFirebase(this.image)
        loader.dismiss();

      }, (error) => {
        // alert("Error while creating download url");
        // alert(error);
        // alert(error.body);
        // alert(error.message);
        // alert(JSON.stringify(error));
      });
    }, (error) => {
      // alert("Error while putting");
    });
    // this.presentLoading();
    // setTimeout(() => {
    //   const UP: firebase.storage.UploadTask = Store;
    //   UP.snapshot.ref.getDownloadURL().then((downloadURL) => {
    //     alert(downloadURL + " is download url");
    //     this.image = downloadURL;
    //     this.SendMsgToFirebase(this.image)
    //   }, (error) => {
    //     alert("Error while creating download url");
    //     alert(error);
    //     alert(error.body);
    //     alert(error.message);
    //     alert(JSON.stringify(error));
    //   });
    // }, 10000);

    // const Store2 = StorageRef.put(file, metaData).then((snapshot) => {
    //   alert("Download URL is " + snapshot.downloadURL);
    //   alert(snapshot);
    //   alert(JSON.stringify(snapshot));
    //   this.image = snapshot.downloadURL;
    //   this.SendMsgToFirebase(this.image);
    // }
    // , (error) => {
    //   alert("Error in new method");
    //   alert(error.message);
    //   alert(JSON.stringify(error));
    // }
    // );

    // setTimeout(() => {
    //   this.ImageLink = aa;
    // }, 8000);

  }

  BackMsg() {
    document.getElementById('DvScroll').style.opacity = "1";
    document.getElementById('DvScroll').style.backgroundImage = "url('https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png')";
    document.getElementById('DvScroll').style.backgroundColor = "";
    this.ImagePreview = 0;
  }


  CurrentGroupType = "";
  SendMsgToFirebase(MessageText) {
    this.database.list("/TblGroupDetails/").push({
      userid: this.ObjHelper.LoggedInUserID,
      username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username,
      groupid: this.CurrectGroupID,
      message: MessageText,
      messagedate: new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
      grouptype: this.CurrentGroupType,
      status: "Recieved",
      isreply: 0,
      replymessageid: "",
      village: this.CurrentGroupStatus
    }).then((data) => {
      this.ChatMeg[this.ChatMeg.length - 1].status = 'Recieved';
      // this.SelectedChat.length = 0;
      // this.SelectedID.length = 0;
      this.ConversationDivClick()

    }, (error) => { alert("Error while adding message into firebase") });
    // let PlayerIds = []
    // this.CurrentGroupStatus = this.CurrectGroupID == 1 ? Village.village : this.CurrectGroupID == 2 ? Village.area : this.CurrectGroupID == 3 ? Village.city : "";
    let URL = "";
    if (this.CurrectGroupID == 1)
      URL = "SendVillageNotification?Message=" + this.MessageText + "&Village=" + this.CurrentGroupStatus + "&LoggedInUserID=" + this.ObjHelper.LoggedInUserID;
    else if (this.CurrectGroupID == 2)
      URL = "SendAreaNotification?Message=" + this.MessageText + "&Area=" + this.CurrentGroupStatus + "&LoggedInUserID=" + this.ObjHelper.LoggedInUserID;
    else if (this.CurrectGroupID == 3)
      URL = "SendCityNotification?Message=" + this.MessageText + "&City=" + this.CurrentGroupStatus + "&LoggedInUserID=" + this.ObjHelper.LoggedInUserID;
    else if (this.CurrectGroupID == 4)
      URL = "SendAllAdminNotification?Message=" + this.MessageText + "&LoggedInUserID=" + this.ObjHelper.LoggedInUserID
    else if (this.CurrectGroupID == 5)
      URL = "SendMasterAdminNotification?Message=" + this.MessageText;


    this.ObjHttp.get(this.ObjHelper.ServerHost + URL)
      .map((res: Response) => res.text()).subscribe((data) => {

        let jsonString = this.GetJSONString(data);
        let jsonString2 = jsonString.split("<int>");
        let UserIds = [];
        for (let i = 0; i < jsonString2.length; i++) {
          if (i != 0)
            UserIds.push(jsonString2[i] = jsonString2[i].split("</")[0]);
        }

        if (UserIds.length > 0) {
          this.AddToUserUnreadMsgs(UserIds);
        }
      }
        , (error) => {

        }
      )

    this.image = "";

  }


  SelectedChat = [];
  SelectedID = [];
  presentActionSheet(param, selectedid, SorR) {
    selectedid = SorR + selectedid;
    // this.SelectedChat.push(param);

    // if (this.SelectedID != "" && this.SelectedID != selectedid) {
    //   document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
    //   this.SelectedID = "";
    // }

    // ============
    // if (this.SelectedID == selectedid) {
    //   document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
    //   this.IsActionPresented = false;
    //   this.SelectedID = "";
    // }
    // else {
    //   if (this.SelectedID != "") {
    //     document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
    //     this.SelectedID = "";
    //   }
    //   else {
    //     this.IsActionPresented = true;
    //     this.SelectedID = selectedid;
    //     document.getElementById(this.SelectedID).style.backgroundColor = "cornflowerblue";
    //   }
    if (this.SelectedID.find(items => items == selectedid) != undefined) {
      document.getElementById(this.SelectedID.find(items => items == selectedid)).style.backgroundColor = this.SelectedID.find(items => items == selectedid)[0] == 'S' ? "#e1ffc7" : "white";
      this.IsActionPresented = false;
      this.SelectedID.splice(this.SelectedID.indexOf(this.SelectedID.find(items => items == selectedid)), 1);
      this.SelectedChat.splice(this.SelectedChat.indexOf(this.SelectedChat.find(items => items == param)), 1);
      // this.SelectedChat.push(param);

      // this.SelectedID = "";
    }
    else {
      // if (this.SelectedID.length > 0) {
      //   document.getElementById(selectedid).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
      //   // this.SelectedID = "";
      //   this.SelectedID.splice(this.SelectedID.indexOf(this.SelectedID.find(items => items == selectedid)), 1);

      // }
      // else {
      this.IsActionPresented = true;
      this.SelectedID.push(selectedid);
      this.SelectedChat.push(param);
      document.getElementById(selectedid).style.backgroundColor = "cornflowerblue";
      // }
    }


    // const actionSheet = this.actionSheetCtrl.create({
    //   title: 'Select Your Activity',
    //   buttons: [
    //     {
    //       text: 'Copy',
    //       role: 'destructive',
    //       handler: () => {
    //         this.clipboard.copy(param.message);
    //         console.log('Destructive clicked');
    //       }
    //     }, {
    //       text: 'Forward',
    //       handler: () => {
    //         if (this.network.type != "this.network.type != ") {
    //           this.clipboard.copy(param.message)
    //           // .then(() => {
    //           //     this.navCtrl.push(FoundationNewChatPage, { isForwardText: param.message });
    //           // });

    //           this.clipboard.paste().then(
    //             (resolve: string) => {
    //               var text = resolve;
    //                
    //               this.navCtrl.push(FoundationNewChatPage, { isForwardText: text });
    //               // this.clipboard.clear();
    //             },
    //             (reject: string) => {
    //               //  alert('Error: ' + reject);
    //             }
    //           );
    //           // this.navCtrl.push(FoundationNewChatPage, { isForwardText: "ForwardText" });

    //         }
    //         else {
    //           alert("Network Error");
    //         }
    //         console.log('Archive clicked');
    //       }
    //     }, {
    //       text: 'Delete',
    //       handler: () => {
    //         // this.database.list('/TblChat/').update("param.Key", {
    //         //   Name: 'arqam',
    //         //   pass: 234
    //         // })

    //         // bus Messge ki ID dedenge to delete hgyega 
    //          
    //         if (param.userid == this.ObjHelper.LoggedInUserID) {
    //           this.database.list('/TblChatDetails/').remove(param.key).then((data) => {
    //              
    //           }, (error) => {
    //              
    //           }
    //           );
    //         }
    //         else
    //           alert("Cannot delete other user messages");

    //         console.log('Archive clicked');
    //       }
    //     }, {
    //       text: 'Cancel',
    //       role: 'cancel',
    //       handler: () => {
    //         console.log('Cancel clicked');
    //       }
    //     }
    //   ]
    // });
    // actionSheet.onDidDismiss(() => {
    //    
    //   this.IsActionPresented = false;
    // })
    // actionSheet.present();
  }

  ConversationDivClick() {
    if (!this.RealClick) {

      this.toggled = false;
      this.IsActionPresented = false
      // if (this.SelectedID != "") {
      //   document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
      //   this.SelectedID = ""
      // }
      if (this.SelectedID.length > 0) {
        for (let i = 0; i < this.SelectedID.length; i++) {
          document.getElementById(this.SelectedID[i]).style.backgroundColor = this.SelectedID[i][0] == 'S' ? "#e1ffc7" : "white";
        }
        this.SelectedID = [];
        this.SelectedChat = [];
        this.ObjHelper.isReply = 0;
        this.ObjHelper.isReplyData = [];
        this.ObjHelper.isReplyText = "";
      }
    } else {
      this.RealClick = false;
    }
  }


  // newSK
  ReplyClick() {
    this.ObjHelper.isReply = 1;
    this.ObjHelper.isReplyData = this.SelectedChat;
    if (this.ObjHelper.isReplyData[0].message.split(':')[0] == "https") {
      // this.presentToast("Reply Against Your Selected Iamge");
      this.ObjHelper.isReplyText = 'Reply Against Your Selected Iamge';

    } else {
      // this.presentToast("Reply Against ( " + this.ObjHelper.isReplyData[0].message + " )");
      this.ObjHelper.isReplyText = this.ObjHelper.isReplyData[0].message.substring(0, 30) + "..";
    }
  }

  ForwardClick() {
    let messages = [];

    for (let i = 0; i < this.SelectedChat.length; i++) {
      messages.push(this.SelectedChat[i].message);
    }
    if (this.network.type != "none") {
      // this.clipboard.copy(this.SelectedChat.message)
      // .then(() => {
      //     this.navCtrl.push(FoundationNewChatPage, { isForwardText: param.message });
      // });

      // this.clipboard.paste().then(
      // (resolve: string) => {
      // var text = resolve;

      this.navCtrl.push(FoundationNewChatPage, { isForwardText: messages }).then(() => {
        this.IsActionPresented = false;

        if (this.SelectedChat.length == this.SelectedID.length) {
          for (let i = 0; i < this.SelectedChat.length; i++) {
            document.getElementById(this.SelectedID[i]).style.backgroundColor = this.SelectedID[i][0] == 'S' ? "#e1ffc7" : "white";
          }
          this.SelectedChat = [];
          this.SelectedID = []
        }
      });
      // this.clipboard.clear();
      // },
      // (reject: string) => {
      //  alert('Error: ' + reject);
      // }
      // );
      // this.navCtrl.push(FoundationNewChatPage, { isForwardText: "ForwardText" });

    }
    else {
      alert("Network Error");
    }



    // if (this.network.type != "none") {
    //   this.clipboard.copy(this.SelectedChat.message)
    //   // .then(() => {
    //   //     this.navCtrl.push(FoundationNewChatPage, { isForwardText: param.message });
    //   // });

    //   this.clipboard.paste().then(
    //     (resolve: string) => {
    //       var text = resolve;
    //        
    //       this.navCtrl.push(FoundationNewChatPage, { isForwardText: text }).then(() => {
    //         this.IsActionPresented = false;
    //       });
    //       // this.clipboard.clear();
    //     },
    //     (reject: string) => {
    //       //  alert('Error: ' + reject);
    //     }
    //   );
    //   // this.navCtrl.push(FoundationNewChatPage, { isForwardText: "ForwardText" });

    // }
    // else {
    //   alert("Network Error");
    // }
  }
  IsActionPresented = false;
  DeleteClick() {

    for (let i = 0; i < this.SelectedChat.length; i++) {
      if (this.SelectedChat[i].userid == this.ObjHelper.LoggedInUserID) {
        let param = this.SelectedChat[i];
        this.database.list('/TblGroupDetails/').remove(this.SelectedChat[i].Key).then((data) => {

          this.IsActionPresented = false;
          this.SelectedID = [];
          this.SelectedChat.splice(this.SelectedChat.indexOf(this.SelectedChat.find(items => items == param)), 1);
        }, (error) => {

        }
        );
      }
      else
        alert("Cannot delete other user messages");

    }

    // if (this.SelectedChat.userid == this.ObjHelper.LoggedInUserID) {
    //   this.database.list('/TblGroupDetails/').remove(this.SelectedChat.Key).then((data) => {
    //      
    //     this.IsActionPresented = false;
    //     this.SelectedID = "";
    //   }, (error) => {
    //      
    //   }
    //   );
    // }
    // else
    //   alert("Cannot delete other user messages");

  }

  CopyClick() {
    this.clipboard.copy(this.SelectedChat.length > 0 ? this.SelectedChat[0].message : "").then(() => {
      this.IsActionPresented = false;
    });
  }

  GetJSONString(Json: string) {
    return Json.substring(Json.indexOf("<int>"), Json.indexOf("</ArrayOfInt>"));
  }
  ShowGroupUsers() {
    this.navCtrl.push(FoundationNewChatPage, { Users: this.CurrentGroupUsers });
  }
  UpdateUnReadMsg(UserID) {


    var ref = firebase.database().ref().child('TblGroupCounter');
    // this.isnew = true;
    ref.orderByChild('recieverid').startAt(UserID).endAt(UserID).on('child_added', (snapshot) => {

      // this.ChatMeg = snapshot.val();
      let Value = snapshot.val();
      // if (Value.userid == this.ObjHelper.LoggedInUserID) {

      // if (UnReadMsgCount == undefined)
      //   UnReadMsgCount = 0;
      // UnReadMsgCount = Value.UnreadMsgQun;
      if (this.navCtrl.getActive().component == FoundationGroupChatPage && Value.groupid == this.CurrectGroupID) {
        this.database.list('/TblGroupCounter/').remove(snapshot.key).then((data) => {
          // alert("Delete");
        })
      }
      // }
    })

  }


  // GetGroupDetailFromFirebase() {

  //   // let Village = this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).village;
  //   this.database.object('/TblGroupDetails/').valueChanges().subscribe(data => {
  //     this.ChatMeg = [];
  //     let arr = Object.keys(data);
  //     // this.BindArray = data;
  //     for (var i = 0; i < arr.length; i++) {
  //       const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
  //        
  //       if (object2.village == this.CurrentGroupStatus && object2.groupid == this.CurrectGroupID)
  //         this.ChatMeg.push(object2);
  //     }
  //     this.UpdateUnReadMsg(this.ObjHelper.LoggedInUserID);

  //   })
  // }


  // Commmnt by SK

  checkUserClear(param) {

    var Keys = Object.keys(param);
    var res = 0;

    for (var loop = 0; loop < Keys.length; loop++) {

      if (Keys[loop] == "groupid" || Keys[loop] == "grouptype" || Keys[loop] == "isreply" || Keys[loop] == "message" || Keys[loop] == "messagedate" ||
        Keys[loop] == "replymessageid" || Keys[loop] == "status" || Keys[loop] == "userid" || Keys[loop] == "username" || Keys[loop] == "village"
      ) {

      } else {
        if (param[Keys[loop]].ClearUserID == this.ObjHelper.LoggedInUserID) {
          res = 1;
        }
      }
    }

    return res;
  }

  GetGroupDetailFromFirebase() {

    // let Village = this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).village;

    this.database.object('/TblGroupDetails/').valueChanges().subscribe(data => {
      this.ChatMeg = [];
      let AllMsgs = [];
      let arr = [];
      if (data != null) arr = Object.keys(data);

      // this.BindArray = data;

      for (var i = 0; i < arr.length; i++) {
        const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
        if (this.checkUserClear(data[arr[i]]) == 1) {

        } else {

          if (object2.village == this.CurrentGroupStatus && object2.groupid == this.CurrectGroupID)
            AllMsgs.push(object2);

          // this.ChatMeg.push(object2);

          if (i == (arr.length - 1) && AllMsgs.length > 1) {
            let IsFirst = false;
            const group = AllMsgs.reduce((Date, MsgObject, i) => {

              if (!IsFirst) {
                Date = [];
                Date[AllMsgs[0].messagedate.split(' ')[0]] = [];
                Date[AllMsgs[0].messagedate.split(' ')[0]].push(AllMsgs[0]);
              }
              IsFirst = true;
              let MsgDate = MsgObject.messagedate.split(' ')[0];
              if (!Date[MsgDate])
                Date[MsgDate] = [];

              Date[MsgDate].push(MsgObject);
              return Date;

            })

            // this.ChatMeg = group;
            // console.clear();

            if (IsFirst) {
              this.ChatMeg = Object.keys(group).map((res) => {

                console.log(group + res);
                return {
                  MsgDate: res,
                  Msgs: group[res]
                }
              });
            }
          }
          else if (AllMsgs.length > 0) {


            this.ChatMeg[0] = {
              MsgDate: AllMsgs[0].messagedate.split(' ')[0],
              Msgs: AllMsgs
            };
            // this.ChatMeg = {
            //   MsgDate: new Date().toISOString().split('T')[0],
            //   Msgs: AllMsgs[0]
            // }
          }

        }
      }

      this.UpdateUnReadMsg(this.ObjHelper.LoggedInUserID);

    })
  }

  RealClick = false;

  SelectMsgClick(param, index, SorR) {
    this.RealClick = true;
    if (this.SelectedID.length > 0)
      this.presentActionSheet(param, index, SorR);
    else {
      this.RealClick = false;
      this.SelectedChat.length = 0;
    }
  }


  // Chat Details 
  // 1) Village
  // 2) City
  // 3) Area
  // 4) Admin Chat Disabled
  // 4) Admin Chats 




}

