import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoundationGroupChatPage } from './foundation-group-chat';

@NgModule({
  declarations: [
    FoundationGroupChatPage,
  ],
  imports: [
    IonicPageModule.forChild(FoundationGroupChatPage),
  ],
})
export class FoundationGroupChatPageModule {}
