import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoundationChatPageDetailsPage } from './foundation-chat-page-details';

@NgModule({
  declarations: [
    FoundationChatPageDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(FoundationChatPageDetailsPage),
  ],
})
export class FoundationChatPageDetailsPageModule {}
