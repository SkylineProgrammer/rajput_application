import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Content, ActionSheetController } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { FoundationChatMainPage } from '../foundation-chat-main/foundation-chat-main';
import { HomePage } from '../home/home';
import { Network } from '@ionic-native/network';
import firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { HelperProvider } from '../../providers/helper/helper';

import 'rxjs/add/operator/first';
import { FoundationChatLogingPage } from '../foundation-chat-loging/foundation-chat-loging';
import { MAX_LENGTH_VALIDATOR } from '@angular/forms/src/directives/validators';
import { DatabaseHelperProvider } from '../../providers/database-helper/database-helper';
import { Http, Response } from '@angular/http';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FoundationNewChatPage } from '../foundation-new-chat/foundation-new-chat';
// import { File } from '@ionic-native/file';
import { File } from '@ionic-native/file';
import { Clipboard } from '@ionic-native/clipboard';
import { CaptureAudioOptions, MediaFile, MediaCapture, CaptureError } from '@ionic-native/media-capture';
import { Base64 } from '@ionic-native/base64';
import { Media, MediaObject } from '@ionic-native/media';
import { Crop } from '@ionic-native/crop';
import { RatioCropOptions, RatioCrop } from 'ionic-cordova-plugin-ratio-crop';
// import { AngularCropperjsModule, CropperComponent } from 'angular-cropperjs';
// import { MediaCaptureOriginal } from '@ionic-native/media-capture';
// import { IonicStorageModule } from '@ionic/storage';
// import { MediaOriginal } from '@ionic-native/media';


// const MEDIA_FILES_KEY = 'mediaFiles';


/**
 * Generated class for the FoundationChatPageDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foundation-chat-page-details',
  templateUrl: 'foundation-chat-page-details.html',
})

export class FoundationChatPageDetailsPage {
  AudioText: any;


  @ViewChild(Content) content: Content;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  // @ViewChild('angularcropper') private angularCropper: CropperComponent;
  // cropperOptions: any;
  // croppedImage = null;
  // myImage = null;
  // scaleValX = 1;
  // scaleValY = 1;


  recieverid = "";
  chatID: any;

  MessageText = "";
  MessageRows = 4;

  ChatInfo: any;
  isnew = false;

  CurrentUserName = "";
  CurrentUserImage = "";

  LastSeen = "";
  ImagePreview = 0;
  ImagePreviewVar = "https://yt3.ggpht.com/a/AGF-l79ZSQE5TBgbUJnPdvmDK_TYYi8itV6JXqGl8A=s900-mo-c-c0xffffffff-rj-k-no"
  // ChatMeg = {Msgs : []};

  ChatMeg: any = [
    // Msgs: []
    // {
    //   Self: true,
    //   Other: false,
    //   Msg: 'Assalam-o-Alikum',
    //   status: "Read", // Read ? Send ? Received, Pending
    //   Time_Date: "12:15 am",
    //   MsgType: '0', // 0 = "Text" , 1 = "Image" , 2 = "Video"
    //   Image: '',
    //   Video: ''
    // }
  ];

  imageURI: any;
  imageFileName: any;
  IsActionPresented = false;
  UserAllInfo: any;

  IncreaseRows(value) {
    debugger;
    let ScrollHeight = document.getElementsByName("input")[0].scrollHeight;
    if (ScrollHeight > 50) {
      this.MessageRows += 1;
    }
  }

  isSyncTrue() {

    this.ObjDatabase.SelectQuerySqlite("select * from  tblChatDetails set isSync = 0").then((data: any) => {

      if (data.length > 0) {
        data.forEach(element => {

          this.database.list("/TblChatDetails/").push({
            userid: element.userid,
            username: element.username,
            reciverid: element.reciverid,
            message: element.message,
            messagedate: element.messagedate,
            status: "Send",
            isReply: element.isreply,
            replymessageid: element.replymessageid,
            chatid: element.chatid
          }).then((data) => {
            this.ObjDatabase.SelectQuerySqlite("update tblChatDetails set isSync = 1").then((data: any) => {
              this.GetRealTimeChats();
            })
          });

        });
      } else {
      }

    })
  }

  presentToast(param) {
    const toast = this.toastCtrl.create({
      message: param,
      duration: 9000,
      position: 'top'
    });
    toast.present();
  }

  private cropOptions: RatioCropOptions = {
    quality: 75,
    targetWidth: 1080,
    targetHeight: 1080,
    widthRatio: -1,
    heightRatio: -10
  };

  takePicture() {
    return this.camera.getPicture({
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: false,
      correctOrientation: true
    })
      .then((fileUri) => {
        this.Ccrop.ratioCrop(fileUri, this.cropOptions).then(ImageData => {
          const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 5000
          });
          loader.present();
          let imgFinal = this.generateFromImage(ImageData);
        })
      })
  }

  selectPicture() {

    return this.camera.getPicture({
      allowEdit: false,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      destinationType: this.camera.DestinationType.FILE_URI
    })
      .then((fileUri) => {
        return this.Ccrop.ratioCrop(fileUri, this.cropOptions).then(ImageData => {
          const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 5000
          });
          loader.present();
          let imgFinal = this.generateFromImage(ImageData);
        })
      })

  }


  CurrentChatID = -1;
  tblChatDetails = "INSERT INTO tblChatDetails (chatid, isreply, message , messagedate , reciverid , replymessageid , status , userid , username , isSync) VALUES";
  TodayDate = new Date().toISOString().split('T')[0];
  YesterDayDate = new Date(Date.now() - 864e5).toISOString().split('T')[0];
  constructor(public navCtrl: NavController, public actionSheetCtrl: ActionSheetController, public navParams: NavParams,
    private photoViewer: PhotoViewer, private network: Network, private database: AngularFireDatabase, private ObjHelper: HelperProvider,
    public loadingCtrl: LoadingController, public ObjDatabase: DatabaseHelperProvider, public toastCtrl: ToastController,
    private ObjHttp: Http, private transfer: FileTransfer,
    private camera: Camera, private clipboard: Clipboard,
    private filePlugin: File,
    private base64: Base64,
    private media: Media,
    private crop: Crop,
    private mediaCapture: MediaCapture,
    private Ccrop: RatioCrop) {

    this.GetText();
    this.ChatInfo = navParams.get('UserData');
    this.ObjHelper.ChatDetailUserID = this.ChatInfo;
    this.ObjHelper.IsUserOnline();
    //this.LastSeen = this.ObjHelper.AllUsersLastSeen.find(items => items.UserID == this.ChatInfo).LastSeen;
    //this.LastSeen = this.ObjHelper.IsUserOnline(this.ChatInfo);

    this.UserAllInfo = this.ObjHelper.GetUserInfo(this.ChatInfo);
    this.recieverid = this.ChatInfo;
    this.CurrentChatID = navParams.get("ChatID");
    this.CurrentUserName = navParams.get("username");
    this.chatID = this.CurrentChatID;
    let TestNet = this.network.type;

    if (TestNet == 'none') {
      // alert("NO network " + this.CurrentChatID);
      this.ObjDatabase.SelectQuerySqlite("select * from tblChatDetails").then((data: any) => {
        // this.ObjDatabase.SelectQuerySqlite("select * from tblChatDetails where chatid = " + this.CurrentChatID).then((data: any) => {

        console.clear();
        this.ChatMeg = data;
        console.log(this.ChatMeg);
      })
    } else {
      this.isSyncTrue();
      this.GetRealTimeChats();
      this.CurrentUserImage = navParams.get("image");
    }
    // let connectSubscription = this.network.onConnect().subscribe(() => {
    //   console.log('network connected!');
    //   // We just got a connection but we need to wait briefly
    //   // before we determine the connection type. Might need to wait.
    //   // prior to doing any api requests as well.
    //   setTimeout(() => {
    //     if (TestNet == "none") {
    //       // this.ChatMeg.forEach(element => {
    //       //   element.status = "Send";
    //       // });
    //     }
    //   }, 2000);
    // });
  }

  checkUserClear(param) {

    var Keys = Object.keys(param);
    var res = 0;

    for (var loop = 0; loop < Keys.length; loop++) {

      if (Keys[loop] == "chatid" || Keys[loop] == "isreply" || Keys[loop] == "message" || Keys[loop] == "messagedate" || Keys[loop] == "reciverid" ||
        Keys[loop] == "replymessageid" || Keys[loop] == "status" || Keys[loop] == "userid" || Keys[loop] == "username" || Keys[loop] == "key"
      ) {

      } else {

        if (param[Keys[loop]].ClearUserID == this.ObjHelper.LoggedInUserID) {
          res = 1;
        }
      }
    }

    return res;
  }

  Test() {
    var count = 0;
    return new Promise((resolve, reject) => {
      this.database.object('/TblChat/').valueChanges().subscribe(data => {
        if (data != null) {
          let arr = Object.keys(data);

          for (var i = 0; i < arr.length; i++) {
            const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
            if (this.chatID == object2.chatid) {
              count++;
            }
          }

        } else {
          count = 5;
        }
        resolve(count);
      })

      // reject("error");

    });

  }

  CheckisDeleted() {
    var count = 0;
    // this.recieverid = "0";
    this.database.object('/TblChat/').valueChanges().subscribe(data => {
      if (data != null) {
        let arr = Object.keys(data);

        for (var i = 0; i < arr.length; i++) {
          const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
          if (this.chatID == object2.chatid) {
            count++;
          }
        }

      } else {
        count = 5;
      }
    })
    return count;
  }

  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      alert("Got Image " + imageData);
      // this.UploadFileToFirebase("");
    }, (err) => {
      console.log(err);
      this.presentToast(err);
      alert("Got Error " + err);
    });
  }

  ionViewWillLeave() {
    // 

    // this.database.object('/TblChatDetails/').valueChanges().subscribe((data) => { });

    //   this.db.object('/parents/' + auth.uid).first().subscribe(profileResult => {
    //     this.profileResult = profileResult;
    //     console.log(profileResult);

    // });
  }

  ImageClick(param) {
    this.photoViewer.show(param);
  }

  GetRealTimeChats() {

    this.database.object('/TblChatDetails/').valueChanges().subscribe(data => {

      // if (this.navCtrl.getActive().component == FoundationChatPageDetailsPage) {
      this.ChatMeg = [];

      if (this.CurrentChatID == undefined || this.CurrentChatID == null)
        this.GetUserChat(this.ChatInfo);

      else
        this.GetUserChatDetails(this.CurrentChatID);
      // }
    })

  }

  GetUserChat(UserID) {
    var ref = firebase.database().ref().child('TblChat');
    this.isnew = true;
    ref.orderByChild('recieverid').startAt(UserID).endAt(UserID).on('child_added', (snapshot) => {
      // this.ChatMeg = snapshot.val();
      let Value = snapshot.val();

      if (Value.userid == this.ObjHelper.LoggedInUserID) {
        this.isnew = false;
        this.GetUserChatDetails(Value.chatid);
      }
      else
        this.isnew = true;
    })
    // console.log(this.isnew);
  }

  createID() {
    let date = new Date().getTime().toString();
    date = date.substring(date.length - 6);
    return date
  }

  GetText() {
    this.database.object('/Validation/').valueChanges().subscribe(data => {

      let arr = Object.keys(data);

      for (var i = 0; i < arr.length; i++) {
        const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
        this.AudioText = object2.Text;
      }


    })
  }

  UpdateStatus(param) {

    if (this.navCtrl.getActive().component == FoundationChatPageDetailsPage) {
      this.database.list('/TblChatDetails/').update(param, {
        status: "Read"
      })
    }
  }

  toggled: boolean = false;
  // emojitext: string;

  handleSelection(event) {
    // this.emojitext = this.emojitext + " " + event.char;
    this.MessageText = this.MessageText + " " + event.char;
    this.toggled = false;
  }

  UpdateUnReadMsg(ChatID) {
    var ref = firebase.database().ref().child('TblChat');
    // this.isnew = true;
    ref.orderByChild('chatid').startAt(ChatID).endAt(ChatID).on('child_added', (snapshot) => {

      // this.ChatMeg = snapshot.val();
      let Value = snapshot.val();
      if (Value.userid == this.ObjHelper.LoggedInUserID) {

        // if (UnReadMsgCount == undefined)
        //   UnReadMsgCount = 0;
        // UnReadMsgCount = Value.UnreadMsgQun;
        if (this.navCtrl.getActive().component == FoundationChatPageDetailsPage) {
          this.database.list('/TblChat/').update(snapshot.key, {
            UnreadMsgQun: 0
          })
        }
      }
    })

  }

  AddToUserUnreadMsgs(UnReadMsgCount) {

    var ref = firebase.database().ref().child('TblChat');
    // this.isnew = true;
    ref.orderByChild('chatid').startAt(this.CurrentUserAllChatIds).endAt(this.CurrentUserAllChatIds).on('child_added', (snapshot) => {

      // this.ChatMeg = snapshot.val();
      let Value = snapshot.val();
      if (Value.userid == this.recieverid) {

        // if (UnReadMsgCount == undefined)
        //   UnReadMsgCount = 0;
        UnReadMsgCount = Value.UnreadMsgQun;
        if (this.navCtrl.getActive().component == FoundationChatPageDetailsPage) {
          this.database.list('/TblChat/').update(snapshot.key, {
            UnreadMsgQun: UnReadMsgCount + 1,
            lastmessage: this.MessageText
          })
        }
      }
      else if (Value.userid == this.recieverid || Value.userid == this.ObjHelper.LoggedInUserID) {
        if (this.navCtrl.getActive().component == FoundationChatPageDetailsPage) {
          this.database.list('/TblChat/').update(snapshot.key, {
            lastmessage: this.MessageText
          })
        }
      }
    })
  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
  }


  async generateFromImage(img, MAX_WIDTH: number = 700, MAX_HEIGHT: number = 700, quality: number = 1, ) {
    var canvas: any = document.createElement("canvas");
    var image = new Image();

    image.onload = () => {
      var width = image.width;
      var height = image.height;

      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      canvas.width = width;
      canvas.height = height;
      var ctx = canvas.getContext("2d");

      ctx.drawImage(image, 0, 0, width, height);

      // IMPORTANT: 'jpeg' NOT 'jpg'
      var dataUrl = canvas.toDataURL('image/jpeg', quality);

      //Save To Fireabse
      const pictures: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + this.createID());

      pictures.putString(dataUrl, 'data_url').then(() => {
        // const UP: firebase.storage.UploadTask = sotre;
        pictures.getDownloadURL().then((downloadURL) => {

          this.image = downloadURL;
          // this.ImagePreviewVar = downloadURL
          // // document.getElementById('DvScroll').style.opacity = "0.7";
          // document.getElementById('DvScroll').style.backgroundImage = "";
          // document.getElementById('DvScroll').style.backgroundColor = "black";
          // this.ImagePreview = 1;

          // alert(this.image);
          this.SendMsg()
        }, (error) => {
          alert("Error " + error);

          alert(JSON.stringify(error));
        });
      });



    }
    image.src = img;
  }

  image = "";
  async OpenCamera() {

    try {

      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }


      this.camera.getPicture(options).then((Image) => {
        this.crop.crop(Image, { quality: 100, targetWidth: 10, targetHeight: 10 })
          .then(
            newImage => {

              let imgFinal = this.generateFromImage(newImage);
            },
            error => alert('Error cropping image' + error)
          );
      })




      // setTimeout(() => {
      //   const UP: firebase.storage.UploadTask = sotre;
      //   UP.snapshot.ref.getDownloadURL().then((downloadURL) => {
      //     
      //     this.image = downloadURL;

      //     this.SendMsg()
      //   }, (error) => {
      //     alert("Error " + error);

      //     alert(JSON.stringify(error));
      //   });
      // }, 20000);

    } catch (error) {
      alert("Catch " + error);
      alert(JSON.stringify(error));
    }
  }

  onFileChanged(param) {
    var aa;
    const file: any = param.target.files[0];

    this.crop.crop(file, { quality: 75 })
      .then(
        newImage => alert('new image path is: ' + newImage),
        error => alert('Error cropping image ' + error)
      );


    const metaData = { 'contentType': file.type };
    //  Video wala kaam
    if (file.type == "video/mp4") {
      var StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/vidoes/' + this.createID());
    } else {
      var StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + this.createID());
    }
    //  Video wala kaam
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      // duration: 3000
    });
    loader.present();
    StorageRef.put(file, metaData).then((data) => {
      // alert("Put");
      StorageRef.getDownloadURL().then((downloadURL) => {
        // alert(downloadURL + " is download url");


        this.image = downloadURL;
        this.ImagePreviewVar = downloadURL
        // document.getElementById('DvScroll').style.opacity = "0.7";
        document.getElementById('DvScroll').style.backgroundImage = "";
        document.getElementById('DvScroll').style.backgroundColor = "black";
        this.ImagePreview = 1;


        // this.SendMsg()
        loader.dismiss();

      }, (error) => {
        // alert("Error while creating download url");
        // alert(error);
        // alert(error.body);
        // alert(error.message);
        // alert(JSON.stringify(error));
      });
    }, (error) => {
      // alert("Error while putting");
    });
    // const Store = StorageRef.put(file, metaData);
    // this.presentLoading();
    // setTimeout(() => {
    //   const UP: firebase.storage.UploadTask = Store;
    //   UP.snapshot.ref.getDownloadURL().then((downloadURL) => {
    //     this.image = downloadURL;
    //     this.SendMsg()
    //   });
    // }, 3000);

    // setTimeout(() => {
    //   this.ImageLink = aa;
    // }, 8000);

  }

  CurrentUserAllChatIds = "";
  CurrentChatUnreadMsgCount = 0;

  GetUserChatDetails(ChatID) {

    let AllMsgs = [];
    this.chatID = ChatID
    if (this.ChatMeg.length == 0) {
      var ref = firebase.database().ref().child('TblChatDetails');
      this.isnew = true;
      ref.orderByChild('chatid').startAt(ChatID).endAt(ChatID).on('child_added', (snapshot) => {
        // 
        let Value = snapshot.val();
        Value.key = snapshot.key;
        //Commented by Haroon on 24/6/19
        // this.ChatMeg.push(Value);

        if (this.checkUserClear(Value) == 0) {
          AllMsgs.push(Value);
          console.log(this.ChatMeg);
          this.isnew = false;

          if (Value.userid != this.ObjHelper.LoggedInUserID && this.navCtrl.getActive().component == FoundationChatPageDetailsPage) {
            this.UpdateStatus(snapshot.key);
            this.UpdateUnReadMsg(Value.chatid);
          }
          if (Value.userid == this.ObjHelper.LoggedInUserID && Value.reciverid == this.recieverid && this.navCtrl.getActive().component == FoundationChatPageDetailsPage) {
            this.CurrentUserAllChatIds = Value.chatid;
            this.CurrentChatUnreadMsgCount = Value.UnreadMsgQun;
          }
        }


      })


      if (AllMsgs.length > 1) {
        let IsFirst = false;
        const group = AllMsgs.reduce((Date, MsgObject, i) => {

          if (!IsFirst) {
            Date = [];
            Date[AllMsgs[0].messagedate.split(' ')[0]] = [];
            Date[AllMsgs[0].messagedate.split(' ')[0]].push(AllMsgs[0]);
          }
          IsFirst = true;
          let MsgDate = MsgObject.messagedate.split(' ')[0];
          if (!Date[MsgDate])
            Date[MsgDate] = [];

          Date[MsgDate].push(MsgObject);
          return Date;

        })

        // this.ChatMeg = group;
        // console.clear();

        if (IsFirst) {
          this.ChatMeg = Object.keys(group).map((res) => {

            console.log(group + res);
            return {
              MsgDate: res,
              Msgs: group[res]
            }
          });
        }
      }
      else if (AllMsgs.length > 0) {


        this.ChatMeg[0] = {
          MsgDate: AllMsgs[0].messagedate.split(' ')[0],
          Msgs: AllMsgs
        };
        // this.ChatMeg = {
        //   MsgDate: new Date().toISOString().split('T')[0],
        //   Msgs: AllMsgs[0]
        // }
      }



      // if (AllMsgs.length > 1) {
      //    
      //   let group = [];
      //   // if (AllMsgs[0].messagedate.split(' ')[0] in group)
      //   //   group[AllMsgs[0].messagedate.split(' ')[0]].push(AllMsgs[0]);
      //   // else {
      //   // group[AllMsgs[0].messagedate.split(' ')[0]] = [];
      //   // group[AllMsgs[0].messagedate.split(' ')[0]].push(AllMsgs[0]);
      //   // }
      //   let IsFirst = false;
      //   group = AllMsgs.reduce((Date, MsgObject, i) => {
      //      
      //     if (!IsFirst) {
      //       Date = [];
      //       Date[AllMsgs[0].messagedate.split(' ')[0]] = [];
      //       Date[AllMsgs[0].messagedate.split(' ')[0]].push(AllMsgs[0]);
      //     }

      //     IsFirst = true;
      //     let MsgDate = MsgObject.messagedate.split(' ')[0];
      //     if (!Date[MsgDate])
      //       Date[MsgDate] = [];

      //     Date[MsgDate].push(MsgObject);
      //     return Date;

      //   })

      //   // this.ChatMeg = group;
      //   // console.clear();
      //   this.ChatMeg = Object.keys(group).map((res) => {
      //      
      //     console.log(group + res);
      //     return {
      //       MsgDate: res,
      //       Msgs: group[res]
      //     }
      //   });
      // }
      // else if (AllMsgs.length > 0) {
      //   this.ChatMeg[0] = {
      //     MsgDate: AllMsgs[0].messagedate.split(' ')[0],
      //     Msgs: AllMsgs
      //   };
      // }

      console.log(this.ChatMeg);
      // alert("After loop?" + this.ChatMeg.length);
      // this.ScrollToBottom("From GetUserChatDetail");

      // setTimeout(() => {
      //   console.log("Scrolling");
      //   // window.scrollTo(0, document.body.scrollHeight);

      //   // var objDiv = document.getElementById("DvScroll");
      //   // objDiv.scrollTop = objDiv.scrollHeight;

      // }, 1500)
    }
  }

  GoBack() {
    //Commented by Haroon on 20/5/19
    // this.navCtrl.pop();
    // this.navCtrl.push(HomePage).then(() => {
    //   const index = this.navCtrl.getActive().index;
    //   this.navCtrl.remove(0, index);
    // });

    //Added by Haroon on 20/5/19
    this.navCtrl.pop();

  }

  ionViewWillEnter() {
    // 


  }

  // scrollToBottom() {
  //   setTimeout(() => {
  //     // this.content.scrollTo(this.content.scrollHeight, this.content.scrollWidth, 300);
  //     var objDiv = document.getElementById("ScrollToBottom");
  //     objDiv.scrollTop = objDiv.scrollHeight - objDiv.scrollHeight;
  //   }, 1000);
  // }



  BackMsg() {
    document.getElementById('DvScroll').style.opacity = "1";
    document.getElementById('DvScroll').style.backgroundImage = "url('https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png')";
    document.getElementById('DvScroll').style.backgroundColor = "";
    this.ImagePreview = 0;

  }
  // newSK
  SendMsg() {
    document.getElementById('DvScroll').style.opacity = "1";
    document.getElementById('DvScroll').style.backgroundImage = "url('https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png')";
    document.getElementById('DvScroll').style.backgroundColor = "";
    this.ImagePreview = 0;

    let DateTime = new Date();
    if (this.ObjHelper.isReply == 1) {

      this.Test().then((data: any) => {
        count = data;
        if (count < 2) {
          this.database.list("/TblChat/").push({
            // lastmessage: "click to see your message",
            lastmessage: this.MessageText,
            datechat: new Date().toISOString().split('T')[0],
            userid: this.recieverid,
            username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).name,
            dp: "https://rajputfoundation.org/" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).image,
            recieverid: this.ObjHelper.LoggedInUserID,
            chatid: this.chatID,
            image: "",
            isadminview: 0,
            UnreadMsgQun: 0
          })
        }
      })

      // rao mazahir
      // 123456
      // Rana wiliyat
      // 123456

      if (this.MessageText != "" || this.image != "") {
        let text = this.MessageText;
        this.database.list("/TblChatDetails/").push({
          userid: this.ObjHelper.LoggedInUserID,
          username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).name,
          reciverid: this.recieverid,
          message: text,
          messagedate: DateTime.toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
          status: "Send",
          isreply: 1,
          replymessageid: this.ObjHelper.isReplyData[0].message,
          chatid: this.chatID
        }).then(data => {
          this.AddToUserUnreadMsgs(this.CurrentChatUnreadMsgCount);
          this.MessageText = "";
          this.ObjHelper.isReply = 0;
          this.ConversationDivClick();
          // this.SelectedChat.length = 0;
          // this.SelectedID.length = 0;
        })
      }
    }




    else {
      if (this.MessageText != "" || this.image != "") {

        if (this.isnew) {
          this.chatID = this.createID();
          this.database.list("/TblChat/").push({
            // lastmessage: "click to see your message",
            lastmessage: this.MessageText,
            datechat: new Date().toISOString().split('T')[0],
            userid: this.ObjHelper.LoggedInUserID,
            username: this.ObjHelper.UserInfo.find(items => items.userid == this.recieverid).name,
            dp: "https://rajputfoundation.org/" + this.ObjHelper.UserInfo.find(items => items.userid == this.recieverid).image,
            recieverid: this.recieverid,
            chatid: this.chatID,
            image: "",
            isadminview: 0,
            UnreadMsgQun: 0
          })
          this.database.list("/TblChat/").push({
            // lastmessage: "click to see your message",
            lastmessage: this.MessageText,
            datechat: new Date().toISOString().split('T')[0],
            userid: this.recieverid,
            username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).name,
            dp: "https://rajputfoundation.org/" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).image,
            recieverid: this.ObjHelper.LoggedInUserID,
            chatid: this.chatID,
            image: "",
            isadminview: 0,
            UnreadMsgQun: 0
          })

          this.CurrentChatUnreadMsgCount = 0;
        }
        // let Count = ;
        // alert(Count);
        var count = 0;
        this.Test().then((data: any) => {
          count = data;
          if (count < 2) {
            this.database.list("/TblChat/").push({
              // lastmessage: "click to see your message",
              lastmessage: this.MessageText,
              datechat: new Date().toISOString().split('T')[0],
              userid: this.recieverid,
              username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).name,
              dp: "https://rajputfoundation.org/" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).image,
              recieverid: this.ObjHelper.LoggedInUserID,
              chatid: this.chatID,
              image: "",
              isadminview: 0,
              UnreadMsgQun: 0
            })
          }
        })



        let text = this.MessageText;
        if (this.image != "") {
          text = this.image;
        }
        let network = this.network.type;
        if (network == "none") {
          this.tblChatDetails = this.tblChatDetails + "('" + this.chatID + "','" + 0 + "','" + text + "','" + new Date().toISOString().split('T')[0] + "','" + this.recieverid + "','" + "" + "','Pending','" + this.ObjHelper.LoggedInUserID + "','" + this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).username + "',0),"
          var query = this.tblChatDetails.slice(0, -1);

          this.ObjDatabase.InsertQuerySqlite(query, []).then(data => {
            this.ChatMeg.push({
              userid: this.ObjHelper.LoggedInUserID,
              username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).name,
              reciverid: this.recieverid,
              message: text,
              messagedate: DateTime.toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
              status: "Pending",
              isReply: 0,
              replymessageid: "",
              chatid: this.chatID
            })
          })
        }
        else {
          // let PlayerIds = [this.ObjHelper.UserInfo.find(items => items.userid == this.recieverid).notificationid];
          let recivername = this.ObjHelper.UserInfo.find(items => items.userid == this.recieverid).username;
          let heading = "New message from " + recivername + " :";
          this.ObjHttp.get(this.ObjHelper.ServerHost + "SendNotificationSer?Message=" + this.MessageText + "&Heading=" + heading + "&UserID=" + this.recieverid)
            .map((res: Response) => res.text()).subscribe((data) => {

            })

          this.database.list("/TblChatDetails/").push({
            userid: this.ObjHelper.LoggedInUserID,
            username: this.ObjHelper.UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID).name,
            reciverid: this.recieverid,
            message: text,
            messagedate: DateTime.toISOString().split('T')[0] + " " + new Date().toLocaleTimeString(),
            status: "Send",
            isreply: 0,
            replymessageid: "",
            chatid: this.chatID
          })
          // 
          // this.ScrollToBottom("From SendMsg");
          this.AddToUserUnreadMsgs(this.CurrentChatUnreadMsgCount);
        }

        this.MessageText = "";
        this.image = "";
        // this.SelectedChat.length = 0;
        // this.SelectedID.length = 0;
        this.ConversationDivClick();
        // Added by Haroon on 24/6/2019
        this.isnew = false;
      }
    }


  }

  SelectedID = [];
  SelectedChat = [];

  presentActionSheet(param, selectedid, SorR) {
    selectedid = SorR + selectedid;
    // this.SelectedChat.push(param);


    // if (this.SelectedID != "" && this.SelectedID != selectedid) {
    //   document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
    //   this.SelectedID = "";
    // }

    // ============
    // if (this.SelectedID == selectedid) {
    //   document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
    //   this.IsActionPresented = false;
    //   this.SelectedID = "";
    // }
    // else {
    //   if (this.SelectedID != "") {
    //     document.getElementById(this.SelectedID).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
    //     this.SelectedID = "";
    //   }
    //   else {
    //     this.IsActionPresented = true;
    //     this.SelectedID = selectedid;
    //     document.getElementById(this.SelectedID).style.backgroundColor = "cornflowerblue";
    //   }
    if (this.SelectedID.find(items => items == selectedid) != undefined) {
      document.getElementById(this.SelectedID.find(items => items == selectedid)).style.backgroundColor = this.SelectedID.find(items => items == selectedid)[0] == 'S' ? "#e1ffc7" : "white";
      this.IsActionPresented = false;
      this.SelectedID.splice(this.SelectedID.indexOf(this.SelectedID.find(items => items == selectedid)), 1);
      this.SelectedChat.splice(this.SelectedChat.indexOf(this.SelectedChat.find(items => items == param)), 1);
      // this.SelectedChat.push(param);

      // this.SelectedID = "";
    }
    else {
      // if (this.SelectedID.length > 0) {
      //   document.getElementById(selectedid).style.backgroundColor = this.SelectedID[0] == 'S' ? "#e1ffc7" : "white";
      //   // this.SelectedID = "";
      //   this.SelectedID.splice(this.SelectedID.indexOf(this.SelectedID.find(items => items == selectedid)), 1);

      // }
      // else {
      this.IsActionPresented = true;
      this.SelectedID.push(selectedid);
      this.SelectedChat.push(param);
      document.getElementById(selectedid).style.backgroundColor = "cornflowerblue";
      // }
    }


    // const actionSheet = this.actionSheetCtrl.create({
    //   title: 'Select Your Activity',
    //   buttons: [
    //     {
    //       text: 'Copy',
    //       role: 'destructive',
    //       handler: () => {
    //         this.clipboard.copy(param.message);
    //         console.log('Destructive clicked');
    //       }
    //     }, {
    //       text: 'Forward',
    //       handler: () => {
    //         if (this.network.type != "this.network.type != ") {
    //           this.clipboard.copy(param.message)
    //           // .then(() => {
    //           //     this.navCtrl.push(FoundationNewChatPage, { isForwardText: param.message });
    //           // });

    //           this.clipboard.paste().then(
    //             (resolve: string) => {
    //               var text = resolve;
    //               
    //               this.navCtrl.push(FoundationNewChatPage, { isForwardText: text });
    //               // this.clipboard.clear();
    //             },
    //             (reject: string) => {
    //               //  alert('Error: ' + reject);
    //             }
    //           );
    //           // this.navCtrl.push(FoundationNewChatPage, { isForwardText: "ForwardText" });

    //         }
    //         else {
    //           alert("Network Error");
    //         }
    //         console.log('Archive clicked');
    //       }
    //     }, {
    //       text: 'Delete',
    //       handler: () => {
    //         // this.database.list('/TblChat/').update("param.Key", {
    //         //   Name: 'arqam',
    //         //   pass: 234
    //         // })

    //         // bus Messge ki ID dedenge to delete hgyega 
    //         
    //         if (param.userid == this.ObjHelper.LoggedInUserID) {
    //           this.database.list('/TblChatDetails/').remove(param.key).then((data) => {
    //             
    //           }, (error) => {
    //             
    //           }
    //           );
    //         }
    //         else
    //           alert("Cannot delete other user messages");

    //         console.log('Archive clicked');
    //       }
    //     }, {
    //       text: 'Cancel',
    //       role: 'cancel',
    //       handler: () => {
    //         console.log('Cancel clicked');
    //       }
    //     }
    //   ]
    // });
    // actionSheet.onDidDismiss(() => {
    //   
    //   this.IsActionPresented = false;
    // })
    // actionSheet.present();
  }

  ConversationDivClick() {

    if (!this.RealClick) {
      this.toggled = false;
      this.IsActionPresented = false
      if (this.SelectedID.length > 0) {
        for (let i = 0; i < this.SelectedID.length; i++) {
          document.getElementById(this.SelectedID[i]).style.backgroundColor = this.SelectedID[i][0] == 'S' ? "#e1ffc7" : "white";
        }
        this.SelectedID = [];
        this.SelectedChat = [];
        this.ObjHelper.isReply = 0;
        this.ObjHelper.isReplyData = [];
        this.ObjHelper.isReplyText = "";
      } else {
      }
    }
    else {
      this.RealClick = false;
    }
  }

  // newSK
  ReplyClick() {
    this.ObjHelper.isReply = 1;
    this.ObjHelper.isReplyData = this.SelectedChat;
    if (this.ObjHelper.isReplyData[0].message.split(':')[0] == "https") {
      // this.presentToast("Reply Against Your Selected Iamge");
      this.ObjHelper.isReplyText = 'Reply Against Your Selected Iamge';

    } else {
      // this.presentToast("Reply Against ( " + this.ObjHelper.isReplyData[0].message + " )");
      this.ObjHelper.isReplyText = this.ObjHelper.isReplyData[0].message.substring(0, 30) + "..";
    }
  }

  ForwardClick() {
    let messages = [];

    for (let i = 0; i < this.SelectedChat.length; i++) {
      messages.push(this.SelectedChat[i].message);
    }
    if (this.network.type != "none") {
      // this.clipboard.copy(this.SelectedChat.message)
      // .then(() => {
      //     this.navCtrl.push(FoundationNewChatPage, { isForwardText: param.message });
      // });

      // this.clipboard.paste().then(
      // (resolve: string) => {
      // var text = resolve;

      this.navCtrl.push(FoundationNewChatPage, { isForwardText: messages }).then(() => {
        this.IsActionPresented = false;

        if (this.SelectedChat.length == this.SelectedID.length) {
          for (let i = 0; i < this.SelectedChat.length; i++) {
            document.getElementById(this.SelectedID[i]).style.backgroundColor = this.SelectedID[i][0] == 'S' ? "#e1ffc7" : "white";
          }
          this.SelectedChat = [];
          this.SelectedID = []
        }
      });
      // this.clipboard.clear();
      // },
      // (reject: string) => {
      //  alert('Error: ' + reject);
      // }
      // );
      // this.navCtrl.push(FoundationNewChatPage, { isForwardText: "ForwardText" });

    }
    else {
      alert("Network Error");
    }



    // if (this.network.type != "none") {
    //   this.clipboard.copy(this.SelectedChat.message)
    //   // .then(() => {
    //   //     this.navCtrl.push(FoundationNewChatPage, { isForwardText: param.message });
    //   // });

    //   this.clipboard.paste().then(
    //     (resolve: string) => {
    //       var text = resolve;
    //       
    //       this.navCtrl.push(FoundationNewChatPage, { isForwardText: text }).then(() => {
    //         this.IsActionPresented = false;
    //       });
    //       // this.clipboard.clear();
    //     },
    //     (reject: string) => {
    //       //  alert('Error: ' + reject);
    //     }
    //   );
    //   // this.navCtrl.push(FoundationNewChatPage, { isForwardText: "ForwardText" });

    // }
    // else {
    //   alert("Network Error");
    // }
  }

  DeleteClick() {

    for (let i = 0; i < this.SelectedChat.length; i++) {
      if (this.SelectedChat[i].userid == this.ObjHelper.LoggedInUserID) {
        let param = this.SelectedChat[i];
        this.database.list('/TblChatDetails/').remove(this.SelectedChat[i].key).then((data) => {

          this.IsActionPresented = false;
          this.SelectedID = [];
          this.SelectedChat.splice(this.SelectedChat.indexOf(this.SelectedChat.find(items => items == param)), 1);
        }, (error) => {

        }
        );
      }
      else
        alert("Cannot delete other user messages");

    }

    // if (this.SelectedChat.userid == this.ObjHelper.LoggedInUserID) {
    //   this.database.list('/TblGroupDetails/').remove(this.SelectedChat.Key).then((data) => {
    //     
    //     this.IsActionPresented = false;
    //     this.SelectedID = "";
    //   }, (error) => {
    //     
    //   }
    //   );
    // }
    // else
    //   alert("Cannot delete other user messages");

  }

  CopyClick() {
    this.clipboard.copy(this.SelectedChat.length > 0 ? this.SelectedChat[0].message : "").then(() => {
      this.IsActionPresented = false;
    });
  }
  // Chat Details 
  // 1) Village
  // 2) City
  // 3) Area
  // 4) Admin Chat Disabled
  // 4) Admin Chats 
  RealClick = false;

  SelectMsgClick(param, index, SorR) {

    this.RealClick = true;
    if (this.SelectedID.length > 0)
      this.presentActionSheet(param, index, SorR);
    else {

      this.RealClick = false;
      this.SelectedChat.length = 0;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoundationChatPageDetailsPage');
    // this.ObjStorage.getItem(MEDIA_FILES_KEY).th

  }

  OpenMic2() {

    let fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.3gp';
    let filePath = this.filePlugin.externalDataDirectory.replace(/file:\/\//g, '') + fileName;
    let audio: MediaObject = this.media.create(filePath);

    audio.startRecord();

    setTimeout(() => {
      audio.stopRecord();
      let Date = fileName;
      let DATA = filePath;

      let storageRef = firebase.storage().ref();
      let metadata = {
        contentType: 'audio/mp3',
      };


      filePath = "file:///storage/emulated/0/VoiceRecorder/my_sounds/";
      this.filePlugin.readAsDataURL(filePath, fileName).then((file) => {

        let voiceRef = storageRef.child('/photos/audios/' + this.createID()).putString(file, firebase.storage.StringFormat.DATA_URL);
        voiceRef.on(firebase.storage.TaskEvent.STATE_CHANGED, (snapshot) => {
          console.log("uploading");
        }, (e) => {

          console.log(JSON.stringify(e, null, 2));
        }, () => {

          // let DURL = "https://firebasestorage.googleapis.com/v0/b/" + voiceRef.snapshot.metadata.bucket + "/o/" + voiceRef.snapshot.metadata.fullPath + "?alt=media"
          let DURL = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F020863?alt=media&token=90723f01-1a20-4049-9ddc-09c3916df539"
          // var downloadURL = voiceRef.snapshot.downloadURL;
          // this.Audio = DURL;
          // this.Audio = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F499007?alt=media"
          this.image = DURL;
          this.SendMsg();
          var str = "<audio controls><source src=' " + this.Audio + " ' type='audio/amr'></audio>";
          // document.getElementById('audio').innerHTML = str;
        });
      }
        , (error) => {
          console.log(error)
          console.log(JSON.stringify(error));
        }
      );


    }, 5000)
    //this.recording = true;
  }

  Audio = "";
  OpenMic() {


    if (this.AudioText == "") {


      let options: CaptureAudioOptions = { limit: 1 };
      this.mediaCapture.captureAudio(options)
        .then((data: MediaFile[]) => {
          debugger
          this.Audio = data[0].fullPath;
          let Name = data[0].name;
          // Name = Name.split('.')[0] + ".mp3";
          let storageRef = firebase.storage().ref();
          let metadata = {
            contentType: 'audio/ogg',
          };

          let filePath = data[0].fullPath;
          this.base64.encodeFile(filePath).then(
            (base64: any) => {

              console.log('file base64 encoding: ' + base64);
            }
            , (error) => {

            }
          );
          // filePath = "file:///storage/emulated/0/VoiceRecorder/my_sounds/";
          filePath = data[0].fullPath.split(data[0].fullPath.split('/')[data[0].fullPath.split('/').length - 1])[0];
          this.filePlugin.readAsDataURL(filePath, Name).then((file) => {

            let voiceRef = storageRef.child('/photos/audios/' + this.createID()).putString(file, firebase.storage.StringFormat.DATA_URL);
            voiceRef.on(firebase.storage.TaskEvent.STATE_CHANGED, (snapshot) => {
              console.log("uploading");
            }, (e) => {

              console.log(JSON.stringify(e, null, 2));
            }, () => {
              // let DURL = "https://firebasestorage.googleapis.com/v0/b/" + voiceRef.snapshot.metadata.bucket + "/o/";
              var rep = voiceRef.snapshot.metadata.fullPath.replace('/', '%2F');
              let DURL = "https://firebasestorage.googleapis.com/v0/b/" + voiceRef.snapshot.metadata.bucket + "/o/" + rep.replace('/', '%2F');

              let DURL2 = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F250342";

              this.ObjHttp.get(DURL)
                .map((res: Response) => res.json())
                .subscribe((data) => {
                  DURL = DURL + "?alt=media&token=" + data.downloadTokens;
                  this.image = DURL;

                  this.SendMsg();
                  // let jsonString: string = this.GetJsonString(data);
                  // let Data = JSON.parse(jsonString);
                  //  
                },
                  (error) => {

                    console.log("Error in Subscribe." + error);
                  });

              // let DURL = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F020863?alt=media&token=90723f01-1a20-4049-9ddc-09c3916df539"
              // var downloadURL = voiceRef.snapshot.downloadURL;
              // this.Audio = DURL;
              // this.Audio = "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/photos%2Faudios%2F499007?alt=media"

              var str = "<audio controls><source src=' " + this.Audio + " ' type='audio/amr'></audio>";
              // document.getElementById('audio').innerHTML = str;
            });
          }
            , (error) => {

              console.log(error)
              console.log(JSON.stringify(error));
            }
          );


          // const file = {
          //   lastModified: 1553893084222,
          //   lastModifiedDate: new Date(),
          //   name: data[0].name,
          //   size: data[0].size,
          //   type: data[0].type,
          //   webkitRelativePath: ""
          // }
          // const metaData = { 'contentType': file.type };
          // let filename = this.createID();
          // var StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/audios/' + this.createID());

          // StorageRef.put(file, metaData).then((data) => {
          //   // alert("Put");
          //    
          //   StorageRef.getDownloadURL().then((downloadURL) => {
          //      
          //     // alert(downloadURL + " is download url");
          //     this.Audio = downloadURL;

          //     var str = "<audio controls><source src=' " + this.Audio + " ' type='audio/wav'></audio>";
          //     document.getElementById('audio').innerHTML = str;

          //     // var str = "<audio controls><source src=' " + this.Audio + " ' type='audio/wav'></audio>";
          //     // document.getElementById('audio').innerHTML = str;

          //   }, (error) => {
          //   });
          // }, (error) => {
          //   // alert("Error while putting");
          // });
          // var Obj = {
          //   lastModified : data[0].lastModifiedDate,
          //   lastModifiedDate : new Date(data[0].lastModifiedDate),
          //   name : data[0].name.split(".")[0],
          //   size : data[0].size,
          //   type : "",
          //   webkitRelativePath : ""
          // }
          // const metaData = { 'contentType': "" };
          // var StorageRef: firebase.storage.Reference = firebase.storage().ref('/photos/imgs/' + this.createID());

          // StorageRef.put(Obj , metaData).then((data) => {
          //   // alert("Put");
          //   StorageRef.getDownloadURL().then((downloadURL) => {
          //     // alert(downloadURL + " is download url");
          //     this.Audio = downloadURL;
          //     alert(downloadURL);
          //     var str = "<audio controls><source src=' " + this.Audio + " ' type='audio/wav'></audio>";
          //     document.getElementById('audio').innerHTML = str;
          //     // this.ImagePreviewVar = downloadURL;
          //     // document.getElementById('DvScroll').style.backgroundImage = "";
          //     // document.getElementById('DvScroll').style.backgroundColor = "black";
          //     // this.ImagePreview = 1;
          //     //  loader.dismiss();

          //   }, (error) => {
          //   });
          // }, (error) => {
          //   // alert("Error while putting");
          // });



        }, (err: CaptureError) => {

        });


    } else {
      alert(this.AudioText);
    }
  }


}

