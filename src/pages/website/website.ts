import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the WebsitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-website',
  templateUrl: 'website.html',
})
export class WebsitePage {
  my_url: any;
  SampleURL = "https://www.rajputfoundation.org/";
  constructor(public navCtrl: NavController, public navParams: NavParams, private sanitize: DomSanitizer) {
    this.my_url = this.getSantizedURL();
    // this.LoadFrame();
  }
  getSantizedURL() {
    return this.sanitize.bypassSecurityTrustResourceUrl(this.SampleURL);
  }

  // LoadFrame() {
  //   // var iframeURL = 'https://www.rajputfoundation.org/';
  //   var iframeURL = "https://www.rajputfoundation.org/login.php"
  //   var iframeID = 'MyIFrame';


  //   //pre-authenticate
  //    
  //   var req = new XMLHttpRequest();
    
  //   req.open("POST", iframeURL, false, "wajid", "3wajid"); //use POST to safely send combination
  //   req.send(null); //here you can pass extra parameters through

  //   //setiFrame's SRC attribute
  //   var iFrameWin : HTMLIFrameElement;
  //   iFrameWin = document.getElementById(iframeID) as HTMLIFrameElement;
  //   iFrameWin.src = "https://www.rajputfoundation.org/index.php";

  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WebsitePage');
  }

}
