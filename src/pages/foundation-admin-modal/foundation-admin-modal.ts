import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';

/**
 * Generated class for the FoundationAdminModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foundation-admin-modal',
  templateUrl: 'foundation-admin-modal.html',
})
export class FoundationAdminModalPage {

  AdminData : any;

  constructor(public navCtrl: NavController, public ObjHelper : HelperProvider ,public navParams: NavParams) {
    this.AdminData = ObjHelper.UserInfo.find(data => data.userid == 113);
     
  }

  CloseModal(){
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoundationAdminModalPage');
  }

}
