import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoundationAdminModalPage } from './foundation-admin-modal';

@NgModule({
  declarations: [
    FoundationAdminModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FoundationAdminModalPage),
  ],
})
export class FoundationAdminModalPageModule {}
