import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoundationCheckPage } from './foundation-check';

@NgModule({
  declarations: [
    FoundationCheckPage,
  ],
  imports: [
    IonicPageModule.forChild(FoundationCheckPage),
  ],
})
export class FoundationCheckPageModule {}
