import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

/**
 * Generated class for the FoundationCheckPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foundation-check',
  templateUrl: 'foundation-check.html',
})
export class FoundationCheckPage {

  AudioText = "";

  constructor(
    public navCtrl: NavController,
     public navParams: NavParams,
     private database: AngularFireDatabase
    ) {
      this.GetText();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoundationCheckPage');
  }

  GetText(){
    this.database.object('/Validation/').valueChanges().subscribe(data => {
      
      let arr = Object.keys(data);

      for (var i = 0; i < arr.length; i++) {
        const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
        this.AudioText = object2.Text;
      }
      

    })
  }

  Save(){
      // this.database.list("/Validation/").push({
      //   Text : this.AudioText,
      // });

      this.database.list('/Validation/').update("-LnsGF5ZIxokvrAle2_3", {
        Text: this.AudioText,
      })

  }

}
