import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { Http, Response } from '@angular/http';
import { DatabaseHelperProvider } from '../../providers/database-helper/database-helper';

/**
 * Generated class for the FoundationProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foundation-profile',
  templateUrl: 'foundation-profile.html',
})
export class FoundationProfilePage {

  ShowUserInfo: any;

  ObjReg = {
    Email: "",
    Password: "",
    ConfirmPassword: "",
    Name: "",
    FatherName: "",
    Gender: "",
    DOB: "",
    Country: "",
    Area: "",
    Village: "",
    Phone: "",
    Profession: "",
    pic: ""
  }



  constructor(public navCtrl: NavController, public ObjDatabase: DatabaseHelperProvider, private ObjHttp: Http, public navParams: NavParams, private ObjHelper: HelperProvider) {
    let Data = this.navParams.get('userid');
    if (Data == undefined) {
      Data = this.ObjHelper.LoggedInUserID;
    }
    this.ShowUserInfo = this.ObjHelper.UserInfo.find(items => items.userid == Data);
     
    this.ObjReg.Name = this.ShowUserInfo.name;
    this.ObjReg.Email = this.ShowUserInfo.email;
    this.ObjReg.Country = this.ShowUserInfo.country;
    this.ObjReg.Area = this.ShowUserInfo.area;
    this.ObjReg.Village = this.ShowUserInfo.village;
    this.ObjReg.pic = this.ShowUserInfo.pic;
    // this.ObjReg.Password = this.ShowUserInfo.email;
    // this.ObjReg.ConfirmPassword = this.ShowUserInfo.email;
    // this.ObjReg.FatherName = this.ShowUserInfo.email;
    // this.ObjReg.Gender = this.ShowUserInfo.email;
    // this.ObjReg.DOB = this.ShowUserInfo.email;

    // this.ObjReg.Phone = this.ShowUserInfo.email;
    // this.ObjReg.Profession = this.ShowUserInfo.email;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoundationProfilePage');
  }

  Update() {
    const Params = {
      ID: this.ShowUserInfo.userid,
      username: this.ObjReg.Name,
      email: this.ObjReg.Email,
      password: this.ObjReg.Password,
      name: this.ObjReg.Name,
      fathername: this.ObjReg.FatherName,
      gender: this.ObjReg.Gender,
      dob: this.ObjReg.DOB,
      location: "",
      area: this.ObjReg.Area,
      village: this.ObjReg.Village,
      phone: this.ObjReg.Phone,
      profession: this.ObjReg.Profession
    }
    this.ObjHttp.post(this.ObjHelper.ServerHost + "RegisterUser", Params).map((res: Response) => res.json()).subscribe((data) => {
       
      if (data.d == "Edited") {
        let query = "update TblAllUsers set username = '" + Params.username + "' , village ='" + Params.village + "' , area = '" + Params.area + "' , country = '" + this.ObjReg.Country + "' , email = '" + Params.email + "' where userid = " + Params.ID;
        this.ObjDatabase.SelectQuerySqlite(query).then((data: any) => {
          var Index = this.ObjHelper.UserInfo.findIndex(data => data.userid == Params.ID);
          this.ObjHelper.UserInfo[Index].username = Params.username;
          this.ObjHelper.UserInfo[Index].village = Params.village;
          this.ObjHelper.UserInfo[Index].area = Params.area;
          this.ObjHelper.UserInfo[Index].country = this.ObjReg.Country;
          this.ObjHelper.UserInfo[Index].email = Params.email;
          alert("Profile Updated");
          // this.ObjReg = {
          //   Email: "",
          //   Password: "",
          //   ConfirmPassword: "",
          //   Name: "",
          //   FatherName: "",
          //   Gender: "",
          //   DOB: "",
          //   Country: "",
          //   Area: "",
          //   Village: "",
          //   Phone: "",
          //   Profession: "",
          //   pic: ""
          // }
        }).catch((error2) => {
           
        })
      }
      else {
        alert("Something went wrong. Please check all fields");
      }
    }, (error) => {
       
    }
    )
  }

}
