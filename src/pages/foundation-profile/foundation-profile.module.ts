import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoundationProfilePage } from './foundation-profile';

@NgModule({
  declarations: [
    FoundationProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(FoundationProfilePage),
  ],
})
export class FoundationProfilePageModule {}
