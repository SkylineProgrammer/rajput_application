import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InoutHomePage } from './inout-home';

@NgModule({
  declarations: [
    InoutHomePage,
  ],
  imports: [
    IonicPageModule.forChild(InoutHomePage),
  ],
})
export class InoutHomePageModule {}
