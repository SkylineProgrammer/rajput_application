import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { FoundationNewChatPage } from '../foundation-new-chat/foundation-new-chat';
import { HelperProvider } from '../../providers/helper/helper';
import firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

/**
 * Generated class for the InoutHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inout-home',
  templateUrl: 'inout-home.html',
})
export class InoutHomePage {
  LogerGroup: any;
  GroupId: any;
  CurrentGroupUsers = [];
  GroupDetails :any ;

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Clearing ..... ",
      duration: 2000
    });
    loader.present();
  }


  constructor( public viewCtrl: ViewController ,public alertCtrl: AlertController, public loadingCtrl: LoadingController , public navCtrl: NavController, private database: AngularFireDatabase ,public navParams: NavParams, private ObjHelper: HelperProvider , private ViewCtrl: ViewController) {
    let Users = this.navParams.get("Users");
    if (Users != undefined)
      this.CurrentGroupUsers = Users;
      this.GroupId = this.navParams.get("GroupID");
      this.LogerGroup = this.navParams.get("GroupName");
      this.GroupDetails = this.navParams.get("GroupDteails");
    }

  
    BtnLogoutClick() {
      const confirm = this.alertCtrl.create({
        // title: 'Use this lightsaber?',
        message: 'Do you want to Clear Group ?? ',
        buttons: [
          {
            text: 'YES',
            handler: () => {
              this.ClearAllChat()
            }
          },
          {
            text: 'No',
            handler: () => {
              console.log('No clicked');
            }
          }
        ]
      });
      confirm.present();
  
    }



  ionViewDidLoad() {
    console.log('ionViewDidLoad InoutHomePage');
  }

  ShowGroupUsers() {
    // this.ViewCtrl.dismiss().then(() => {
       
      this.navCtrl.push(FoundationNewChatPage, { Users: this.CurrentGroupUsers });
    // });

  }

  MuteNoti(){

  }

  ClearAllChat(){
    this.presentLoading()
    let aa = this.GroupId;
    let bb = this.LogerGroup;
    let cc = this.GroupDetails;
    let dd = this.ObjHelper.LoggedInUserID;
    //  
    this.GroupDetails.forEach(Outerelement => {
      Outerelement.Msgs.forEach(Innerelement => {
        this.database.list("/TblGroupDetails/"+Innerelement.Key).push({
          ClearUserID : this.ObjHelper.LoggedInUserID
        }).then(res => {
          this.viewCtrl.dismiss();
        // alert('running');          
        });
      });
    });
  }

  
}
