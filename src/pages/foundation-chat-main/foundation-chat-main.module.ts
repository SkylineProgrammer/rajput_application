import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoundationChatMainPage } from './foundation-chat-main';

@NgModule({
  declarations: [
    FoundationChatMainPage,
  ],
  imports: [
    IonicPageModule.forChild(FoundationChatMainPage),
  ],
})
export class FoundationChatMainPageModule {}
