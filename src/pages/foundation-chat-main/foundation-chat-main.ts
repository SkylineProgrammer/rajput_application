import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FoundationChatPageDetailsPage } from '../foundation-chat-page-details/foundation-chat-page-details';
import { FoundationGroupChatPage } from '../foundation-group-chat/foundation-group-chat';
import { Http, Response, Headers } from '@angular/http';
import { HelperProvider } from '../../providers/helper/helper';
import firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { DatabaseHelperProvider } from '../../providers/database-helper/database-helper';
import { Network } from '@ionic-native/network';
import { FoundationChatLogingPage } from '../foundation-chat-loging/foundation-chat-loging';
import { WebsitePage } from '../website/website';
import { HomePage } from '../home/home';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { FoundationNewChatPage } from '../foundation-new-chat/foundation-new-chat';
import { FoundationOrgGroupsPage } from '../foundation-org-groups/foundation-org-groups';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { FoundationAdminModalPage } from '../foundation-admin-modal/foundation-admin-modal';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

/**
 * Generated class for the FoundationChatMainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foundation-chat-main',
  templateUrl: 'foundation-chat-main.html',
})
export class FoundationChatMainPage {

  Heading = 0;

  BindArray = [

  ]
  tblChat = "INSERT INTO tblMainChat (chatid, datechat, dp , groupname , image , lastmessage , recieverid , userid , username) VALUES ";
  tblGroup = "INSERT INTO tblMainGroup (Key, dp, groupid, groupname , grouptype , isadminview , lastmessage , lastmessagedate) VALUES ";
  tblChatDetails = "INSERT INTO tblChatDetails (chatid, isreply, message , messagedate , reciverid , replymessageid , status , userid , username ) VALUES ";
  tblGroupDetails = "INSERT INTO tblGroupDetails (groupid, grouptype, isreply , message , messagedate , replymessageid , status , userid , username , village , isSync) VALUES ";

  // new
  tblOrgGroup = "INSERT INTO TblOrgGroups (GroupName , GroupDP) VALUES ";
  tblOrgUser = "INSERT INTO TblOrgUser (GroupName , UserID , Status) VALUES ";
  OrgUser = [];
  OneTime = true;
  VillageCount = 0;
  AreaCount = 0;
  CityCount = 0;
  AdministratorCount = 0;

  isSearch = 0;

  isDeletedOn = 0;

  GotoWebsite() {
    this.navCtrl.push(WebsitePage);
  }

  SendingDemoNotification() {
    console.log("Got into Method");
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'key=' + "AAAAAc8s12s:APA91bHQgjTknnYUx8FQvddOmtLAMSsQDc38xlXOZ_V_SDn0tiZmDk0dow49P6Ut5zcxzVTmAoFlzAU3SZEh7__4VESNrGAf4aGJAhp5NsOArIwcGWDhgPb3M18uuPgN-itPkr_xhLWn");

    let body = {
      "notification": {
        "body": "This is is demo notification",
        "badge": 1,
        "sound": "default"
      },
      "priority": "high",
      "to": this.ObjHelper.FCMToken
    }
    console.log("Sending Notification")
    this.ObjHttp.post('https://fcm.googleapis.com/fcm/send', JSON.stringify(body), { headers: headers })
      .map(res => res.json())
      .subscribe(data => {
        console.log(data);
        console.log("Notification Sent");
        console.log(data);
      }, (error) => {
        console.log("Got Error")
        console.log(error);
      }
      );
  }

  UpdateChatDetails(params) {
    if (params != undefined) {


      // this.database.list("/TblChatDetails/"+snapshot.key).push({
      //   ClearUserID : this.ObjHelper.LoggedInUserID
      // }).then(res => {
      // // alert('running');          
      // });
      var OneTime = 0;
      this.database.object('/TblChatDetails/').valueChanges().subscribe(data => {
        if (OneTime == 0) {
          OneTime = 1;
          let arr = Object.keys(data);
          for (var i = 0; i < arr.length; i++) {
            const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
            if (object2.chatid == params) {
              this.database.list("/TblChatDetails/" + object2.Key).push({
                ClearUserID: this.ObjHelper.LoggedInUserID
              }).then(res => {
                // alert('running');          
              });
            }
          }

        }


      })

    }
  }

  DeleteChat(Params) {
    this.database.list('/TblChat/').remove(Params.ChatKey).then(data => {
      this.isDeletedOn = 1;

      this.UpdateChatDetails(Params.chatid);
      this.GetChatsFirebase();
      const loader = this.loadingCtrl.create({
        content: "Please wait...",
        duration: 1500
      });
      loader.present();
    })
  }

  DeleteshowConfirm(param) {
    const confirm = this.alertCtrl.create({
      // title: 'Use this lightsaber?',
      message: 'Delete Permanently',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            this.DeleteChat(param);
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  // public ObjDatabase : DatabaseHelperProvider
  constructor(public navCtrl: NavController, private modalCtrl: ModalController, public alertCtrl: AlertController, public loadingCtrl: LoadingController, private photoViewer: PhotoViewer, public navParams: NavParams, private ObjHttp: Http, private ObjHelper: HelperProvider, private database: AngularFireDatabase, public ObjNetwork: Network, public ObjDatabase: DatabaseHelperProvider) {
    // const loader = this.loadingCtrl.create({
    //   content: "Please wait...",
    //   duration: 2000
    // });
    // loader.present();

    // this.GetChats();

    //// REMOVE THIS BEFORE APK
    // this.GetOrgtablesFromServer();
    this.ObjHelper.UserInfo;
    debugger;
    let network = this.ObjNetwork.type;
    // back
    // if (network == "none" || network == "unknown") {
    // this.GetOrTablefromSQLITE();
    // } else {
    // this.CompareOrgGroupsFromServer();
    // }


    // let connectSubscription = this.ObjNetwork.onConnect().subscribe(() => {
    //   // this.SendingDemoNotification();

    //   console.log('network connected!');
    //   // We just got a connection but we need to wait briefly
    //   // before we determine the connection type. Might need to wait.
    //   // prior to doing any api requests as well.
    //   setTimeout(() => {
    //     console.log(this.ObjNetwork.type + "From On COnnet");
    //     if (this.ObjNetwork.type === 'wifi') {
    //       console.log('we got a wifi connection, woohoo!');
    //     }
    //   }, 3000);
    // });


    // let network = this.ObjNetwork.type;
    // alert("network is " + network)
    // setTimeout(() => {
    if (network == "none" || network == "unknown") {
      // alert("NON");
      setTimeout(() => {
        this.GetChatFromSQlite();
      }, 1000);
    } else {
      // alert("YES");
      this.GetAllDataFromFireBaseAndInsertInSqlite();
      setTimeout(() => {

        // Main Group
        this.ObjDatabase.InsertQuerySqlite("delete from tblMainGroup", []).then(data => {
          console.log("tblMainGroup deleted");
          this.ObjDatabase.InsertQuerySqlite(this.tblGroup, []).then(data => {
            //alert("tblMainGroup Inserted");
          })
        })

        // Main Chat
        this.ObjDatabase.InsertQuerySqlite("delete from tblMainChat", []).then(data => {
          console.log("tblMainChat Inserted");
          this.ObjDatabase.InsertQuerySqlite(this.tblChat, []).then(data => {
            //alert("Hey Hey 2 tblMainChat Inserted");
          })
        })

        //CHat Details

        var query = this.tblChatDetails.slice(0, -1);
        console.clear();
        console.log(query);
        this.ObjDatabase.InsertQuerySqlite("delete from tblChatDetails", []).then(data => {
          console.log("tblChatDetails Details Deleted");
          this.ObjDatabase.InsertQuerySqlite(query, []).then(data => {
            //alert("tblChatDetails Details Inserted");
          })
        })


        var query2 = this.tblGroupDetails.slice(0, -1);
        console.log(query);
        this.ObjDatabase.InsertQuerySqlite("delete from tblGroupDetails", []).then(data => {
          console.log("tblGroupDetails Details Deleted");
          this.ObjDatabase.InsertQuerySqlite(query2, []).then(data => {
            //alert("tblGroupDetails Details Inserted");
          })
        })
        console.log("Inserted Data");
      }, 8000);



      setTimeout(() => {
        let aa = this.BindArray;
        let bb = this.ObjHelper.isAdmin;

      }, 20000);



    }
    // }, 2000);


  }

  ionViewWillEnter() {
    this.VillageCount = this.ObjHelper.GetYourVillageUserCount();
    this.AreaCount = this.ObjHelper.GetYourAreaUserCount();
    this.CityCount = this.ObjHelper.GetYourCityCount();
    this.AdministratorCount = this.ObjHelper.GetYourAdministratorUserCount();
    var aa = this.ObjHelper.VillageCountHelper;
    var bb = this.ObjHelper.AreaCountHelper;
    var cc = this.ObjHelper.AreaCountHelper;

  }

  GetAllDataFromFireBaseAndInsertInSqlite() {
    this.GetChatsFirebase();
    //Commented by Haroon because added in GetChatsFirebase() method
    // this.GetUserChat();
    this.GetUserGroupDetails();
  }

  // 16/7/2019
  presentModal(param, GroupID) {

    // if (param == "https://firebasestorage.googleapis.com/v0/b/rajputchat-42dc6.appspot.com/o/546.PNG?alt=media&token=71504f9f-0a4c-41e5-938a-2f5e66ccc466") {
    if (GroupID == 5) {
      const modal = this.modalCtrl.create(FoundationAdminModalPage);
      modal.present();
    } else {
      // const modal = this.modalCtrl.create(FoundationModalPage);
      // modal.present();
      this.photoViewer.show(param);
    }

  }

  ArrayCompletedLength = [];
  GetChatsFirebase() {
    this.BindArray = [];

    this.database.object('/TblGroup/').valueChanges().subscribe(data => {
      let arr = Object.keys(data);
      for (var i = 0; i < arr.length; i++) {
        // const object2 = Object.assign({ Key: arr[i] } , data[arr[i]]);
        // const object2 = Object.assign({ Key: arr[i] }, { UnreadMsgQun: this.GetGroupMsgCount(data[arr[i]].groupid) }, data[arr[i]]);
        const object2 = Object.assign({ Key: arr[i] }, { chatid: 0 }, data[arr[i]]);
        if (object2.groupname == "Admin") {
          let Value = this.ObjHelper.UserInfo.find(items => items.userid == 113);

          if (Value != undefined)
            if (Value.pic.includes("firebasestorage"))
              object2.dp = Value.pic
            else
              object2.dp = "https://rajputfoundation.org/" + Value.image;
          else
            object2.dp = "";
          // alert("Image of Admin ~" + object2.dp);
        }
        this.BindArray.push(object2);
        this.SortArray();

        if (i == arr.length - 1) {
          // if (this.ArrayCompletedLength.find(items => items == "C") == undefined)
          //   this.ArrayCompletedLength.push("C")
          if (this.ArrayCompletedLength.find(items => items == "G") == undefined)
            this.ArrayCompletedLength.push("G")

          // this.ArrayCompletedLength += 2;
          if (this.ArrayCompletedLength.length >= 3)
            this.SortArray();
        }
        this.tblGroup = this.tblGroup + "('" + object2.Key + "','" + object2.dp + "','" + object2.groupid + "','" + object2.groupname + "','" + object2.grouptype + "','" + object2.isadminview + "','" + object2.lastmessage + "','" + object2.lastmessagedate + "'),"
      }

      this.tblChat = this.tblChat.slice(0, -1);
      this.tblGroup = this.tblGroup.slice(0, -1);

      // console.log(tblGroup);
      //  
      console.log(this.BindArray[0].groupid + " is groupid");
    })

    // var ref = firebase.database().ref().child('TblChat');
    console.log("Getting Data from Table Chat and Group");
    //alert("Logged In UserID is " + this.ObjHelper.LoggedInUserID);
    setTimeout(() => {
      this.database.object('/TblChat/').valueChanges().subscribe((data) => {
        if (data != null) {
          let arr = Object.keys(data);

          for (var i = 0; i < arr.length; i++) {

            if (data[arr[i]].userid == this.ObjHelper.LoggedInUserID) {
              let ChatID = data[arr[i]].chatid;
              this.GetUserChatDetails(ChatID);
              let Value = Object.assign(data[arr[i]], { ChatKey: arr[i] });
              // Changed by  Haroon on 21/5/19
              //Commending only for update
              // if (this.BindArray.find(items => items.chatid == Value.chatid) != undefined) {
              //   this.BindArray.splice(this.BindArray.indexOf(this.BindArray.find(items => items.chatid == Value.chatid)), 1);
              //   this.BindArray.push(Value);
              // }
              // else
              //   this.BindArray.push(Value);

              //Doing for update only . This is jugaad
              // ================================================
              if (i == arr.length - 1) {

                console.log(this.BindArray);
              }
              // Changed by  Haroon on 21/5/19
              if (this.BindArray.find(items => items.groupname == Value.username) != undefined) {
                this.BindArray.splice(this.BindArray.indexOf(this.BindArray.find(items => items.groupname == Value.username)), 1);
                this.BindArray.push(Value);
                this.SortArray();

              }
              else {
                this.BindArray.push(Value);
                this.SortArray();

              }

              if (i == arr.length - 1) {
                if (this.ArrayCompletedLength.find(items => items == "C") == undefined)
                  this.ArrayCompletedLength.push("C")
                // if (this.ArrayCompletedLength.find(items => items == "G") == undefined)
                //   this.ArrayCompletedLength.push("G")

                // this.ArrayCompletedLength += 2;
                if (this.ArrayCompletedLength.length >= 3)
                  this.SortArray();
                // this.BindArray.filter(item => {
                //   debugger;
                //   if (item.chatid > 0) {
                //     this.BindArray.splice(this.BindArray.findIndex(x => x == item), 1);
                //     this.BindArray.splice(0, 0, item);
                //   }

                // }

                // )
              }
              // ======================================================
              // if (this.BindArray.filter(items => items.chatid == Value.chatid).length == 0)
              //   this.BindArray.push(Value);
              console.log("HOHOHOHOHOHOHHOOHOH Got Chats too");
              console.log("TblChat " + Value.userid);
              console.log("Hey Hey Hey Inserting Data in tblChat variable");
              //  
              this.tblChat = this.tblChat + "('" + Value.chatid + "','" + Value.datechat + "','" + Value.dp + "','" + Value.groupname + "','" + Value.image + "','" + Value.lastmessage + "','" + Value.recieverid + "','" + Value.userid + "','" + Value.username + "'),"
              // console.log(tblChat);

              let Index = this.BindArray.indexOf(this.BindArray.find(items => items.chatid == data[arr[i]].chatid));
              if (Index >= 0) {

                this.BindArray[Index].groupname = Value.username;

                //Commented by Haroon on 23/6/19
                if (this.ObjHelper.UserInfo.find(items => items.userid == this.BindArray[Index].recieverid).pic == "avatar_default.png") {
                  this.BindArray[Index].dp = "https://www.rajputfoundation.org/image/logo2.png";
                } else {
                  this.BindArray[Index].dp = "https://rajputfoundation.org/" + this.ObjHelper.UserInfo.find(items => items.userid == this.BindArray[Index].recieverid).pic;
                }

                // this.BindArray[Index].dp = "https://rajputfoundation.org/" + Value.image;
                // this.BindArray[Index].UnreadMsgQun = this.GetChatMsgCount(this.BindArray[Index].chatid);
                // this.BindArray[Index].UnreadMsgQun = 0;
              }
            }


          }

        }
      })
    }, 1000);

    if (this.isDeletedOn == 1) {
      // alert('Local ORG');
      this.GetOrTablefromSQLITE();
    } else {
      // alert('Server ORG');
      this.CompareOrgGroupsFromServer();
    }


  }

  SortArray() {

    let Arr = [];
    let Groups = this.BindArray.filter(items => items.groupid > 0 && items.chatid == 0);
    let Chats = this.BindArray.filter(items => items.chatid > 0).reverse();
    let Org = this.BindArray.filter(items => items.chatid == -1 && items.groupid == -1);

    for (let index = 0; index < Groups.length; index++) {
      Arr.push(Groups[index]);
    }
    for (let index = 0; index < Org.length; index++) {
      Arr.push(Org[index]);
    }
    for (let index = 0; index < Chats.length; index++) {
      Arr.push(Chats[index]);
    }

    this.BindArray = [];
    this.BindArray = Arr;
  }

  IsReadyToSearch = false;
  FilteredProfiles = [];
  onInput(event) {
    // Reset items back to all of the items
    // this.initializeItems();
    this.IsReadyToSearch = true;
    // set q to the value of the searchbar
    var q = event.srcElement.value;
    // alert(q);

    // if the value is an empty string don't filter the items
    if (!q) {
      this.FilteredProfiles = this.ObjHelper.UserInfo
      this.IsReadyToSearch = false;
      return;
    }

    // this.ObjHelper.UserInfo.filter((v) => {

    // });
    // alert(this.ObjHelper.UserInfo[0].name);
    // alert(this.ObjHelper.UserInfo[0].userid)
    // alert(this.ObjHelper.UserInfo[0].username)
    // alert(this.ObjHelper.UserInfo[0].name)

    this.FilteredProfiles = this.ObjHelper.UserInfo.filter((v) => {
      // if (v.name != undefined)
      //   alert(v.name);
      if (v.name && q && v.userid != this.ObjHelper.LoggedInUserID) {

        if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          // if (v.name != undefined)
          //   alert("Found " + v.name + q);
          return true;
        }
        return false;
      }
    });

    console.log(q + this.FilteredProfiles.length);
  }

  onCancelInput(event) {

    console.log(event);
    this.IsReadyToSearch = false;
    this.FilteredProfiles = this.FilteredProfiles;
    this.isSearch = 0;
  }

  BtnLogoutClick() {
    const confirm = this.alertCtrl.create({
      // title: 'Use this lightsaber?',
      message: 'LogOut?',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            this.ObjHelper.UpdateUserLastSeen(false);
            this.Logout();
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('No clicked');
          }
        }
      ]
    });
    confirm.present();

  }

  Logout() {
    this.ObjHelper.UpdateUserLastSeen(false);

    // alert("Logout");
    this.ObjDatabase.InsertQuerySqlite("delete from tblMainGroup", []).then(data => {
    })

    //Main Chat
    this.ObjDatabase.InsertQuerySqlite("delete from tblMainChat", []).then(data => {
    })

    //Chat Details
    var query = this.tblChatDetails.slice(0, -1);
    this.ObjDatabase.InsertQuerySqlite("delete from tblChatDetails", []).then(data => {
    })

    //Group Details       
    var query2 = this.tblGroupDetails.slice(0, -1);
    this.ObjDatabase.InsertQuerySqlite("delete from tblGroupDetails", []).then(data => {
    })

    // ==========================================================
    this.ObjDatabase.InsertQuerySqlite("delete from TblAllUsers", []).then(data => {
    })
    this.ObjDatabase.InsertQuerySqlite("delete from TblOrgGroups", []).then(data => {
    })
    this.ObjDatabase.InsertQuerySqlite("delete from TblOrgUser", []).then(data => {
    })
    this.ObjDatabase.InsertQuerySqlite("delete from tblOrgTableDetails", []).then(data => {
    })
    // this.ObjDatabase.InsertQuerySqlite("delete from tblGroupDetails", []).then(data => {
    // })

    // ==========================================================
    // 
    // Users
    this.ObjDatabase.InsertQuerySqlite("delete from TblCurrentLoggedInUser", []).then(data => {
      // this.navCtrl.setRoot(FoundationChatLogingPage);
      // this.navCtrl.setRoot(HomePage);
      this.navCtrl.push(FoundationChatLogingPage).then(() => {
        const index = this.navCtrl.getActive().index;
        this.navCtrl.remove(0, index);
      });
    })

  }

  test = 0;

  GetUserChatDetails(ChatID) {
    var ref = firebase.database().ref().child('TblChatDetails');
    ref.orderByChild('chatid').startAt(ChatID).endAt(ChatID).on('child_added', (snapshot) => {
      //  
      let Value = snapshot.val();
      // INSERT INTO tblChatDetails (chatid, isreply, message , messagedate , reciverid , replymessageid , status , userid) VALUES
      if (this.test < 51) {
        this.tblChatDetails = this.tblChatDetails + "('" + Value.chatid + "' , '" + Value.isreply + "' , '" + Value.message + "' , '" + Value.messagedate + "' , '" + Value.reciverid + "' , '" + Value.replymessageid + "' , '" + Value.status + "' , '" + Value.userid + "' , '" + Value.username + "' ),"
        this.test += 1;
      }
    })
  }

  VisitWebsite() {
    this.navCtrl.push(WebsitePage);
  }

  GetGroupDetailCount(UserID) {
    // var ref = firebase.database().ref().child('TblGroupCounter');
    // this.isnew = true;
    // ref.orderByChild('recieverid').startAt(UserID).endAt(UserID).on('child_added', (snapshot) => {
    this.database.object('/TblGroupCounter/').valueChanges().subscribe((data) => {

      for (let j = 0; j < this.BindArray.length; j++) {
        if (this.BindArray[j].groupid > 0)
          this.BindArray[j].UnreadMsgQun = 0;
      }
      // this.ChatMeg = snapshot.val();
      let array = [];
      if (data != null)
        array = Object.keys(data);
      for (let i = 0; i < array.length; i++) {
        const ValueObject = Object.assign({ Key: array[i] }, data[array[i]]);

        if (ValueObject.recieverid == this.ObjHelper.LoggedInUserID) {
          let ObjectValue = this.BindArray.find(items => items.groupid == ValueObject.groupid);
          if (ObjectValue != undefined) {
            this.BindArray[this.BindArray.indexOf(ObjectValue)].UnreadMsgQun++;
            if (isNaN(this.BindArray[this.BindArray.indexOf(ObjectValue)].UnreadMsgQun))
              this.BindArray[this.BindArray.indexOf(ObjectValue)].UnreadMsgQun = 1;
          }
        }
      }
      // let Value = snapshot.val();
      // if (Value.userid == this.ObjHelper.LoggedInUserID) {

      // if (UnReadMsgCount == undefined)
      //   UnReadMsgCount = 0;
      // UnReadMsgCount = Value.UnreadMsgQun;
      // if (this.navCtrl.getActive().component == FoundationChatMainPage) {

      // }
      // }
    })
  }

  GetUserGroupDetails() {
    this.database.object('/TblGroupDetails/').valueChanges().subscribe(data => {

      let arr = Object.keys(data);
      for (var i = 0; i < arr.length; i++) {
        const object2 = Object.assign({ Key: arr[i] }, { chatid: 0 }, data[arr[i]]);
        // tblGroupDetails = "INSERT INTO tblGroupDetails (groupid, grouptype, isreply , message , messagedate , replymessageid , status , userid , username , village) VALUES";
        // 
        // object2.village = object2.groupid == 1 ? object2.village : object2.groupid == 2 ? 

        // if (this.BindArray.length > 0) {
        // if (object2.status == "Send" || object2.status == "Recieved") {
        //   let index = this.BindArray.indexOf(this.BindArray.find(items => items.groupid == object2.groupid));
        //   if (index >= 0) {
        //     this.BindArray[index].UnreadMsgQun++;
        //   }
        // }
        // }
        this.tblGroupDetails = this.tblGroupDetails + "(" + object2.groupid + ",'" + object2.grouptype + "','" + object2.isreply + "','" + object2.message + "','" + object2.messagedate + "','" + object2.replymessageid + "','" + object2.status + "','" + object2.userid + "','" + object2.username + "','" + object2.village + "', 1 ),"
      }

      if (this.BindArray.length > 0)
        this.GetGroupDetailCount(this.ObjHelper.LoggedInUserID);

    });
  }

  NewChat(param) {
    this.navCtrl.push(FoundationNewChatPage);
  }

  GetGroupMsgCount(GroupID) {
    var Count = 0;
    this.database.object('/tblGroupDetails/').valueChanges().subscribe((data) => {
      let arr = Object.keys(data);
      for (var i = 0; i < arr.length; i++) {
        const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
        if (object2.groupid == GroupID && object2.status == "Send") {
          Count++;
        }
      }
      return Count;
    })
  }

  GetChatFromSQlite() {
    console.log("Getting Data from SQLite");
    this.ObjDatabase.SelectQuerySqlite("select * from tblMainChat").then((data: any) => {

      console.log("Length of array before loop of tblmainChat " + this.BindArray.length)
      data.forEach(element => {
        this.BindArray.push(element);
        let Index = this.BindArray.indexOf(this.BindArray.find(items => items.chatid == element.chatid));
        this.BindArray[Index].groupname = element.username;
        this.BindArray[Index].dp = "https://rajputfoundation.org/" + element.image;
        this.SortArray();

      })
      // .then((data) => {
      console.log("After ForEach of all Main Chat the length is " + this.BindArray.length);
      // });

      this.ObjDatabase.SelectQuerySqlite("select * from tblMainGroup").then((data: any) => {

        console.log("Length of array before tblgroup array " + this.BindArray.length)
        data.forEach(element => {
          this.BindArray.push(element);
          this.SortArray();

        })
        // .then((data) => {
        console.log("After Foreach of All Main Group the length of array " + this.BindArray.length);
        // }
        // , (error) => {
        // console.log(error);
        // this.GetAllDataFromFireBaseAndInsertInSqlite();
        // }
        // )
      })

      this.GetOrTablefromSQLITE();


    }
      , (error) => {
        console.log(error);
        this.GetAllDataFromFireBaseAndInsertInSqlite();
      }
    )
  }

  GetJSONString(Json: string) {
    return Json.substring(Json.indexOf("["), Json.indexOf("</string>"));
  }

  StartPersonalChat(Params) {
    this.navCtrl.push(FoundationChatPageDetailsPage, { UserData: Params.userid, username: Params.username, image: "https://rajputfoundation.org/" + Params.image });
  }

  GotoDetails(param) {

    // if (param.isGroup) {
    if (param.chatid > 0) {
      this.navCtrl.push(FoundationChatPageDetailsPage, { UserData: param.recieverid, ChatID: param.chatid, chatkey: param.chatkey, username: param.username, image: param.dp });
    }
    // New Block
    else if (param.chatid == -1 && param.groupid == -1) {
      this.navCtrl.push(FoundationOrgGroupsPage, { GroupName: param.groupname });
    }
    // New Block

    else {
      this.navCtrl.push(FoundationGroupChatPage, { GroupID: param.groupid, GroupName: param.groupname })

    }
    // } else {
    //   this.navCtrl.push(FoundationChatPageDetailsPage, { data: param })
    // }
  }

  GetChatMsgCount(ChatId) {
    var Count = 0;
    this.database.object('/tblChatDetails/').valueChanges().subscribe(data => {
      let arr = Object.keys(data);
      for (var i = 0; i < arr.length; i++) {
        const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
        if (object2.chatid == ChatId && object2.status == "Send") {
          Count++;
        }
      }
      return Count;
    })
  }

  // New Block

  BindOrgTabFromFirebase() {

    this.database.object('/TblOrgGroups/').valueChanges().subscribe((data) => {

      let Count = 0;
      if (data != null) {
        let arr = Object.keys(data);
        for (var i = 0; i < arr.length; i++) {
          const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
          var Obj = {
            isadminview: 0,
            chatid: -1,
            groupname: object2.GroupName,
            dp: object2.GroupDP,
            groupid: -1,
            HeadingCount: Count,
            OrgUserCount: this.OrgUserCount(object2.GroupName, this.OrgUser)
          }

          // alert(Obj.OrgUserCount);
          ////UNCOMMENT THIS BEFORE APK
          // this.OrgUser = [{ "UserID": "114", "GroupNames": ["Rajput Foundation", "Rana Foundation"], "UserStatus": "unblock", "UserGroup": "Rajput Foundation" }, { "UserID": "219", "GroupNames": ["Rajput Foundation", "Rana Foundation"], "UserStatus": "unblock", "UserGroup": "Rajput Foundation" }, { "UserID": "113", "GroupNames": ["Rajput Foundation", "Rana Foundation"], "UserStatus": "unblock", "UserGroup": "Rajput Foundation" }, { "UserID": "219", "GroupNames": ["Rajput Foundation", "Rana Foundation"], "UserStatus": "unblock", "UserGroup": "Rajput Foundation" }, { "UserID": "212", "GroupNames": ["Rajput Foundation", "Rana Foundation"], "UserStatus": "unblock", "UserGroup": "Rajput Foundation" }, { "UserID": "87", "GroupNames": ["Rajput Foundation", "Rana Foundation"], "UserStatus": "unblock", "UserGroup": "Rana Foundation" }, { "UserID": "113", "GroupNames": ["Rajput Foundation", "Rana Foundation"], "UserStatus": "unblock", "UserGroup": "Rana Foundation" }]
          // let index = this.OrgUser.findIndex(x => x.UserGroup == Obj.groupname && x.UserID == this.ObjHelper.LoggedInUserID)
          let index = this.OrgUser.findIndex(x => x.GroupName == Obj.groupname && x.UserID == this.ObjHelper.LoggedInUserID)

          if (index != -1) {
            let Arrayindex = this.BindArray.findIndex(x => x.groupname == Obj.groupname);
            if (Arrayindex <= 0) {
              this.BindArray.push(Obj);
              this.SortArray();

              if (i == arr.length - 1) {
                if (this.ArrayCompletedLength.find(items => items == "O") == undefined)
                  this.ArrayCompletedLength.push("O")
                // this.ArrayCompletedLength++;
                if (this.ArrayCompletedLength.length >= 3)
                  this.SortArray();
              }
            }
            Count++
            // else
            //   alert("Already exists");
          }
        }
      }
    })

  }

  GetOrgUserFromSqlite() {
    this.ObjDatabase.SelectQuerySqlite("select * from TblOrgUser").then((data: any) => {
      this.OrgUser = data;
      let network = this.ObjNetwork.type;
      if (network == "none" || network == "unknown") {

      } else {
        this.BindOrgTabFromFirebase();
      }
    })
  }

  GetOrgtablesFromServer() {
    this.ObjHttp.get(this.ObjHelper.ServerHost + "GetAllOrgGroups").map((res: Response) => res.text()).subscribe((data) => {
      let jsonString = this.GetJSONString(data);
      let UserInfo = JSON.parse(jsonString);


      //Commented by Haroon on 30-6-19
      // for (let index = 1; index < UserInfo.length; index++) {
      //   this.tblOrgUser = this.tblOrgUser + "('" + UserInfo[index].UserGroup + "' , '" + UserInfo[index].UserID + "' , '" + UserInfo[index].UserStatus + "' ),";
      // }

      for (let index = 0; index < UserInfo.length; index++) {
        this.tblOrgUser = this.tblOrgUser + "('" + UserInfo[index].UserGroup + "' , '" + UserInfo[index].UserID + "' , '" + UserInfo[index].UserStatus + "' ),";
      }
      var query = this.tblOrgUser.slice(0, -1);
      // alert(query);
      this.ObjDatabase.InsertQuerySqlite("delete from TblOrgUser", []).then(data => {
        // console.log("tblGroupDetails Details Deleted");
        this.ObjDatabase.InsertQuerySqlite(query, []).then(data => {
          // alert("Org User");
        })
      })

      this.database.list('/TblOrgGroups/').remove();

      UserInfo[0].GroupNames.forEach(element => {
        this.tblOrgGroup = this.tblOrgGroup + "('" + element + "','https://www.rajputfoundation.org/image/logo2.png' ),";
        // " INSERT INTO TblOrgGroups (GroupName , GroupDP) VALUES 
        var Obj = {
          GroupName: element,
          GroupDP: "https://www.rajputfoundation.org/image/logo2.png"
        }
        this.database.list("/TblOrgGroups/").push(Obj).then(x => {
          var query2 = this.tblOrgGroup.slice(0, -1);
          // alert(query2);
          this.ObjDatabase.InsertQuerySqlite("delete from TblOrgGroups", []).then(data => {
            console.log("tblGroupDetails Details Deleted");

            this.ObjDatabase.InsertQuerySqlite(query2, []).then(data => {
              // alert("Org Groups inserted");
              this.GetOrgUserFromSqlite();

            }
              // , (error) => {
              //   alert("Error while adding TblOrgGroups");
              // }
            )
          })
          console.log("Inserted Data");


          // ====== /
          // this.BindOrgTabFromFirebase();

        })
      });

    }
      , (error) => {
        //alert("Got error while getting all users data from cloud");
        return error(false)
      })
  }

  CompareOrgGroupsFromServer() {

    let ServerArray = [];
    let DatabaseArray = [];
    let FireBaseArray = [];
    this.ObjHttp.get(this.ObjHelper.ServerHost + "GetAllOrgGroups").map((res: Response) => res.text()).subscribe((data) => {
      let jsonString = this.GetJSONString(data);
      let FullArray = JSON.parse(jsonString);
      if (FullArray.length > 0)
        ServerArray = FullArray[0].GroupNames;

      this.ObjDatabase.SelectQuerySqlite("select * from TblOrgGroups").then((SQLdata: any) => {
        DatabaseArray = SQLdata;
        this.database.object('/TblOrgGroups/').valueChanges().subscribe((data) => {
          if (data != null) {
            let arr = Object.keys(data);
            for (var i = 0; i < arr.length; i++) {
              const object2 = Object.assign({ Key: arr[i] }, data[arr[i]]);
              FireBaseArray.push(object2);
            }
          }
          if (this.OneTime) {
            this.OneTime = false;

            if (this.LetsCompare(ServerArray, FireBaseArray, DatabaseArray)) {
              this.GetOrgtablesFromServer();
              // this.ObjDatabase.SelectQuerySqlite("select * from TblOrgGroups").then(data => {
              //   
              // })
            } else {
              // this.ObjDatabase.SelectQuerySqlite("select * from TblOrgGroups").then(data => {
              //   
              // })
              this.GetOrTablefromSQLITE();
            }
          }

        })

      })


    }
      , (error) => {
        //alert("Got error while getting all users data from cloud");
        return error(false)
      })

  }

  LetsCompare(ArrayOne, ArrayTwo, ArrayThree) {

    if (ArrayOne.length != ArrayTwo.length) {
      // alert(true + "1");
      return true;
      // for(let index = 1; index < ArrayOne.length; index++){

      // }    
    } else if (ArrayOne.length != ArrayThree.length) {
      // alert(true + "2");
      return true;
    }
    else {
      // alert(false);
      return false;
    }
  }

  OrgUserCount(GroupName, AllORGUser) {
    // this.ObjDatabase.SelectQuerySqlite("select * from TblOrgUser").then((InnerDATA: any) => {

    // })
    var Count = 0;
    AllORGUser.forEach(element => {
      if (element.GroupName == GroupName) {
        Count++;
      }
    });
    return Count;
  }

  GetOrTablefromSQLITE() {
    this.ObjDatabase.SelectQuerySqlite("select DISTINCT GroupName from TblOrgGroups").then((data: any) => {

      if (this.isDeletedOn == 1) {
        setTimeout(() => {
          this.ObjDatabase.SelectQuerySqlite("select * from TblOrgUser").then((InnerDATA: any) => {
            // 
            this.OrgUser = InnerDATA;
            let network = this.ObjNetwork.type;
            let Count = 0;
            // alert(JSON.stringify(data));
            data.forEach(element => {
              var Obj = {
                isadminview: 0,
                chatid: -1,
                groupname: element.GroupName,
                dp: element.GroupDP,
                groupid: -1,
                HeadingCount: Count
              }

              // alert(JSON.stringify(this.OrgUser));
              let index = this.OrgUser.findIndex(x => x.GroupName == Obj.groupname && x.UserID == this.ObjHelper.LoggedInUserID)
              // alert(index)
              if (index != -1) {
                // alert("inserted " + Obj.groupname)
                this.BindArray.push(Obj);
                // this.SortArray();

                Count = Count + 1;

                if (Count == data.length - 1) {
                  if (this.ArrayCompletedLength.find(items => items == "O") == undefined)
                    this.ArrayCompletedLength.push("O")
                  // this.ArrayCompletedLength++;
                  if (this.ArrayCompletedLength.length >= 3)
                    this.SortArray();
                }
              }
              // else
              //   alert("Not inserting " + Obj.groupname);
            });

          })
        }, 3000);
      } else {
        this.ObjDatabase.SelectQuerySqlite("select * from TblOrgUser").then((InnerDATA: any) => {

          this.OrgUser = InnerDATA;
          let network = this.ObjNetwork.type;
          let Count = 0;
          // alert(JSON.stringify(data));
          data.forEach(element => {
            var Obj = {
              isadminview: 0,
              chatid: -1,
              groupname: element.GroupName,
              dp: element.GroupDP,
              groupid: -1,
              HeadingCount: Count,
              OrgUserCount: this.OrgUserCount(element.GroupName, InnerDATA)
            }
            // alert(Obj.OrgUserCount);
            // alert(JSON.stringify(this.OrgUser));
            let index = this.OrgUser.findIndex(x => x.GroupName == Obj.groupname && x.UserID == this.ObjHelper.LoggedInUserID)
            // alert(index)
            if (index != -1) {
              // alert("inserted " + Obj.groupname)
              this.BindArray.push(Obj);
              this.SortArray();

              Count = Count + 1;

              if (Count == data.length - 1) {
                if (this.ArrayCompletedLength.find(items => items == "O") == undefined)
                  this.ArrayCompletedLength.push("O")
                // this.ArrayCompletedLength++;
                if (this.ArrayCompletedLength.length >= 3)
                  this.SortArray();
              }
            }
            // else
            //   alert("Not inserting " + Obj.groupname);
          });

        })
      }

    })
  }

  // New Block


  GetChats() {
    const Params = {};
    this.ObjHttp.post(this.ObjHelper.ServerHost + "GetChats", Params).map((res: Response) => res.json()).subscribe((data) => {

      // let jsonString = this.GetJSONString(data.d);
      this.BindArray = JSON.parse(data.d);
      console.log(this.BindArray)
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoundationChatMainPage');
  }

}
