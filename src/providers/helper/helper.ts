
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { DatabaseHelperProvider } from '../database-helper/database-helper';
import { FoundationChatMainPage } from '../../pages/foundation-chat-main/foundation-chat-main';
import { NavController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { AngularFireDatabase } from '@angular/fire/database';

/*
  Generated class for the HelperProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HelperProvider {

  // ServerHost = "http://localhost:18857/services/Testing.asmx/";
  ServerHost = "http://rajputchatserver.somee.com/services/testing.asmx/";
  LoggedInUserID = -1;
  LoggedUserName = "";
  LoggedInUserImage = "";
  isBlocked;
  FCMToken = "";
  isAdmin = false
  IsSqliteHasAllData = false;
  UserInfo = [];
  UserPlayerID = "";
  // NEW Varaibles
  isReply = 0;
  isReplyData: any;
  isReplyText = "";

  AllCountry = [];
  AllVilages = []
  AllCities = [];

  VillageCountHelper = 0;
  AdministratorCountHelper = 0;
  AreaCountHelper = 0;
  CityCountHelper = 0;

  //Added by Haroon

  AllUsersLastSeen = [];
  IsItRealtiminginLastSeen = false;
  ChatDetailUserLastSeen = "Never";
  ChatDetailUserID = -1;
  IsUserOnline() {
    let Value = this.AllUsersLastSeen.find(items => items.UserID == this.ChatDetailUserID);
    if (Value != undefined)
      this.ChatDetailUserLastSeen = Value.LastSeen

  }

  GetAllUserLastSeen() {
    this.database.object('/UserLastSeen/').valueChanges().subscribe((data) => {
      // if (data != null)
      //   this.IsUserOnline();
      if (data != null && !this.IsItRealtiminginLastSeen) {
        debugger;
        let UserLastSeenRecords = Object.keys(data);
        let FindCurrentUser = [];
        this.AllUsersLastSeen = [];
        for (var i = 0; i < UserLastSeenRecords.length; i++) {
          let Value = Object.assign(data[UserLastSeenRecords[i]], { SeenKey: UserLastSeenRecords[i] });
          this.AllUsersLastSeen.push(Value);

          if (i == UserLastSeenRecords.length - 1)
            this.SaveAllUsersLastSeen(this.UserInfo).then((data) => {
              if (data)
                this.IsItRealtiminginLastSeen = false;
            });
        }

        if (UserLastSeenRecords.length == 0)
          this.SaveAllUsersLastSeen(this.UserInfo).then((data) => {
            if (data)
              this.IsItRealtiminginLastSeen = false;
          });
        this.IsUserOnline();
      }
      else if (data == null)
        this.SaveAllUsersLastSeen(this.UserInfo).then((data) => {
          if (data)
            this.IsItRealtiminginLastSeen = false;
        });

    })
  }

  SaveAllUsersLastSeen(Users): Promise<Boolean> {
    return new Promise((resolve, error) => {
      this.IsItRealtiminginLastSeen = true;
      if (Users.length > 0) {
        for (let index = 0; index < Users.length; index++) {
          let Value = this.AllUsersLastSeen.find(items => items.UserID == Users[index].userid);
          debugger;
          if (Value == undefined) {
            let Obj = {
              UserID: Users[index].userid,
              LastSeen: Users[index].userid == this.LoggedInUserID ? "Online" : "App Never Used"
            }
            this.database.list("/UserLastSeen/").push(Obj).then(x => {
              debugger;
              console.log("User Last Seen added");
            })
          }

          if (index == Users.length - 1)
            return resolve(true);
          //   this.IsItRealtiminginLastSeen = false;
        }
      }
    })

  }


  UpdateUserLastSeen(IsOnline) {
    let UserKey = this.AllUsersLastSeen.find(items => items.UserID == this.LoggedInUserID);
    if (UserKey != undefined)
      this.database.list('/UserLastSeen/').update(UserKey.SeenKey, {
        LastSeen: IsOnline ? 'Online' : new Date().toISOString().split('T')[0] + " " + new Date().toLocaleTimeString()
      }).then(() => {
        if (IsOnline)
          this.AllUsersLastSeen[this.AllUsersLastSeen.indexOf(UserKey)].LastSeen = "Online";
      })
  }

  GetAllVillages() {
    this.ObjHttp.get(this.ServerHost + "GetAllVillages").map((res: Response) => res.text()).subscribe((data) => {

      let jsonString = this.GetJSONString(data);
      this.AllVilages = JSON.parse(jsonString);
    })
  }



  GetRowMazahirDP() {
    var picture = this.UserInfo.find(data => data.userid == 113).pic;
    return picture;
  }

  AlphabaticallyOrder(Arr, IsGroup) {

    Arr.sort(function (a, b) {
      if (a.name != null && b.name != null) {
        if (a.name.toLowerCase() < b.name.toLowerCase()) { return -1; }
        if (a.name.toLowerCase() > b.name.toLowerCase()) { return 1; }
      }

      return Arr;
    })


    // if (IsGroup)
    //   Arr.sort(function (a, b) {
    //     if (a.username < b.username) { return -1; }
    //     if (a.username > b.username) { return 1; }

    //     return 0;
    //   })

    // else
    //   Arr.sort(function (a, b) {
    //     if (a.name < b.name) { return -1; }
    //     if (a.name > b.name) { return 1; }


    //     return 0;
    //   })
    return Arr;
  }

  AlphabaticallyOrderGroup(Arr, IsGroup) {

    Arr.sort(function (a, b) {
      if (a.name.toLowerCase() < b.name.toLowerCase()) { return -1; }
      if (a.name.toLowerCase() > b.name.toLowerCase()) { return 1; }
      return Arr;
    })
    return Arr;
  }

  GetAllCities() {
    this.ObjHttp.get(this.ServerHost + "GetAllCities").map((res: Response) => res.text()).subscribe((data) => {
      let jsonString = this.GetJSONString(data);
      this.AllCities = JSON.parse(jsonString);
      this.AllCities.sort(function (a, b) {
        if (a.city_name < b.city_name) { return -1; }
        if (a.city_name > b.city_name) { return 1; }
        return 0;
      })
    })
  }

  GetAllCountries() {
    this.ObjHttp.get(this.ServerHost + "GetAllCountries").map((res: Response) => res.text()).subscribe((data) => {

      let jsonString = this.GetJSONString(data);
      this.AllCountry = JSON.parse(jsonString);
    })
  }

  GetUserInfo(Param) {
    var aa = this.UserInfo;
    return this.UserInfo.find(data => data.userid == Param);
  }

  constructor(private ObjHttp: Http, public ObjDatabase: DatabaseHelperProvider, private network: Network, private database: AngularFireDatabase) {

    this.GetYourVillageUserCount();
    this.GetYourAreaUserCount();
    this.GetYourCityCount();
    this.GetYourAdministratorUserCount();
    // alert('Hello HelperProvider Provider');
    // if (this.UserInfo.length == 0) {
    //     this.GetAllUsersFromMYSQL().then((data) => {
    //       alert(data);
    //       if (data) {
    //         this.LoggedInUserImage = this.UserInfo.find(items => items.userid == this.LoggedInUserID).image;
    //         this.LoggedUserName = this.UserInfo.find(items => items.userid == this.LoggedInUserID).name;
    //       }
    //     }, (error) => {
    //       alert("Error getting all users from cloud");
    //       alert(error);
    //     });
    // }

  }

  GetYourVillageUserCount() {

    let Village = this.UserInfo.find(items => items.userid == this.LoggedInUserID);
    let aa = this.UserInfo;
    let CountArray = this.UserInfo.filter(data => {
      if (data.village != null)
        return data.village.toLowerCase() == Village.village.toLowerCase()
    });

    this.VillageCountHelper = CountArray.length;
    return CountArray.length;
  }

  GetYourAdministratorUserCount() {

    let Village = this.UserInfo.find(items => items.userid == this.LoggedInUserID);
    let aa = this.UserInfo;
    let CountArray = this.UserInfo.filter(data => {
      if (data.isadmin != null)
        return data.isadmin == Village.isadmin
    });

    this.AdministratorCountHelper = CountArray.length;
    return CountArray.length;
  }

  GetYourAreaUserCount() {

    let Area = this.UserInfo.find(items => items.userid == this.LoggedInUserID);
    let aa = this.UserInfo;

    let CountArray = this.UserInfo.filter(data => {
      if (data.area != null)
        return data.area.toLowerCase() == Area.area.toLowerCase()
    });

    this.AreaCountHelper = CountArray.length;
    return CountArray.length;
  }

  GetYourCityCount() {

    let City = this.UserInfo.find(items => items.userid == this.LoggedInUserID);
    let aa = this.UserInfo;
    let CountArray = this.UserInfo.filter(data => {
      if (data.city != null)
        return data.city.toLowerCase() == City.city.toLowerCase()
    });

    this.CityCountHelper = CountArray.length;
    return CountArray.length;
  }


  IsUserLoginNew(): Promise<Boolean> {
    return new Promise((resolve, error) => {
      this.ObjDatabase.SelectQuerySqlite("select * from TblCurrentLoggedInUser").then((data: any) => {
        if (data.length > 0) {
          return resolve(true);
        } else {
          return error(false);
        }
      })
    })
  }

  IsUserLogin(): Promise<Boolean> {

    return new Promise((resolve, error) => {
      //alert("Inside IsUserLogin method");
      this.ObjDatabase.SelectQuerySqlite("select * from TblCurrentLoggedInUser").then((data: any) => {
        //alert("Got Data of LoggedInUser");
        let Value = [];
        //alert("Length of value is " + Value.length);
        data.forEach(element => {
          Value.push(element);
        });
        //alert("Length of value after loop is " + Value.length);

        if (Value.length > 0) {
          //alert("IsLoggedIn " + Value[0].userid);
          //alert("Is block" + Value[0].isblock)
          this.LoggedInUserID = Value[0].userid;
          this.isAdmin = Value[0].isAdmin;
          this.isBlocked = Value[0].isblock;
          this.GetAllUsersFromSqlite().then((data) => {
            if (data) {
              //alert("Data " + data)
              this.IsSqliteHasAllData = true;
              this.LoggedInUserImage = this.UserInfo.find(items => items.userid == this.LoggedInUserID).image;
              this.LoggedUserName = this.UserInfo.find(items => items.userid == this.LoggedInUserID).name;
              return resolve(true);
            }
            else {
              //alert("Else");
              this.GetAllUsersFromMYSQL().then((data) => {
                //alert(data);
                if (data) {
                  this.LoggedInUserImage = this.UserInfo.find(items => items.userid == this.LoggedInUserID).image;
                  this.LoggedUserName = this.UserInfo.find(items => items.userid == this.LoggedInUserID).name;
                  return resolve(true);
                }
                else
                  return error(false);
              }, (error) => {
                //alert("Error getting all users from cloud");
                //alert(error);
                return error(false);
              });
            }
          }, (error) => {
            //alert("Error while Getting all users from sqlite");
            //alert(error);
            return error(false);

          }
          )
          // if (this.GetAllUsersFromSqlite()) {
          //   this.IsSqliteHasAllData = true;
          //   return resolve(true);
          // }
          // else {
          // this.GetAllUsersFromMYSQL().then((data) => {
          //   //alert(data);
          //   if (data)
          //     return resolve(true);
          //   else
          //     return error(false);

          // });
        }
        // }
        else
          return error(false);
        // });
      }, (error) => {
        //alert("Error While selecting LoggedInUser");
        //alert(error);
        return error(false);

      }
      )
    })

  }

  GetAllUsersFromSqlite(): Promise<Boolean> {
    //alert("Trying to get All Users from Sqlite 0");
    return new Promise((resolve, error) => {
      //alert("Trying to get All Users from Sqlite");
      this.ObjDatabase.SelectQuerySqlite("select * from TblAllUsers").then((data: any) => {
        //alert("Gonna Start loop");
        //alert(data);
        //alert(data[0])
        //alert(data[0].userid + " is Userid")
        data.forEach(element => {
          // //alert(element);
          // //alert(element.userid)
          this.UserInfo.push(element);
        })


        // .then((data) => {
        //alert("Fetched All Users From Sqlite Bcz Length is " + this.UserInfo.length);
        if (this.UserInfo.length > 0) {
          //alert("There are so many users . Logged In is " + this.UserInfo[0].userid);
          return resolve(true);
        }
        else {
          //alert("No User found in Sqlite")
          return error(false);
        }
        // }, (error) => {
        //   //alert("Error While looping " + error);
        //   return error(false);
        // });
      }, (error) => {
        //alert("Error in Fetching Data" + error);
        return error(false);
      })
    })

  }

  GetJSONString(Json: string) {
    return Json.substring(Json.indexOf("["), Json.indexOf("</string>"));
  }

  GetAllUsersFromMYSQL(): Promise<boolean> {

    return new Promise((resolve, error) => {

      const Params = {};
      //alert("Getting Date of all users from cloud");
      this.ObjHttp.get(this.ServerHost + "GetAllUser").map((res: Response) => res.text()).subscribe((data) => {
        let jsonString = this.GetJSONString(data);
        this.UserInfo = JSON.parse(jsonString);

        //alert("Got Data of all users from cloud " + this.UserInfo.length);

        if (this.UserInfo.length > 0) {
          let AllUsers = "INSERT INTO TblAllUsers (userid, username, village , country , area , city , pic , email , isadmin , name) VALUES";

          this.isAdmin = this.UserInfo.find(items => items.userid == this.LoggedInUserID).isadmin == 1 ? true : false;
          this.isBlocked = this.UserInfo.find(items => items.userid == this.LoggedInUserID).isblock == 1 ? true : false;
          for (let i = 0; i < this.UserInfo.length; i++) {
            AllUsers = AllUsers + "('" + this.UserInfo[i].userid + "','" + this.UserInfo[i].username + "','" + this.UserInfo[i].village + "','" + this.UserInfo[i].country + "','" + this.UserInfo[i].area + "','" + this.UserInfo[i].city + "','" + this.UserInfo[i].image + "','" + this.UserInfo[i].email + "','" + this.UserInfo[i].isAdmin + "','" + this.UserInfo[i].name + "' ),"
            this.UserInfo.push(this.UserInfo[i]);
          }

          this.ObjDatabase.InsertQuerySqlite("delete from TblAllUsers", []).then(data => {
            //alert("Table Deleted");
            AllUsers = AllUsers.slice(0, -1);

            this.ObjDatabase.InsertQuerySqlite(AllUsers, []).then(data => {
              //alert("All Users Inserted ");
              //alert(AllUsers);
              return resolve(true);
            }
              , (error) => {
                //alert("Got Error adding all users in sqlite");
                //alert(error);
                return error(false)
              }
            )
          }, (error) => {
            //alert("Error while deleting table All Users");
            //alert(error);
            this.ObjDatabase.InsertQuerySqlite(AllUsers, []).then(data => {
              //alert("All Users Inserted ");
              //alert(AllUsers);
              return resolve(true);
            }, (error) => {
              //alert("Got Error While inserting All Users")
              //alert(error);
              return error(false)
            });
            return error(false)
          })

          // return resolve(true);

          //alert(this.UserInfo[0])
        }
      }
        , (error) => {
          //alert("Got error while getting all users data from cloud");
          return error(false)
        }
      )
    })
  }

}
