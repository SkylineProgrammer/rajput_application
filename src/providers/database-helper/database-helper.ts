import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the DatabaseHelperProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseHelperProvider {
  isOpen: any;
  TaxManager: SQLiteObject;


  constructor(public storage: SQLite) {
    console.log('Hello DatabaseHelperProvider Provider');
    //  Commented by Haroon on 5/13/2019
    // if (!this.isOpen) {
    //   this.storage = new SQLite();
    //   this.storage.create({ name: "TaxManager.db", location: "default" }).then((db: SQLiteObject) => {
    //     this.TaxManager = db;
    //     this.isOpen = true;
    //     // db.executeSql("DROP TABLE tbl_Login", []).then(res => { console.log('Table tbl_Login Deleted');  }, error => { console.log('Table tbl_Login Not Created', error); });

    //     // db.executeSql("DROP TABLE tblChatDetails", []).then(res => { console.log('tblChatDetails Drop');  }, error => { console.log('Table Transaction Not Created', error); });

    //     // db.executeSql("DROP TABLE tbl_AccountHead", []).then(res => { console.log('Table AccountHead deleted');  }, error => { console.log('Table AccountHead Not Created', error); });
    //     db.executeSql("CREATE TABLE IF NOT EXISTS tblTest ( ID INTEGER PRIMARY KEY AUTOINCREMENT , Name TEXT)", []).then(res => { console.log('Table Transaction Created') }, error => { console.log('Table Transaction Not Created'); });
    //     db.executeSql("CREATE TABLE IF NOT EXISTS tblMainChat ( ID INTEGER PRIMARY KEY AUTOINCREMENT , chatid TEXT , datechat TEXT , dp TEXT , groupname TEXT , image TEXT , lastmessage TEXT , recieverid TEXT , userid TEXT , username TEXT )", []).then(res => { console.log('TBL MAIN CHAT') }, error => { console.log('Table Transaction Not Created'); });
    //     db.executeSql("CREATE TABLE IF NOT EXISTS tblMainGroup( ID INTEGER PRIMARY KEY AUTOINCREMENT , Key TEXT , dp TEXT , groupid TEXT , groupname TEXT , grouptype TEXT , isadminview TEXT , lastmessage TEXT , lastmessagedate TEXT )", []).then(res => { console.log('TBL MAIN GROUP') }, error => { console.log('Table Transaction Not Created'); });
    //     db.executeSql("CREATE TABLE IF NOT EXISTS tblChatDetails( ID INTEGER PRIMARY KEY AUTOINCREMENT , chatid TEXT , isreply TEXT , message TEXT , messagedate TEXT , reciverid TEXT , replymessageid TEXT , status TEXT , userid TEXT , username TEXT )", []).then(res => { console.log('TBL CHAT DETAILS') }, error => { console.log('Table Transaction Not Created'); });
    //     db.executeSql("CREATE TABLE IF NOT EXISTS tblGroupDetails( ID INTEGER PRIMARY KEY AUTOINCREMENT , groupid integer , grouptype TEXT , isreply TEXT , message TEXT , messagedate TEXT , replymessageid TEXT , status TEXT , userid TEXT , username TEXT , village TEXT )", []).then(res => { console.log('TBL CHAT DETAILS') }, error => { console.log('Table Transaction Not Created'); });
    //     db.executeSql("CREATE TABLE IF NOT EXISTS TblAllUsers( ID INTEGER PRIMARY KEY AUTOINCREMENT , userid integer , username TEXT , village TEXT , country TEXT , area TEXT , city TEXT , pic TEXT , email TEXT )", []).then(res => { console.log('TBL AllUsers Created') }, error => { console.log('Table AllUsers Not Created'); });
    //     db.executeSql("CREATE TABLE IF NOT EXISTS TblCurrentLoggedInUser(   ID INTEGER PRIMARY KEY AUTOINCREMENT , userid integer  , islogin TEXT )", []).then(res => { console.log('TblCurrentLoggedInUser Created') }, error => { console.log('Table AllUsers Not Created'); });

    //     db.executeSql("ALTER TABLE tblMainChat ADD isSync integer;");
    //     db.executeSql("ALTER TABLE tblMainGroup ADD isSync integer;");
    //     db.executeSql("ALTER TABLE tblChatDetails ADD isSync integer;");
    //     db.executeSql("ALTER TABLE tblGroupDetails ADD isSync integer;");
    //     db.executeSql("ALTER TABLE TblAllUsers ADD isAdmin text;");
    //     db.executeSql("ALTER TABLE TblCurrentLoggedInUser ADD isAdmin text;");
    //     db.executeSql("ALTER TABLE TblCurrentLoggedInUser ADD isblock text;");

    //     // db.executeSql("CREATE TABLE IF NOT EXISTS tbl_Login       ( CustomerID TEXT , Username TEXT, Password TEXT,  TrialExpiryDate DATETIME, IsSync BIT, IsDeleted BIT)", []).then(res => { console.log('Table tbl_Login Created', res) }, error => { console.log('Table tbl_Login Not Created', error); });
    //     // db.executeSql("CREATE TABLE IF NOT EXISTS tbl_AccountHead (  CustomerID TEXT , AccountServerID TEXT, AccountHeadID INTEGER PRIMARY KEY AUTOINCREMENT, AccountType INTEGER, AccountHeadName TEXT , IsSync BIT, IsDeleted BIT)", []).then(res => { console.log('Table AccountHead Created', res) }, error => { console.log('Table AccountHead Not Created', error); });

    //   }).catch((error) => {
    //     console.log("While creating DB ")
    //     console.log(error);
    //   })
    // }

  }

  CreateDB(): Promise<Boolean> {
    return new Promise((resolve, error) => {
      if (!this.isOpen) {
        this.storage = new SQLite();
        this.storage.create({ name: "TaxManager.db", location: "default" }).then((db: SQLiteObject) => {
          this.TaxManager = db;
          this.isOpen = true;
          // db.executeSql("DROP TABLE tbl_Login", []).then(res => { console.log('Table tbl_Login Deleted');  }, error => { console.log('Table tbl_Login Not Created', error); });

          // db.executeSql("DROP TABLE tblChatDetails", []).then(res => { console.log('tblChatDetails Drop');  }, error => { console.log('Table Transaction Not Created', error); });

          // db.executeSql("DROP TABLE tbl_AccountHead", []).then(res => { console.log('Table AccountHead deleted');  }, error => { console.log('Table AccountHead Not Created', error); });
          db.executeSql("CREATE TABLE IF NOT EXISTS tblTest ( ID INTEGER PRIMARY KEY AUTOINCREMENT , Name TEXT)", []).then(res => { console.log('Table Transaction Created') }, error => { console.log('Table Transaction Not Created'); });
          db.executeSql("CREATE TABLE IF NOT EXISTS tblMainChat ( ID INTEGER PRIMARY KEY AUTOINCREMENT , chatid TEXT , datechat TEXT , dp TEXT , groupname TEXT , image TEXT , lastmessage TEXT , recieverid TEXT , userid TEXT , username TEXT )", []).then(res => { console.log('TBL MAIN CHAT') }, error => { console.log('Table Transaction Not Created'); });
          db.executeSql("CREATE TABLE IF NOT EXISTS tblMainGroup( ID INTEGER PRIMARY KEY AUTOINCREMENT , Key TEXT , dp TEXT , groupid TEXT , groupname TEXT , grouptype TEXT , isadminview TEXT , lastmessage TEXT , lastmessagedate TEXT )", []).then(res => { console.log('TBL MAIN GROUP') }, error => { console.log('Table Transaction Not Created'); });
          db.executeSql("CREATE TABLE IF NOT EXISTS tblChatDetails( ID INTEGER PRIMARY KEY AUTOINCREMENT , chatid TEXT , isreply TEXT , message TEXT , messagedate TEXT , reciverid TEXT , replymessageid TEXT , status TEXT , userid TEXT , username TEXT )", []).then(res => { console.log('TBL CHAT DETAILS') }, error => { console.log('Table Transaction Not Created'); });
          db.executeSql("CREATE TABLE IF NOT EXISTS tblGroupDetails( ID INTEGER PRIMARY KEY AUTOINCREMENT , groupid integer , grouptype TEXT , isreply TEXT , message TEXT , messagedate TEXT , replymessageid TEXT , status TEXT , userid TEXT , username TEXT , village TEXT )", []).then(res => { console.log('TBL CHAT DETAILS') }, error => { console.log('Table Transaction Not Created'); });
          db.executeSql("CREATE TABLE IF NOT EXISTS TblAllUsers( ID INTEGER PRIMARY KEY AUTOINCREMENT , userid integer , username TEXT , village TEXT , country TEXT , area TEXT , city TEXT , pic TEXT , email TEXT )", []).then(res => { console.log('TBL AllUsers Created') }, error => { console.log('Table AllUsers Not Created'); });
          db.executeSql("CREATE TABLE IF NOT EXISTS TblCurrentLoggedInUser( ID INTEGER PRIMARY KEY AUTOINCREMENT , userid integer  , islogin TEXT )", []).then(res => { console.log('TblCurrentLoggedInUser Created') }, error => { console.log('Table AllUsers Not Created'); });

          db.executeSql("CREATE TABLE IF NOT EXISTS TblOrgGroups( ID INTEGER PRIMARY KEY AUTOINCREMENT , GroupName TEXT  , GroupDP TEXT )", []).then(res => { console.log('TblCurrentLoggedInUser Created') }, error => { console.log('Table AllUsers Not Created'); });
          db.executeSql("CREATE TABLE IF NOT EXISTS TblOrgUser( ID INTEGER PRIMARY KEY AUTOINCREMENT , GroupName TEXT  , UserID TEXT , Status TEXT)", []).then(res => { console.log('TblCurrentLoggedInUser Created') }, error => { console.log('Table AllUsers Not Created'); });
          db.executeSql("CREATE TABLE IF NOT EXISTS tblOrgTableDetails( ID INTEGER PRIMARY KEY AUTOINCREMENT  , isreply TEXT , message TEXT , messagedate TEXT , replymessageid TEXT , status TEXT , userid TEXT , username TEXT , OrgGroupName TEXT )", []).then(res => { console.log('TBL CHAT DETAILS') }, error => { console.log('Table Transaction Not Created'); });



          db.executeSql("ALTER TABLE tblOrgTableDetails ADD isSync integer;");
          db.executeSql("ALTER TABLE tblMainChat ADD isSync integer;");
          db.executeSql("ALTER TABLE tblMainGroup ADD isSync integer;");
          db.executeSql("ALTER TABLE tblChatDetails ADD isSync integer;");
          db.executeSql("ALTER TABLE tblGroupDetails ADD isSync integer;");

          db.executeSql("ALTER TABLE TblAllUsers ADD isadmin text;");
          db.executeSql("ALTER TABLE TblAllUsers ADD name text;");


          db.executeSql("ALTER TABLE TblCurrentLoggedInUser ADD isAdmin text;");
          db.executeSql("ALTER TABLE TblCurrentLoggedInUser ADD isblock text;");

          // db.executeSql("CREATE TABLE IF NOT EXISTS tbl_Login       ( CustomerID TEXT , Username TEXT, Password TEXT,  TrialExpiryDate DATETIME, IsSync BIT, IsDeleted BIT)", []).then(res => { console.log('Table tbl_Login Created', res) }, error => { console.log('Table tbl_Login Not Created', error); });
          // db.executeSql("CREATE TABLE IF NOT EXISTS tbl_AccountHead (  CustomerID TEXT , AccountServerID TEXT, AccountHeadID INTEGER PRIMARY KEY AUTOINCREMENT, AccountType INTEGER, AccountHeadName TEXT , IsSync BIT, IsDeleted BIT)", []).then(res => { console.log('Table AccountHead Created', res) }, error => { console.log('Table AccountHead Not Created', error); });
          // alert("DB Created");
          return resolve(true);
        }).catch((error2) => {
          alert("Erro While creating DB ")
          alert(error);
          return error(false);
        })
      }
    });

  }

  SelectQuerySqlite(param) {
    return new Promise((resolve, reject) => {
      this.TaxManager.executeSql(param, []).then((data) => {
        let activityValues = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            activityValues.push(data.rows.item(i));
          }
        }
        console.table(activityValues)
        resolve(activityValues);
      }, (error) => {
        reject(error);
      })
    });
  }

  InsertQuerySqlite(updateQuery, param) {
    console.log(updateQuery);
    return new Promise((resolve, reject) => {
      this.TaxManager.executeSql(updateQuery, param).then((data) => {
        resolve(data);
      }, (error) => {
        reject(error);

        console.log(error.message);
        console.log('Error');
      })
    });
  }

  UpdateQuerySqlite(updateQuery, param) {
    this.TaxManager.executeSql(updateQuery, param).then((data) => {
    });
  }

  ExecuteIntoSQlite(query, param) {

    console.log(query);
    return new Promise((resolve, reject) => {
      this.TaxManager.executeSql(query, param).then((data) => {

      }, (error) => {
        reject(error);
        console.log("Error");
      })
    });

  }


}

