import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { InoutHomePage } from '../pages/inout-home/inout-home';
import { HelperProvider } from '../providers/helper/helper';
import { FoundationChatLogingPage } from '../pages/foundation-chat-loging/foundation-chat-loging';
import { FoundationChatMainPage } from '../pages/foundation-chat-main/foundation-chat-main';
import { FCM } from '@ionic-native/fcm';
import { DatabaseHelperProvider } from '../providers/database-helper/database-helper';
import { Network } from '@ionic-native/network';
import { Http, Response } from '@angular/http';
import { OneSignal } from '@ionic-native/onesignal';
import { WebsitePage } from '../pages/website/website';
import { FoundationCheckPage } from '../pages/foundation-check/foundation-check';
import { Badge } from '@ionic-native/badge';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform,
    private ObjHttp: Http,
    statusBar: StatusBar,
    private network: Network,
    public ObjDatabase: DatabaseHelperProvider,
    splashScreen: SplashScreen,
    private ObjHelper: HelperProvider,
    private fcm: FCM,
    private oneSignal: OneSignal,
    private badge: Badge
  ) {
    platform.ready().then(() => {


      document.addEventListener('deviceready', function () {
          this.database.list("/Validation/").push({
            Text : "this.email",
          });
          // this.database.list("/User/").push(this.pass);
    }, false)


      console.log("Platform is ready");
      this.GetAllUserFromServer();

      this.ObjDatabase.CreateDB().then((data) => {

        this.ObjHelper.IsUserLogin().then((data) => {
          if (data) {
            // alert("If " + data);
            // this.rootPage = FoundationChatMainPage;
            this.ObjHelper.UpdateUserLastSeen(true);
            this.rootPage = HomePage;

          }
          else {
            // alert("Else " + data);
            this.rootPage = FoundationChatLogingPage;
            // this.ObjHelper.LoggedInUserID = 87;
            // this.rootPage = FoundationChatMainPage;
          }
        }, (error) => {
          // alert("Got Error " + error);
          this.rootPage = FoundationChatLogingPage;
          // this.ObjHelper.LoggedInUserID = 87;
          // this.rootPage = FoundationChatMainPage;
        }

        )
      }
        , (error) => {
          alert("Something went wrong please restart the app");
          this.rootPage = FoundationChatLogingPage;
          // this.ObjHelper.LoggedInUserID = 87;
          // this.rootPage = FoundationChatMainPage;
        }
      )

      this.oneSignal.startInit('239cc4e4-39d2-4eca-b00f-d67f43f6270f', '7770789739');

      // // Last work did by Haroon on 02-Aug-2019
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

      this.oneSignal.handleNotificationReceived().subscribe(() => {
        this.badge.increase(1);
        // do something when notification is received
      });

      this.oneSignal.handleNotificationOpened().subscribe(() => {
        alert("Notification Opened");
        // do something when a notification is opened
      });

      this.oneSignal.getIds().then((id) => {
        console.log(id);
        // alert("Simple ID " + id.userId);
        this.ObjHelper.UserPlayerID = id.userId;
        // alert("Finally player id is " + this.ObjHelper.UserPlayerID);
      });

      this.oneSignal.endInit();

      this.fcm.getToken().then(token => {
        this.ObjHelper.FCMToken = token;
        console.log("Got Token");
        console.log(token);
        // this.rootPage = FoundationChatLogingPage;
        // backend.registerToken(token);
      });

      this.fcm.onNotification().subscribe(data => {
        console.log("Notification Recived");
        if (data.wasTapped) {
          console.log("Received in background");
        } else {
          console.log("Received in foreground");
        };
      });

      this.fcm.onTokenRefresh().subscribe(token => {
        this.ObjHelper.FCMToken = token;
        // backend.registerToken(token);
      });
      // For serve purpose ends here of 15/5/19
      // =======================================================================================

      // if (this.ObjHelper.IsUserLogin()) {
      //   console.log("Inside If");
      //   this.rootPage = FoundationChatMainPage
      // }
      // else {
      //   console.log("Inside Else")
      //   this.rootPage = FoundationChatLogingPage;
      // }

      // purpose only
      // this.ObjHelper.GetAllUsersFromMYSQL();
      // this.rootPage = HomePage

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }


  SaveAllUsers(data) {

    this.ObjDatabase.SelectQuerySqlite("delete from TblAllUsers").then(res => {
      let AllUsers = "INSERT INTO TblAllUsers (userid, username, village , country , area , city , pic , email , isadmin , name) VALUES";
      data.forEach(element => {
        AllUsers = AllUsers + "('" + element.userid + "','" + element.username + "','" + element.village + "','" + element.country + "','" + element.area + "','" + element.city + "','" + element.pic + "','" + element.email + "','" + element.isadmin + "','" + element.name + "'),"
      });
      AllUsers = AllUsers.slice(0, -1);
      this.ObjDatabase.InsertQuerySqlite(AllUsers, []).then(data => {
        // alert("User sk Inserted");
      })
    })

  }



  GetAllUserFromServer() {
    const ParamsAllUser = {};
    this.ObjHttp.post(this.ObjHelper.ServerHost + "GetAllUser", ParamsAllUser).map((res: Response) => res.json()).subscribe((data) => {

      let UserInfo = [];
      UserInfo = JSON.parse(data.d);
      if (UserInfo.length > 0) {
        this.ObjHelper.UserInfo = UserInfo;
        this.ObjHelper.GetAllUserLastSeen();
      }
      this.SaveAllUsers(UserInfo);
      let Value = UserInfo.find(items => items.userid == this.ObjHelper.LoggedInUserID);
      if (Value != undefined) {

        console.log("Found")
        this.ObjHelper.LoggedInUserID = Number(Value.userid);
        this.ObjHelper.LoggedUserName = Value.username;
        this.ObjHelper.LoggedInUserImage = Value.image;
        this.ObjHelper.isAdmin = Value.isadmin;
        this.ObjHelper.isBlocked = Value.isblock;
        var aa = this.ObjHelper.GetYourVillageUserCount();
        var bb = this.ObjHelper.GetYourAreaUserCount();
        var cc = this.ObjHelper.GetYourCityCount();
        let Query = "INSERT INTO TblCurrentLoggedInUser (userid , islogin , isAdmin , isblock ) VALUES (" + this.ObjHelper.LoggedInUserID + " , 'true' , " + Value.isadmin + " , " + Value.isblock + ")";

        this.ObjDatabase.SelectQuerySqlite("delete from TblCurrentLoggedInUser").then(res => {
          this.ObjDatabase.ExecuteIntoSQlite(Query, []).then(data => {
            // alert("user Inserted");
          })
        })


        // this.navCtrl.setRoot(HomePage);
      }
    }, (error) => {
      // alert("Error while getting all users from cloud");
      // alert(error);
    })
  }

}

