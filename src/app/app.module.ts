import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { PhotoViewer } from '@ionic-native/photo-viewer';
// import { EmojiPickerModule } from '@ionic-tools/emoji-picker';
import { EmojiPickerModule } from 'ionic-emoji-picker';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { RatioCrop } from 'ionic-cordova-plugin-ratio-crop';
// Voice
// import { MediaCaptureOriginal } from '@ionic-native/media-capture';
// import { MediaOriginal, Media } from '@ionic-native/media';
// import { IonicStorageModule  } from '@ionic/storage';
// Voice

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { InoutHomePage } from '../pages/inout-home/inout-home';
import { FoundationChatMainPage } from '../pages/foundation-chat-main/foundation-chat-main';
import { FoundationChatPageDetailsPage } from '../pages/foundation-chat-page-details/foundation-chat-page-details';
import { FoundationProfilePage } from '../pages/foundation-profile/foundation-profile';
import { FoundationGroupChatPage } from '../pages/foundation-group-chat/foundation-group-chat';
import { FoundationNewChatPage } from '../pages/foundation-new-chat/foundation-new-chat';
import { FoundationOrgGroupsPage } from '../pages/foundation-org-groups/foundation-org-groups';
import { FoundationAdminModalPage } from '../pages/foundation-admin-modal/foundation-admin-modal';
import { FoundationOrgOptionsPage } from '../pages/foundation-org-options/foundation-org-options';
import { FoundationCheckPage } from '../pages/foundation-check/foundation-check';
import { HTTP } from '@ionic-native/http';


import { SQLite } from '@ionic-native/sqlite';
import { AngularFireModule } from '@angular/fire'
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { Network } from '@ionic-native/network';
import { HttpModule } from '@angular/http';
import { HelperProvider } from '../providers/helper/helper';
import { FoundationChatLogingPage } from '../pages/foundation-chat-loging/foundation-chat-loging';
import { RegistrationPage } from '../pages/Registration/registration';
import { DatabaseHelperProvider } from '../providers/database-helper/database-helper';
import { FCM } from '@ionic-native/fcm';
import { OneSignal } from '@ionic-native/onesignal';
import { WebsitePage } from '../pages/website/website';
import { Clipboard } from '@ionic-native/clipboard';
import { MediaCapture } from '@ionic-native/media-capture';
import { Base64 } from '@ionic-native/base64';
import { Media } from '@ionic-native/media';
import { Crop } from '@ionic-native/crop';
import { Badge } from '@ionic-native/badge';

// import { Media, MediaObject } from '@ionic-native/media';
// import {  FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database-deprecated';




const firebase = {
  apiKey: "AIzaSyB6Dh_dcC_kLGVBWLnUmoQCOiCKKFqhiMs",
  authDomain: "rajputchat-42dc6.firebaseapp.com",
  databaseURL: "https://rajputchat-42dc6.firebaseio.com",
  projectId: "rajputchat-42dc6",
  storageBucket: "rajputchat-42dc6.appspot.com",
  messagingSenderId: "7770789739"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    InoutHomePage,
    FoundationChatMainPage,
    FoundationChatPageDetailsPage,
    FoundationProfilePage,
    FoundationGroupChatPage,
    FoundationChatLogingPage,
    WebsitePage,
    FoundationNewChatPage,
    RegistrationPage,
    FoundationOrgGroupsPage,
    FoundationAdminModalPage,
    FoundationOrgOptionsPage,
    FoundationCheckPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      tabsPlacement: 'top', tabsHideOnSubPages: true,
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false,
    }),
    // IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    HttpModule,
    EmojiPickerModule.forRoot(),
    // EmojiPickerModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    InoutHomePage,
    FoundationChatMainPage,
    FoundationChatPageDetailsPage,
    FoundationProfilePage,
    FoundationGroupChatPage,
    FoundationChatLogingPage,
    WebsitePage,
    FoundationNewChatPage,
    RegistrationPage,
    FoundationOrgGroupsPage,
    FoundationAdminModalPage,
    FoundationOrgOptionsPage,
    FoundationCheckPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    PhotoViewer,
    Network,
    SQLite,
    // SQLiteObject,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    HelperProvider,
    DatabaseHelperProvider,
    FCM,
    Media,
    File,
    OneSignal,
    FileTransfer,
    // FileUploadOptions,
    FileTransferObject,
    Camera,
    Clipboard,
    MediaCapture,
    Base64,
    Crop,
    RatioCrop,
    Badge,
    HTTP

    // MediaCaptureOriginal,
    // MediaOriginal
  ]
})
export class AppModule { }
